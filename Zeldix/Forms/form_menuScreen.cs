﻿/*
 * Class        :   form_menuScreen.cs
 * Author       :   clampsy
 * Description  :   
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Zeldix.Classes.Crap;

namespace Zeldix.Forms
{
    public partial class form_menuScreen : Form
    {
        private int[] pointer;
        private string[] layers, boxTextArray;
        private const int FFdel = 0xFF;
        private TextBox[] boxArray;

        internal form_menuScreen()
        {
            string[] text = new string[]
            {
                "Load list",
                "Load screen",
                "Erase list",
                "Copy list",
                "Destination list",
                "Title screen",
                "Unused Pointer",
                "Naming screen",
                "Player select",
                "Copy screen",
                "Erase screen",
                "Map screen",
                "Logo"
            };


            pointer = new int[text.Length];

            fixedPointerScreens(0x65D6C, 0xFC, 0xDF, 0xFC, 0xAC, 0x84);

            InitializeComponent();
            boxTextArray = new string[3];
            boxArray = new TextBox[] { textBox_address1, textBox_address2, textBox_address3 };

            

            //create radio buttons
            for (int i = 0; i < text.Length; i++)
            {
                RadioButton radio = new RadioButton { Name = i + "", Text = text[i], Height = 20 };
                radio.Click += new EventHandler(radio_button_click);
                flowPanel.Controls.Add(radio);
            }

            byte[] b = RomIO.read(0x137D, 0x19);
            richTextBox_pointers.Text = RomIO.readInHex(0x137D, 0x19, " ");

            int j = 9, ii = 0, jj = 5;

            List<int> list = new List<int>();
            while (ii < 7)
            {
                try { pointer[jj] = AddressLoROM.snesToPc(b[ii], b[ii + j], b[ii + j * 2]); }
                catch { pointer[jj] = -1; }
                jj++;
                ii++;

            }
            pointer[jj++] = 0x66D7A; //for Nintendo logo
        }

        private void radio_button_click(object sender, EventArgs e)
        {
            textBox_address1.Text = textBox_address2.Text = textBox_address3.Text = "";
            boxTextArray = new string[3];

            layers = new string[3];
            try
            {
                int i = Convert.ToInt16(((RadioButton)sender).Name),
                    address = pointer[i];

                if (address <= RomIO.size && address >= 0)
                {
                    int len = 0, current = address - 1;
                    switch (i)
                    {
                        case 05: //title screen
                            for (int yy = 0; yy < 3; yy++)
                            {
                                layers[yy] = RomIO.readWithDelimAsHex(current += len + 1, FFdel, ref len, " ");
                                boxTextArray[yy] = MyMath.decToHex(current);
                            }
                            break;
                        case 07: //naming screen
                            for (int op = 0; op < 3; op += 2)
                            {
                                layers[op] = RomIO.readWithDelimAsHex(current += len + 1, FFdel, ref len, " ");
                                boxTextArray[op] = MyMath.decToHex(current);
                            }
                            break;
                        case 08: //player select
                            layeringTwoBytes(0, 0x64E5F, 0x35);
                            layering(1, address, FFdel);
                            layeringTwo(2, 0x64ED0, FFdel, 1);
                            break;
                        case 09: //copy player
                            layeringTwoBytes(0, 0x65148, FFdel);
                            layering(2, address, FFdel); //actually layer 3 
                            layeringTwoBytes(2, 0x65292, FFdel); //actually subcopy of BG3
                            break;
                        case 10: //erase player
                            layering(0, pointer[10], FFdel);
                            layeringTwo(1, 0x654C0, FFdel, 1);
                            layering(2, pointer[9], FFdel);
                            break; 
                        default:
                            layering(0, address, FFdel);
                            break;
                    }

                    radioEnable(layers[0] != null, layers[1] != null, layers[2] != null);

                    textBox_address1.Text = MyMath.decToHex(address);
                    richTextBox_data2.Text = layers[0];
                    scrollToTop();

                    //display address location in textBoxes
                    for (int jj = 0; jj < 3; jj++)
                    {
                        try {  boxArray[jj].Text = boxTextArray[jj]; }
                        catch (Exception ee) { ErrorMessage.Show("Error" + ee); }
                    }
                }
                else
                {
                    richTextBox_data2.Text = "";
                    radioEnable(false, false, false);
                    textBox_address1.Text = MyMath.decToHex(address);
                    ErrorMessage.Show("Invalid pointer that points to address 0x" + MyMath.decToHex(address), false);
                }
            }
            catch (Exception ee) { ErrorMessage.Show("Error: " + ee, false); }
        }

        private void layering(int index, int a, int del) 
        {
            layers[index] = RomIO.readWithDelimAsHex(a, del, " ");
            boxTextArray[index] = MyMath.decToHex(a);
        }

        private void layeringTwoBytes(int index, int a, int del)
        { layeringTwo(index, a, del, 0); }

        private void layeringTwo(int index, int a, int del, int add)
        {
            int s = AddressLoROM.snesToPc(RomIO.read(a), RomIO.read(a + 1), 0x0C);
            layers[index] = RomIO.readWithDelimAsHex(s + add, del, " ");
            boxTextArray[index] = MyMath.decToHex(s + add);
        }

        private void fixedPointerScreens(int address, int i1, int i2, int i3, int i4, int i5)
        {
            int[] size = new int[6] { 0x0, i1, i2, i3, i4, i5 };
            for (int i = 0; i < 5; i++)
                pointer[i] = address += size[i] + 1;
        }

        private void radioEnable(bool b1, bool b2, bool b3)
        {
            radioButton_part1.Enabled = b1;
            radioButton_part2.Enabled = b2;
            radioButton_part3.Enabled = b3;

            radioButton_part1.Checked = true;
        }

        private void radioButton_part1_CheckedChanged(object sender, EventArgs e)
        { check((RadioButton)sender, 0); }
        private void radioButton_part2_CheckedChanged(object sender, EventArgs e)
        { check((RadioButton)sender, 1); }
        private void radioButton_part3_CheckedChanged(object sender, EventArgs e)
        { check((RadioButton)sender, 2); }
        private void check(RadioButton sender, int i)
        {
            scrollToTop();
            richTextBox_data2.Text = layers[i];
        }

        private void scrollToTop()
        {
            richTextBox_data2.SelectionStart = 1;
            richTextBox_data2.SelectionLength = 0;
            richTextBox_data2.ScrollToCaret();
        }
    }
}