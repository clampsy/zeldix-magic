﻿using System;
using System.Collections.Generic;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;
using Zeldix.Classes.Monologue;
using System.Windows.Forms;
using Zeldix.Classes.Enemies;
using Zeldix.Classes.GraphicBoxSelect;
using Zeldix.Classes.Monologue.Image;
using Zeldix.Classes;
using System.Linq;

namespace Zeldix.Forms
{
    internal partial class form_monologue : form_template
    {
        private const string body = "form_monologue_";
        private readonly MonologueIo mono;

        private List<string>
            room_datasource,
            original_room_list;

        private const int scale = 2;

        private int 
            MonologueIndex,
            dialogueLinesIndex;

        private List<PixelBox> dialogueLines;



        /// <summary>
        /// Initilializes a new instance of the form
        /// </summary>
        /// <param name="rom"></param>
        internal form_monologue()
        {
            mono = new MonologueIo();
            InitializeComponent();
            UpdateRoomList();
            UpdateText();
        }

        /// <summary>
        /// Assign text to controls
        /// </summary>
        private void UpdateText()
        {
            toolTip1.SetToolTip(groupBox_edit, Settings.getStringTip(this, "edit"));
            toolTip1.SetToolTip(groupBox_entry, Settings.getStringTip(this, "entry"));
            //toolTip1.SetToolTip(groupBox_actions, Settings.getStringTip(this, "actions"));
            toolTip1.SetToolTip(groupBox_preview, Settings.getStringTip(this, "preview"));

            //button_dump.Text = Settings.getString(this, "dump");
            //button_import.Text = Settings.getString(this, "import");
            //button_save.Text = Settings.getString(this, "save_button");
            groupBox_preview.Text = Settings.getString(this, "preview");
            Text = Settings.getString(this, "header");
        }

        private void MonologueRead()
        {
            richTextBox_selected.Text = mono.ByteToText(MonologueIndex);
            Button_get_dia_Click();
        }

        #region Box
        private void UpdateRoomList()
        {
            UpdateList
                (
                ref ListBox_room_list,
                ref original_room_list,
                ref room_datasource,
                "form_monologue_op",
                mono.NumberOfMonologueEntries
                );
        }

        private string FormatNumber(int i)
        { return i.ToString("000"); }

        private void UpdateList(ref ListBox listbox, ref List<string> original_list, ref List<string> datasource, string name, int entryNo)
        {
            listbox.Items.Clear();
            original_list = new List<string>();

            for (int i = 0; i < entryNo; i++)
            {
                string number = FormatNumber(i);
                string text = Settings.getString(name + number);

                if (text != "")
                    text = divider + text;
                text = number + text;

                original_list.Add(text);
            }

            datasource = new List<string>();
            for (int i = 0; i < original_list.Count; i++)
                datasource.Add(original_list[i]);
            listbox.DataSource = datasource;
        }

        private const int substr = 3;
        private const string divider = " - ";

        private void ListBox_rooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = ListBox_room_list.SelectedItem.ToString();

            MonologueIndex = ushort.Parse(name.Substring(0, substr));

            if (name.Length > substr + divider.Length)
                textBox_room_name_edit.Text = name.Substring(substr + divider.Length);
            else textBox_room_name_edit.Text = "";

            MonologueRead();
        }

        private void Button_edit_Click(object sender, EventArgs e)
        {
            string number = FormatNumber(MonologueIndex);
            string entry = number;
            if (textBox_room_name_edit.Text != "")
                entry += divider + textBox_room_name_edit.Text;

            int iii = MonologueIndex;

            original_room_list[MonologueIndex] = entry;

            Settings.AddOrUpdateResource("form_monologue_op" + number, textBox_room_name_edit.Text);

            int select = ListBox_room_list.SelectedIndex;
            SearchTextBox_TextChanged();
            ListBox_room_list.SelectedIndex = select;
        }

        private void SearchTextBox_TextChanged(object sender = null, EventArgs e = null)
        {
            room_datasource = SearchBoxTools.ListBoxRefinement(original_room_list, SearchTextBox.Text.ToLower());
            ListBox_room_list.DataSource = room_datasource;
        }
        #endregion

        /// <summary>
        /// Event handler for commiting a monologue change and adding it to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_commit_Click(object sender, EventArgs e)
        {
            try
            {
                mono.InsertMonologueEntry(MonologueIndex, richTextBox_selected.Text);
                Button_get_dia_Click();
            }
            catch (NullReferenceException ee)
            { ErrorMessage.Show(ee); }
            catch (IndexOutOfRangeException)
            { ErrorMessage.Show(Settings.getString(body + "searchFirst")); }
            catch (Exception ee)
            { ErrorMessage.Show(ee); }
        }

        /// <summary>
        /// Enables and disables the buttons for the in-game text preview
        /// panel.
        /// </summary>
        private void UpdateMenuButtons()
        {
            //Enabled and disabled depending if on first or last line.
            button_scroll_up.Enabled = (dialogueLinesIndex != 0);
            button_scroll_down.Enabled = (dialogueLinesIndex < dialogueLines.Count - 1);
        }


        private void Button_get_dia_Click()
        {
            //flowLayoutPanel1
            myFont f = new myFont();
            List<object> aa;
            if (!richTextBox_selected.Text.Equals(""))
            {
                aa = f.ConvertToImage(richTextBox_selected.Text.Replace("\n", Constants.NEWLINE).Replace("\r", ""));
                dialogueLines = DisplayText.Display(aa, scale);

                flowLayoutPanel_text.Controls.Clear();
                foreach (PixelBox p in dialogueLines)
                    flowLayoutPanel_text.Controls.Add(p);
                Display(dialogueLinesIndex = 0);
            }

            else
            {
                dialogueLinesIndex = 0;
                dialogueLines = new List<PixelBox>();
                flowLayoutPanel_text.Controls.Clear();
                UpdateMenuButtons();
            }
        }


        private void Display(int index)
        {
            /* 
             * I blame Microsoft for this low performance scrolling.
             * You can't adjust flow panel scrolling
             * programmatically without showing the scrollbar.
             */

            int i = 0;
            int counter = 0;
            foreach (PixelBox p in dialogueLines)
                if (i++ >= index && i <= index + number)
                {
                    if (!p.Visible)
                    {
                        p.Visible = true;
                        counter++;
                    }
                }
                else if (p.Visible)
                {
                    p.Visible = false;
                    if (counter == number) //saves CPU time
                        break;
                }
            UpdateMenuButtons();
        }

        private const int number = 3;

        private void Button_scroll_down_Click(object sender, EventArgs e)
        {
            if (dialogueLinesIndex < dialogueLines.Count - 1)
                Display(++dialogueLinesIndex);
        }

        private void Button_scroll_up_Click(object sender, EventArgs e)
        {
            if (dialogueLinesIndex > 0)
                Display(--dialogueLinesIndex);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mono.WriteListToArray())
                    ((form_parent)MdiParent).updateStatus(Settings.getString(body + "Save"));
            }
            catch (Exception ee)
            { ErrorMessage.Show(Settings.getString(body + "NoSave") + "\n" + ee); }
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_monologue_help>().Any())
            {
                form_monologue_help frm = new form_monologue_help();
                frm.MdiParent = MdiParent;
                frm.Show();
            }
        }

        private void Button_help_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_monologue_help>().Any())
            {
                form_monologue_help frm = new form_monologue_help();
                frm.MdiParent = MdiParent;
                frm.Show();
            }
        }

        private void Button_save_Click(object sender, EventArgs e)
        {

        }

    }
}
