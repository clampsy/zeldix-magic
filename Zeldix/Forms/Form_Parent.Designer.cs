﻿namespace Zeldix
{
    partial class form_parent
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }
        public new void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_parent));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_file = new System.Windows.Forms.ToolStripDropDownButton();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel_window = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuScreensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hexViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel_maps = new System.Windows.Forms.ToolStripDropDownButton();
            this.dungeonEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dungeonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dungeonMapsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dungeonPropertiesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.overworldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worldMapsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton_text = new System.Windows.Forms.ToolStripDropDownButton();
            this.monologuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel_gfx = new System.Windows.Forms.ToolStripDropDownButton();
            this.cHRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zCompressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip_gfx_scheme = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel_sound = new System.Windows.Forms.ToolStripDropDownButton();
            this.musicToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDown_tools = new System.Windows.Forms.ToolStripDropDownButton();
            this.addressTranslatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ComboBox_language = new System.Windows.Forms.ToolStripComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_file,
            this.toolStripLabel_window,
            this.toolStripLabel_maps,
            this.toolStripDropDownButton_text,
            this.toolStripLabel_gfx,
            this.toolStripLabel_sound,
            this.dropDown_tools,
            this.toolStripLabel5,
            this.ComboBox_language});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(784, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripLabel_file
            // 
            this.toolStripLabel_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripLabel_file.Name = "toolStripLabel_file";
            this.toolStripLabel_file.Size = new System.Drawing.Size(38, 22);
            this.toolStripLabel_file.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.close);
            // 
            // toolStripLabel_window
            // 
            this.toolStripLabel_window.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuScreensToolStripMenuItem,
            this.hexViewerToolStripMenuItem,
            this.spritesToolStripMenuItem});
            this.toolStripLabel_window.Enabled = false;
            this.toolStripLabel_window.Name = "toolStripLabel_window";
            this.toolStripLabel_window.Size = new System.Drawing.Size(64, 22);
            this.toolStripLabel_window.Text = "Window";
            // 
            // menuScreensToolStripMenuItem
            // 
            this.menuScreensToolStripMenuItem.Name = "menuScreensToolStripMenuItem";
            this.menuScreensToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.menuScreensToolStripMenuItem.Text = "Menu screens";
            this.menuScreensToolStripMenuItem.Click += new System.EventHandler(this.menuScreensToolStripMenuItem_Click);
            // 
            // hexViewerToolStripMenuItem
            // 
            this.hexViewerToolStripMenuItem.Name = "hexViewerToolStripMenuItem";
            this.hexViewerToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.hexViewerToolStripMenuItem.Text = "Hex Viewer";
            this.hexViewerToolStripMenuItem.Click += new System.EventHandler(this.hexViewerToolStripMenuItem_Click_1);
            // 
            // spritesToolStripMenuItem
            // 
            this.spritesToolStripMenuItem.Name = "spritesToolStripMenuItem";
            this.spritesToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.spritesToolStripMenuItem.Text = "Sprites";
            this.spritesToolStripMenuItem.Click += new System.EventHandler(this.spritesToolStripMenuItem_Click_1);
            // 
            // toolStripLabel_maps
            // 
            this.toolStripLabel_maps.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dungeonEditorToolStripMenuItem,
            this.dungeonsToolStripMenuItem,
            this.dungeonMapsToolStripMenuItem1,
            this.dungeonPropertiesToolStripMenuItem1,
            this.toolStripSeparator1,
            this.overworldToolStripMenuItem,
            this.worldMapsToolStripMenuItem});
            this.toolStripLabel_maps.Enabled = false;
            this.toolStripLabel_maps.Name = "toolStripLabel_maps";
            this.toolStripLabel_maps.Size = new System.Drawing.Size(49, 22);
            this.toolStripLabel_maps.Text = "Maps";
            // 
            // dungeonEditorToolStripMenuItem
            // 
            this.dungeonEditorToolStripMenuItem.Name = "dungeonEditorToolStripMenuItem";
            this.dungeonEditorToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.dungeonEditorToolStripMenuItem.Text = "Dungeon Editor";
            this.dungeonEditorToolStripMenuItem.Click += new System.EventHandler(this.dungeonEditorToolStripMenuItem_Click);
            // 
            // dungeonsToolStripMenuItem
            // 
            this.dungeonsToolStripMenuItem.Name = "dungeonsToolStripMenuItem";
            this.dungeonsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.dungeonsToolStripMenuItem.Text = "Dungeon Repointer";
            this.dungeonsToolStripMenuItem.Click += new System.EventHandler(this.dungeonsToolStripMenuItem_Click);
            // 
            // dungeonMapsToolStripMenuItem1
            // 
            this.dungeonMapsToolStripMenuItem1.Name = "dungeonMapsToolStripMenuItem1";
            this.dungeonMapsToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.dungeonMapsToolStripMenuItem1.Text = "Dungeon Maps";
            // 
            // dungeonPropertiesToolStripMenuItem1
            // 
            this.dungeonPropertiesToolStripMenuItem1.Name = "dungeonPropertiesToolStripMenuItem1";
            this.dungeonPropertiesToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.dungeonPropertiesToolStripMenuItem1.Text = "Dungeon Properties";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // overworldToolStripMenuItem
            // 
            this.overworldToolStripMenuItem.Name = "overworldToolStripMenuItem";
            this.overworldToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.overworldToolStripMenuItem.Text = "Overworld";
            this.overworldToolStripMenuItem.Click += new System.EventHandler(this.overworldToolStripMenuItem_Click);
            // 
            // worldMapsToolStripMenuItem
            // 
            this.worldMapsToolStripMenuItem.Name = "worldMapsToolStripMenuItem";
            this.worldMapsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.worldMapsToolStripMenuItem.Text = "Overworld Maps";
            // 
            // toolStripDropDownButton_text
            // 
            this.toolStripDropDownButton_text.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_text.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monologuesToolStripMenuItem,
            this.creditsToolStripMenuItem,
            this.fontToolStripMenuItem});
            this.toolStripDropDownButton_text.Enabled = false;
            this.toolStripDropDownButton_text.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_text.Image")));
            this.toolStripDropDownButton_text.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_text.Name = "toolStripDropDownButton_text";
            this.toolStripDropDownButton_text.Size = new System.Drawing.Size(41, 22);
            this.toolStripDropDownButton_text.Text = "Text";
            // 
            // monologuesToolStripMenuItem
            // 
            this.monologuesToolStripMenuItem.Name = "monologuesToolStripMenuItem";
            this.monologuesToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.monologuesToolStripMenuItem.Text = "Monologues";
            this.monologuesToolStripMenuItem.Click += new System.EventHandler(this.monologuesToolStripMenuItem_Click);
            // 
            // creditsToolStripMenuItem
            // 
            this.creditsToolStripMenuItem.Name = "creditsToolStripMenuItem";
            this.creditsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.creditsToolStripMenuItem.Text = "Credits";
            this.creditsToolStripMenuItem.Click += new System.EventHandler(this.creditsToolStripMenuItem_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.fontToolStripMenuItem.Text = "Font";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.fontToolStripMenuItem_Click);
            // 
            // toolStripLabel_gfx
            // 
            this.toolStripLabel_gfx.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cHRToolStripMenuItem,
            this.zCompressToolStripMenuItem,
            this.ToolStrip_gfx_scheme});
            this.toolStripLabel_gfx.Enabled = false;
            this.toolStripLabel_gfx.Name = "toolStripLabel_gfx";
            this.toolStripLabel_gfx.Size = new System.Drawing.Size(66, 22);
            this.toolStripLabel_gfx.Text = "Graphics";
            // 
            // cHRToolStripMenuItem
            // 
            this.cHRToolStripMenuItem.Name = "cHRToolStripMenuItem";
            this.cHRToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cHRToolStripMenuItem.Text = "CHR";
            this.cHRToolStripMenuItem.Click += new System.EventHandler(this.cHRToolStripMenuItem_Click);
            // 
            // zCompressToolStripMenuItem
            // 
            this.zCompressToolStripMenuItem.Name = "zCompressToolStripMenuItem";
            this.zCompressToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.zCompressToolStripMenuItem.Text = "ZCompress";
            this.zCompressToolStripMenuItem.Click += new System.EventHandler(this.zCompressToolStripMenuItem_Click);
            // 
            // ToolStrip_gfx_scheme
            // 
            this.ToolStrip_gfx_scheme.Name = "ToolStrip_gfx_scheme";
            this.ToolStrip_gfx_scheme.Size = new System.Drawing.Size(164, 22);
            this.ToolStrip_gfx_scheme.Text = "Graphic schemes";
            this.ToolStrip_gfx_scheme.Click += new System.EventHandler(this.ToolStrip_gfx_scheme_Click);
            // 
            // toolStripLabel_sound
            // 
            this.toolStripLabel_sound.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.musicToolStripMenuItem1});
            this.toolStripLabel_sound.Enabled = false;
            this.toolStripLabel_sound.Name = "toolStripLabel_sound";
            this.toolStripLabel_sound.Size = new System.Drawing.Size(54, 22);
            this.toolStripLabel_sound.Text = "Sound";
            // 
            // musicToolStripMenuItem1
            // 
            this.musicToolStripMenuItem1.Name = "musicToolStripMenuItem1";
            this.musicToolStripMenuItem1.Size = new System.Drawing.Size(106, 22);
            this.musicToolStripMenuItem1.Text = "Music";
            this.musicToolStripMenuItem1.Click += new System.EventHandler(this.musicToolStripMenuItem1_Click);
            // 
            // dropDown_tools
            // 
            this.dropDown_tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addressTranslatorToolStripMenuItem});
            this.dropDown_tools.Name = "dropDown_tools";
            this.dropDown_tools.Size = new System.Drawing.Size(48, 22);
            this.dropDown_tools.Text = "Tools";
            // 
            // addressTranslatorToolStripMenuItem
            // 
            this.addressTranslatorToolStripMenuItem.Name = "addressTranslatorToolStripMenuItem";
            this.addressTranslatorToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.addressTranslatorToolStripMenuItem.Text = "Address Calculator";
            this.addressTranslatorToolStripMenuItem.Click += new System.EventHandler(this.addressTranslatorToolStripMenuItem_Click);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(45, 22);
            this.toolStripLabel5.Text = "Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "Contents";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // ComboBox_language
            // 
            this.ComboBox_language.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_language.Enabled = false;
            this.ComboBox_language.Name = "ComboBox_language";
            this.ComboBox_language.Size = new System.Drawing.Size(121, 25);
            this.ComboBox_language.Visible = false;
            this.ComboBox_language.SelectedIndexChanged += new System.EventHandler(this.ComboBox_language_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.label_status});
            this.statusStrip1.Location = new System.Drawing.Point(0, 739);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // label_status
            // 
            this.label_status.BackColor = System.Drawing.Color.Transparent;
            this.label_status.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(118, 17);
            this.label_status.Text = "toolStripStatusLabel1";
            // 
            // form_parent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(784, 761);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.Name = "form_parent";
            this.Opacity = 1D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zeldix Magic";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.form_parent_FormClosed);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel_file;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel_window;
        private System.Windows.Forms.ToolStripMenuItem menuScreensToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel5;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton dropDown_tools;
        private System.Windows.Forms.ToolStripMenuItem addressTranslatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hexViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel_maps;
        private System.Windows.Forms.ToolStripMenuItem dungeonsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overworldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dungeonMapsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dungeonPropertiesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem worldMapsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel_sound;
        private System.Windows.Forms.ToolStripMenuItem musicToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem spritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox ComboBox_language;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_text;
        private System.Windows.Forms.ToolStripMenuItem monologuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel_gfx;
        private System.Windows.Forms.ToolStripMenuItem cHRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zCompressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStrip_gfx_scheme;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel label_status;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dungeonEditorToolStripMenuItem;
    }
}