﻿namespace Zeldix.Forms
{
    partial class form_menuScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_data = new System.Windows.Forms.RichTextBox();
            this.button_save = new System.Windows.Forms.Button();
            this.textBox_address1 = new System.Windows.Forms.TextBox();
            this.flowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.richTextBox_pointers = new System.Windows.Forms.RichTextBox();
            this.label_raw_pointers = new System.Windows.Forms.Label();
            this.richTextBox_data2 = new System.Windows.Forms.RichTextBox();
            this.radioButton_part1 = new System.Windows.Forms.RadioButton();
            this.radioButton_part2 = new System.Windows.Forms.RadioButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox_address2 = new System.Windows.Forms.TextBox();
            this.radioButton_part3 = new System.Windows.Forms.RadioButton();
            this.textBox_address3 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox_data
            // 
            this.richTextBox_data.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_data.Location = new System.Drawing.Point(1354, -214);
            this.richTextBox_data.Name = "richTextBox_data";
            this.richTextBox_data.Size = new System.Drawing.Size(291, 261);
            this.richTextBox_data.TabIndex = 3;
            this.richTextBox_data.Text = "";
            // 
            // button_save
            // 
            this.button_save.Enabled = false;
            this.button_save.Location = new System.Drawing.Point(358, 39);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(75, 23);
            this.button_save.TabIndex = 6;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            // 
            // textBox_address1
            // 
            this.textBox_address1.Location = new System.Drawing.Point(58, 3);
            this.textBox_address1.Name = "textBox_address1";
            this.textBox_address1.ReadOnly = true;
            this.textBox_address1.Size = new System.Drawing.Size(98, 20);
            this.textBox_address1.TabIndex = 9;
            // 
            // flowPanel
            // 
            this.flowPanel.Location = new System.Drawing.Point(13, 13);
            this.flowPanel.Name = "flowPanel";
            this.flowPanel.Size = new System.Drawing.Size(120, 343);
            this.flowPanel.TabIndex = 10;
            // 
            // richTextBox_pointers
            // 
            this.richTextBox_pointers.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_pointers.Location = new System.Drawing.Point(439, 169);
            this.richTextBox_pointers.Name = "richTextBox_pointers";
            this.richTextBox_pointers.ReadOnly = true;
            this.richTextBox_pointers.Size = new System.Drawing.Size(208, 80);
            this.richTextBox_pointers.TabIndex = 13;
            this.richTextBox_pointers.Text = "";
            // 
            // label_raw_pointers
            // 
            this.label_raw_pointers.AutoSize = true;
            this.label_raw_pointers.Location = new System.Drawing.Point(439, 153);
            this.label_raw_pointers.Name = "label_raw_pointers";
            this.label_raw_pointers.Size = new System.Drawing.Size(75, 13);
            this.label_raw_pointers.TabIndex = 14;
            this.label_raw_pointers.Text = "Raw pointers: ";
            // 
            // richTextBox_data2
            // 
            this.richTextBox_data2.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_data2.Location = new System.Drawing.Point(147, 104);
            this.richTextBox_data2.Name = "richTextBox_data2";
            this.richTextBox_data2.ReadOnly = true;
            this.richTextBox_data2.Size = new System.Drawing.Size(286, 249);
            this.richTextBox_data2.TabIndex = 15;
            this.richTextBox_data2.Text = "";
            // 
            // radioButton_part1
            // 
            this.radioButton_part1.AutoSize = true;
            this.radioButton_part1.Enabled = false;
            this.radioButton_part1.Location = new System.Drawing.Point(3, 3);
            this.radioButton_part1.Name = "radioButton_part1";
            this.radioButton_part1.Size = new System.Drawing.Size(49, 17);
            this.radioButton_part1.TabIndex = 16;
            this.radioButton_part1.TabStop = true;
            this.radioButton_part1.Text = "BG 1";
            this.radioButton_part1.UseVisualStyleBackColor = true;
            this.radioButton_part1.CheckedChanged += new System.EventHandler(this.radioButton_part1_CheckedChanged);
            // 
            // radioButton_part2
            // 
            this.radioButton_part2.AutoSize = true;
            this.radioButton_part2.Enabled = false;
            this.radioButton_part2.Location = new System.Drawing.Point(3, 29);
            this.radioButton_part2.Name = "radioButton_part2";
            this.radioButton_part2.Size = new System.Drawing.Size(49, 17);
            this.radioButton_part2.TabIndex = 17;
            this.radioButton_part2.TabStop = true;
            this.radioButton_part2.Text = "BG 2";
            this.radioButton_part2.UseVisualStyleBackColor = true;
            this.radioButton_part2.CheckedChanged += new System.EventHandler(this.radioButton_part2_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.radioButton_part1);
            this.flowLayoutPanel1.Controls.Add(this.textBox_address1);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_part2);
            this.flowLayoutPanel1.Controls.Add(this.textBox_address2);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_part3);
            this.flowLayoutPanel1.Controls.Add(this.textBox_address3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(147, 13);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(164, 85);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // textBox_address2
            // 
            this.textBox_address2.Location = new System.Drawing.Point(58, 29);
            this.textBox_address2.Name = "textBox_address2";
            this.textBox_address2.ReadOnly = true;
            this.textBox_address2.Size = new System.Drawing.Size(98, 20);
            this.textBox_address2.TabIndex = 20;
            // 
            // radioButton_part3
            // 
            this.radioButton_part3.AutoSize = true;
            this.radioButton_part3.Enabled = false;
            this.radioButton_part3.Location = new System.Drawing.Point(3, 55);
            this.radioButton_part3.Name = "radioButton_part3";
            this.radioButton_part3.Size = new System.Drawing.Size(49, 17);
            this.radioButton_part3.TabIndex = 18;
            this.radioButton_part3.TabStop = true;
            this.radioButton_part3.Text = "BG 3";
            this.radioButton_part3.UseVisualStyleBackColor = true;
            this.radioButton_part3.CheckedChanged += new System.EventHandler(this.radioButton_part3_CheckedChanged);
            // 
            // textBox_address3
            // 
            this.textBox_address3.Location = new System.Drawing.Point(58, 55);
            this.textBox_address3.Name = "textBox_address3";
            this.textBox_address3.ReadOnly = true;
            this.textBox_address3.Size = new System.Drawing.Size(98, 20);
            this.textBox_address3.TabIndex = 19;
            // 
            // form_menuScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 365);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.richTextBox_data2);
            this.Controls.Add(this.label_raw_pointers);
            this.Controls.Add(this.richTextBox_pointers);
            this.Controls.Add(this.flowPanel);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.richTextBox_data);
            this.Name = "form_menuScreen";
            this.Text = "Menu Screen";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox richTextBox_data;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.TextBox textBox_address1;
        private System.Windows.Forms.FlowLayoutPanel flowPanel;
        private System.Windows.Forms.RichTextBox richTextBox_pointers;
        private System.Windows.Forms.Label label_raw_pointers;
        private System.Windows.Forms.RichTextBox richTextBox_data2;
        private System.Windows.Forms.RadioButton radioButton_part1;
        private System.Windows.Forms.RadioButton radioButton_part2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton radioButton_part3;
        private System.Windows.Forms.TextBox textBox_address2;
        private System.Windows.Forms.TextBox textBox_address3;
    }
}