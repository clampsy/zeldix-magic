﻿/*
 * Class        :   form_address_calc.cs
 * Author       :   clampsy
 * Description  :   
 */

using System;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;

namespace Zeldix.Forms
{
    internal partial class form_address_calc : form_template
    {
        private const string blank = "";

        internal form_address_calc()
        { InitializeComponent(); }
        private void button_clear_Click(object sender, EventArgs e)
        { textBox_output.Text = textBox_input.Text = blank; }
        private void clear() { textBox_output.Text = blank; }
        private void radioButton_SneesToPc_CheckedChanged(object sender, EventArgs e)
        { clear(); }
        private void button_calc_Click(object sender, EventArgs e)
        {
            try
            {
                string
                    input = MyMath.removePrefix(textBox_input.Text),
                    output = blank;

                if (MyMath.isHex(input))
                {
                    if (radioButton_SnesToPc.Checked == true)
                        output = MyMath.decToHex(AddressLoROM.snesToPc(MyMath.hexToDec(input)));
                    else if (radioButton_pcToSnes.Checked == true)
                        output = MyMath.decToHex(AddressLoROM.pcToSnes(MyMath.hexToDec(input)));
                    else
                        output = "Error";
                    textBox_output.Text = output;
                }
                else ErrorMessage.Show("Error: The input is not a valid hex address");
            } catch (Exception ee) { ErrorMessage.Show(ee); }
        }

        private void textBox_input_TextChanged(object sender, EventArgs e)
        {
            string input = textBox_input.Text;
            button_calc.Enabled = (input.Length >= MyMath.hasPrefix(input) + 6) /*&& MyMath.isHex(textBox_input.Text)*/;
            button_clear.Enabled = !(input == "" && textBox_output.Text == "");
        }

        private void form_address_calc_Load(object sender, EventArgs e)
        {
            label_input.Text = Settings.getString("Input");
            label_output.Text = Settings.getString("Output");
            Text = Settings.getString("AddressCalc");

            radioButton_SnesToPc.Text = Settings.getString("SNESToPc");
            radioButton_pcToSnes.Text = Settings.getString("PcToSNES");

            button_calc.Text = Settings.getString("Calculate");
            button_clear.Text = Settings.getString("Clear");

        }
    }
}