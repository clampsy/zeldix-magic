﻿/*
 * Class        :   form_parent.cs
 * Author       :   clampsy
 * Description  :   
 */
/*
 * Copyright (C) 2017  clampsy
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using AlttpSpriteEditor;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;
using Zeldix.Forms;
using Zeldix.Forms.Dungeon_karkat;

namespace Zeldix
{
    /// <summary>
    /// The main window form that contains all the other windows.
    /// </summary>
    internal partial class form_parent : form_template
    {
        private string realFilePath;

        /// <summary>
        /// Your standard constructor
        /// </summary>
        internal form_parent(): base()
        {
            //double temp = Opacity;  //Really,
            InitializeComponent();  //Microsoft?
            //Opacity = temp;         //REALLY?

            label_status.Text = Settings.getString(this, "load_rom");
            loadText();

            foreach (string s in Settings.languageName)
                ComboBox_language.Items.Add(s);
            ComboBox_language.SelectedIndex = 0;
        }

        private void loadText()
        {
            Text = Settings.getString("ZeldixMagic") + (string.IsNullOrEmpty(realFilePath)? "" : " - " + realFilePath);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripLabel_window.Enabled =
            toolStripLabel_maps.Enabled =
            toolStripLabel_gfx.Enabled =
            toolStripLabel_sound.Enabled =
            toolStripDropDownButton_text.Enabled = openFile();
        }

        private bool openFile()
        {
            bool pass = false;
            OpenFileDialog FD = new OpenFileDialog();
            FD.Filter = "SNES ROM | *.sfc; *.smc";

            if (FD.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(FD.FileName))
                {
                    try
                    {
                        realFilePath = FD.FileName;
                        RomIO.constructor(realFilePath);
                        RegionId.generateRegion();
                        pass = true;

                        Joke jk = new Joke();
                        string str = jk.checkDate1();
                        if (str != "")
                            MessageBox.Show(jk.checkDate1(), jk.getTitle());
                        loadText();
                        if (RomIO.isChecksumGood())
                            label_status.Text = Settings.getString(this, "loaded_rom_check_good");
                        else label_status.Text = Settings.getString(this, "loaded_rom_check_bad");

                        if (RomIO.isHeaderless())
                            label_status.Text += Settings.getString(this, "notice_noheader");
                        else label_status.Text += Settings.getString(this, "notice_headered");

                        
                        
                    }
                    catch (IOException)
                    { ErrorMessage.Show("The file is open in another program. Close it."); }
                }
                else ErrorMessage.Show("File don't exist, yo");
            }
            return pass;
        }

        private void close(object sender, EventArgs e)
        { Close(); }

        private void form_parent_FormClosed(object sender, FormClosedEventArgs e)
        { close(null, null); }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (e.CloseReason == CloseReason.WindowsShutDown)
                return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, Settings.getString("Closing_text"), Settings.getString("Closing"), MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    //fadeout();
                    Process.GetCurrentProcess().Kill();
                    break;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(realFilePath))
            {
                try
                {
                    if (RomIO.writeToROM())
                        label_status.Text = Settings.getString(this, "saved_fixed_check");
                    else label_status.Text = Settings.getString(this, "saved_const_check");
                }
                catch (Exception ee) { ErrorMessage.Show("Error writing to file:\n" + ee); }
            }
            else ErrorMessage.Show("File don't exist, yo");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Settings.getString(this, "notice"), Settings.getString(this, "about"));
        }
        

        private void addressTranslatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_address_calc>().Any())
            {
                form_address_calc frm = new form_address_calc();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void hexViewerToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_hexEditor>().Any())
            {
                form_hexEditor frm = new form_hexEditor();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void menuScreensToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_menuScreen>().Any())
            {
                form_menuScreen frm = new form_menuScreen();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void dungeonsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_dungeon_repointer>().Any())
            {
                form_dungeon_repointer frm = new form_dungeon_repointer();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void overworldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_overworld>().Any())
            {
                form_overworld frm = new form_overworld();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void musicToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        public void updateStatus(string s)
        { label_status.Text = s; }

        private void spritesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_Form1>().Any())
            {
                form_Form1 frm = new form_Form1();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void ComboBox_language_SelectedIndexChanged(object sender, EventArgs e)
        { Settings.setLanguage(ComboBox_language.SelectedItem.ToString()); }

        private void creditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            if (!Application.OpenForms.OfType<form_credits_text>().Any())
            {
                form_credits_text frm = new form_credits_text();
                frm.MdiParent = this;
                frm.Show();
            }*/
        }

        private void monologuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_monologue>().Any())
            {
                form_monologue frm = new form_monologue();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void cHRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_graphics>().Any())
            {
                form_graphics frm = new form_graphics();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void zCompressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_compress>().Any())
            {
                form_compress frm = new form_compress();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void ToolStrip_gfx_scheme_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_GFXScheme>().Any())
            {
                form_GFXScheme frm = new form_GFXScheme();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            if (!Application.OpenForms.OfType<form_GFXScheme>().Any())
            {
                form_editor frm = new form_editor();
                frm.MdiParent = this;
                frm.Show();
            }*/
        }

        private void dungeonEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Application.OpenForms.OfType<form_Sandbox>().Any())
            {
                form_Sandbox frm = new form_Sandbox();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void sandboxToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
    }
}