﻿/*
 * Class        :   form_hexViewer.cs
 * Author       :   clampsy
 * Description  :   A poorly coded and designed hex viewer
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using Zeldix.Classes.Forms;
using Zeldix.Classes.Crap;
using Zeldix.Classes.HexBox;

namespace Zeldix.Forms
{
    public partial class form_hexEditor : form_template
    {
        private const short blockLW = 16;

        private const int
            lineSize = 0x10,
            pageSize = lineSize * 0x10;

        private const string
            space = " ",
            zero = "0";

        internal form_hexEditor()
        {
            InitializeComponent();
            hexBox.ByteProvider = new DynamicByteProvider(RomIO.read(0, RomIO.size));
            hexBox.Font = new Font(SystemFonts.MessageBoxFont.FontFamily, SystemFonts.MessageBoxFont.Size, SystemFonts.MessageBoxFont.Style);

            var defConverter = new DefaultByteCharConverter();
            ToolStripMenuItem miDefault = new ToolStripMenuItem();
            miDefault.Text = defConverter.ToString();
            miDefault.Tag = defConverter;
            miDefault.Click += new EventHandler(encodingMenuItem_Clicked);

            var ebcdicConverter = new EbcdicByteCharProvider();
            ToolStripMenuItem miEbcdic = new ToolStripMenuItem();
            miEbcdic.Text = ebcdicConverter.ToString();
            miEbcdic.Tag = ebcdicConverter;
            miEbcdic.Click += new EventHandler(encodingMenuItem_Clicked);

            encodingToolStripComboBox.Items.Add(defConverter);
            encodingToolStripComboBox.Items.Add(ebcdicConverter);
            encodingToolStripComboBox.SelectedIndex = 0;
        }

        private void encodingMenuItem_Clicked(object sender, EventArgs e)
        {
            var converter = ((ToolStripMenuItem)sender).Tag;
            encodingToolStripComboBox.SelectedItem = converter;
        }

        private void button_go_Click(object sender, EventArgs e)
        {
            string text = textBox_adressGoto.Text.Replace("0x", "");
            if (MyMath.isHex(textBox_adressGoto.Text))
            {
                int hex = MyMath.hexToDec(text);

                hexBox.SelectionStart = hex;
                hexBox.SelectionLength = 1;
                hexBox.Focus();
            }
            else ErrorMessage.Show("Not a hex string.");
        }
    }
}