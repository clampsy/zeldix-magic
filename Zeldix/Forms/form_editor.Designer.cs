﻿namespace Zeldix.Forms
{
    partial class form_editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.pixelBox1 = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pixelBox1
            // 
            this.pixelBox1.Image = null;
            this.pixelBox1.Location = new System.Drawing.Point(13, 13);
            this.pixelBox1.Name = "pixelBox1";
            this.pixelBox1.Size = new System.Drawing.Size(259, 236);
            this.pixelBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pixelBox1.TabIndex = 0;
            this.pixelBox1.TabStop = false;
            // 
            // form_editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 352);
            this.Controls.Add(this.pixelBox1);
            this.Name = "form_editor";
            this.Text = "Form_editor";
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Classes.GraphicBoxSelect.PixelBox pixelBox1;
    }
}