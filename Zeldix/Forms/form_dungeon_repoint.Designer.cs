﻿namespace Zeldix.Classes.Forms
{
    partial class form_dungeon_repointer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
        {
            this.textBox_rm_pnt = new System.Windows.Forms.TextBox();
            this.textBox_rm_pnt_mv = new System.Windows.Forms.TextBox();
            this.label_rm_pnt = new System.Windows.Forms.Label();
            this.label_rm_pnt_mv = new System.Windows.Forms.Label();
            this.button_rm_pnt_go = new System.Windows.Forms.Button();
            this.button_sp_pnt_go = new System.Windows.Forms.Button();
            this.label_sp_pnt_mv = new System.Windows.Forms.Label();
            this.label_sprite_point = new System.Windows.Forms.Label();
            this.textBox_sp_pnt_mv = new System.Windows.Forms.TextBox();
            this.textBox_sp_pnt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox_rm_pnt
            // 
            this.textBox_rm_pnt.Enabled = false;
            this.textBox_rm_pnt.Location = new System.Drawing.Point(130, 6);
            this.textBox_rm_pnt.Name = "textBox_rm_pnt";
            this.textBox_rm_pnt.Size = new System.Drawing.Size(100, 20);
            this.textBox_rm_pnt.TabIndex = 0;
            // 
            // textBox_rm_pnt_mv
            // 
            this.textBox_rm_pnt_mv.Location = new System.Drawing.Point(130, 32);
            this.textBox_rm_pnt_mv.Name = "textBox_rm_pnt_mv";
            this.textBox_rm_pnt_mv.Size = new System.Drawing.Size(100, 20);
            this.textBox_rm_pnt_mv.TabIndex = 1;
            // 
            // label_rm_pnt
            // 
            this.label_rm_pnt.AutoSize = true;
            this.label_rm_pnt.Location = new System.Drawing.Point(12, 9);
            this.label_rm_pnt.Name = "label_rm_pnt";
            this.label_rm_pnt.Size = new System.Drawing.Size(118, 13);
            this.label_rm_pnt.TabIndex = 2;
            this.label_rm_pnt.Text = "Room pointers location:";
            // 
            // label_rm_pnt_mv
            // 
            this.label_rm_pnt_mv.AutoSize = true;
            this.label_rm_pnt_mv.Location = new System.Drawing.Point(78, 35);
            this.label_rm_pnt_mv.Name = "label_rm_pnt_mv";
            this.label_rm_pnt_mv.Size = new System.Drawing.Size(49, 13);
            this.label_rm_pnt_mv.TabIndex = 3;
            this.label_rm_pnt_mv.Text = "Move to:";
            // 
            // button_rm_pnt_go
            // 
            this.button_rm_pnt_go.Location = new System.Drawing.Point(236, 30);
            this.button_rm_pnt_go.Name = "button_rm_pnt_go";
            this.button_rm_pnt_go.Size = new System.Drawing.Size(75, 23);
            this.button_rm_pnt_go.TabIndex = 4;
            this.button_rm_pnt_go.Text = "Go!";
            this.button_rm_pnt_go.UseVisualStyleBackColor = true;
            this.button_rm_pnt_go.Click += new System.EventHandler(this.button_room_repoint_go_Click);
            // 
            // button_sp_pnt_go
            // 
            this.button_sp_pnt_go.Location = new System.Drawing.Point(236, 108);
            this.button_sp_pnt_go.Name = "button_sp_pnt_go";
            this.button_sp_pnt_go.Size = new System.Drawing.Size(75, 23);
            this.button_sp_pnt_go.TabIndex = 9;
            this.button_sp_pnt_go.Text = "Go!";
            this.button_sp_pnt_go.UseVisualStyleBackColor = true;
            this.button_sp_pnt_go.Click += new System.EventHandler(this.button_sp_pnt_go_Click);
            // 
            // label_sp_pnt_mv
            // 
            this.label_sp_pnt_mv.AutoSize = true;
            this.label_sp_pnt_mv.Location = new System.Drawing.Point(78, 113);
            this.label_sp_pnt_mv.Name = "label_sp_pnt_mv";
            this.label_sp_pnt_mv.Size = new System.Drawing.Size(49, 13);
            this.label_sp_pnt_mv.TabIndex = 8;
            this.label_sp_pnt_mv.Text = "Move to:";
            // 
            // label_sprite_point
            // 
            this.label_sprite_point.AutoSize = true;
            this.label_sprite_point.Location = new System.Drawing.Point(7, 87);
            this.label_sprite_point.Name = "label_sprite_point";
            this.label_sprite_point.Size = new System.Drawing.Size(117, 13);
            this.label_sprite_point.TabIndex = 7;
            this.label_sprite_point.Text = "Sprite pointers location:";
            // 
            // textBox_sp_pnt_mv
            // 
            this.textBox_sp_pnt_mv.Location = new System.Drawing.Point(130, 110);
            this.textBox_sp_pnt_mv.Name = "textBox_sp_pnt_mv";
            this.textBox_sp_pnt_mv.Size = new System.Drawing.Size(100, 20);
            this.textBox_sp_pnt_mv.TabIndex = 6;
            // 
            // textBox_sp_pnt
            // 
            this.textBox_sp_pnt.Enabled = false;
            this.textBox_sp_pnt.Location = new System.Drawing.Point(130, 84);
            this.textBox_sp_pnt.Name = "textBox_sp_pnt";
            this.textBox_sp_pnt.Size = new System.Drawing.Size(100, 20);
            this.textBox_sp_pnt.TabIndex = 5;
            // 
            // form_dungeon_repointer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 426);
            this.Controls.Add(this.button_sp_pnt_go);
            this.Controls.Add(this.label_sp_pnt_mv);
            this.Controls.Add(this.label_sprite_point);
            this.Controls.Add(this.textBox_sp_pnt_mv);
            this.Controls.Add(this.textBox_sp_pnt);
            this.Controls.Add(this.button_rm_pnt_go);
            this.Controls.Add(this.label_rm_pnt_mv);
            this.Controls.Add(this.label_rm_pnt);
            this.Controls.Add(this.textBox_rm_pnt_mv);
            this.Controls.Add(this.textBox_rm_pnt);
            this.Name = "form_dungeon_repointer";
            this.Text = "Dungeons Repointer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_rm_pnt;
        private System.Windows.Forms.TextBox textBox_rm_pnt_mv;
        private System.Windows.Forms.Label label_rm_pnt;
        private System.Windows.Forms.Label label_rm_pnt_mv;
        private System.Windows.Forms.Button button_rm_pnt_go;
        private System.Windows.Forms.Button button_sp_pnt_go;
        private System.Windows.Forms.Label label_sp_pnt_mv;
        private System.Windows.Forms.Label label_sprite_point;
        private System.Windows.Forms.TextBox textBox_sp_pnt_mv;
        private System.Windows.Forms.TextBox textBox_sp_pnt;
    }
}