﻿using System;
using System.Windows.Forms;
using Zeldix.Classes.Forms;
using Zeldix.Classes.GraphicBoxSelect;
using Zeldix.Classes.Overworld;
using static Zeldix.Classes.Overworld.GFXSchemes;
namespace Zeldix.Forms
{
    public partial class form_GFXScheme : form_template
    {
        private const string
            seperator = "_",
            s_numeric = "n",
            s_picture = "p";
        private blockSetEntry[][] entries;

        private struct blockSetEntry
        {
            public NumericUpDown A_numeric;
            public PixelBox pix;
        }

        private NumericUpDown[] numeric_controls;

        public form_GFXScheme()
        {
            entries = new blockSetEntry[GFXSchemes.numberOfBlocksetTypes][];

            InitializeComponent();

            numeric_controls = new NumericUpDown[]
            {
                numeric_main,
                numeric_room,
                numeric_sprite
            };

            if (numeric_controls.Length != GFXSchemes.numberOfBlocksetTypes) //sanity check
                throw new Exception();

            for (int i = 0; i < numeric_controls.Length; i++)
                numeric_controls[i].Maximum = GFXSchemes.blocksets[i].numberOfBlockSets - 1;

            string temp;
            byte entryVal;
            NumericUpDown numeric;
            PixelBox pix;
            BlockSetTemplate currentBlockSet;
            FlowLayoutPanel flow;

            for (int blockSetType = 0; blockSetType < entries.Length; blockSetType++)
            {
                currentBlockSet = (GFXSchemes.blocksets[blockSetType]);
                entries[blockSetType] = new blockSetEntry[currentBlockSet.entrySize];

                for (int blockSetEntry = 0; blockSetEntry < currentBlockSet.entrySize; blockSetEntry++)
                {
                    entryVal = currentBlockSet.getScheme(0, blockSetEntry);
                    temp = blockSetType + seperator + blockSetEntry;

                    numeric = new NumericUpDown();
                    numeric.Maximum = GFXSchemes.maxValOfEntries;
                    numeric.Value = entryVal;
                    numeric.Width = 50;
                    numeric.ValueChanged += new EventHandler(numeric_entry_ValueChanged);
                    numeric.Name = s_numeric + temp;

                    pix = new PixelBox(3, 3);
                    pix.Image = GFXSchemes.gfxBuffer.getBMP(entryVal);
                    pix.Width = doubleMe(pix.Image.Width);
                    pix.Height = doubleMe(pix.Image.Height);
                    pix.Name = s_picture + temp;
                    pix.SizeMode = PictureBoxSizeMode.Zoom;

                    numeric.Margin = new Padding(3, halfMe(pix.Height) - halfMe(numeric.Height), 3, 3);

                    flow = new FlowLayoutPanel();
                    flow.Width = pix.Width + numeric.Width + 16;
                    flow.BorderStyle = BorderStyle.Fixed3D;
                    flow.Height = pix.Height + pix.Margin.Top + pix.Margin.Bottom + 4;

                    flow.Controls.Add(numeric);
                    flow.Controls.Add(pix);

                    switch (blockSetType)
                    {
                        case 0:     flowPanel_main.Controls.Add(flow);      break;
                        case 1:     flowPanel_room.Controls.Add(flow);      break;
                        case 2:     flowPanel_sprite.Controls.Add(flow);    break;
                        default:    throw new ArgumentException();
                    }

                    entries[blockSetType][blockSetEntry].A_numeric = numeric;
                    entries[blockSetType][blockSetEntry].pix = pix;
                }
            }
        }

        private int doubleMe(int i)
        { return i * 2; }

        private void numeric_main_ValueChanged(object sender, EventArgs e)
        {
            int currentBlockSetType = tabControl.SelectedIndex;
            BlockSetTemplate currentBlockSet = (GFXSchemes.blocksets[currentBlockSetType]);
            blockSetEntry cursor;

            for (int i = 0, b; i < currentBlockSet.entrySize; i++)
            {
                cursor = entries[tabControl.SelectedIndex][i];
                b = blocksets[currentBlockSetType].getScheme((int)numeric_controls[currentBlockSetType].Value, i);
                cursor.pix.Image = GFXSchemes.gfxBuffer.getBMP(b);
                cursor.A_numeric.Value = b;
            }
        }

        private int halfMe(int i)
        { return i / 2; }

        private void numeric_entry_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown num = (NumericUpDown)(sender);
            string name = num.Name;
            byte value = (byte)num.Value;
            int index = int.Parse(name.Substring(name.IndexOf(seperator) + 1)),
                currentScheme = tabControl.SelectedIndex;
            //numeric_main
            GFXSchemes.blocksets[currentScheme].setScheme((int)numeric_controls[currentScheme].Value, index, value);

            PixelBox px = entries[currentScheme][index].pix;

            try
            { px.Image = GFXSchemes.gfxBuffer.getBMP(value); }
            catch
            { px.Image = null; }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Save progress", "Closing", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
                case DialogResult.Yes:
                    GFXSchemes.writeToArray();
                    break;
            }
        }
    }
}