﻿using System.Windows.Forms;
using Zeldix.Classes.Forms;

namespace Zeldix.Forms
{
    partial class form_graphics : form_template
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        private new void InitializeComponent()
        {
            this.textBox_address = new System.Windows.Forms.TextBox();
            this.button_go = new System.Windows.Forms.Button();
            this.label_address = new System.Windows.Forms.Label();
            this.combo_bpp = new System.Windows.Forms.ComboBox();
            this.textBox_current_address = new System.Windows.Forms.TextBox();
            this.button_export = new System.Windows.Forms.Button();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label_current_address = new System.Windows.Forms.Label();
            this.button_import = new System.Windows.Forms.Button();
            this.button_goto_player = new System.Windows.Forms.Button();
            this.button_goto_font = new System.Windows.Forms.Button();
            this.button_goto_misc = new System.Windows.Forms.Button();
            this.combo_palette = new System.Windows.Forms.ComboBox();
            this.checkBox_trans = new System.Windows.Forms.CheckBox();
            this.label_hexVal = new System.Windows.Forms.Label();
            this.label_hex = new System.Windows.Forms.Label();
            this.pixelBox_magnif = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel_arrows = new System.Windows.Forms.FlowLayoutPanel();
            this.button_large_up = new System.Windows.Forms.Button();
            this.button_big_up = new System.Windows.Forms.Button();
            this.button_little_up = new System.Windows.Forms.Button();
            this.button_minu = new System.Windows.Forms.Button();
            this.button_uppu = new System.Windows.Forms.Button();
            this.button_little_down = new System.Windows.Forms.Button();
            this.button_big_down = new System.Windows.Forms.Button();
            this.button_large_down = new System.Windows.Forms.Button();
            this.pixelBox = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.vScrollBar = new System.Windows.Forms.VScrollBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox_actions = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_magnif)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.flowLayoutPanel_arrows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox_actions.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_address
            // 
            this.textBox_address.Location = new System.Drawing.Point(9, 32);
            this.textBox_address.Name = "textBox_address";
            this.textBox_address.Size = new System.Drawing.Size(84, 20);
            this.textBox_address.TabIndex = 0;
            this.textBox_address.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_address_KeyPress);
            // 
            // button_go
            // 
            this.button_go.Enabled = false;
            this.button_go.Location = new System.Drawing.Point(99, 32);
            this.button_go.Name = "button_go";
            this.button_go.Size = new System.Drawing.Size(42, 20);
            this.button_go.TabIndex = 1;
            this.button_go.Text = "Go";
            this.button_go.UseVisualStyleBackColor = true;
            this.button_go.Click += new System.EventHandler(this.button_go_Click);
            // 
            // label_address
            // 
            this.label_address.AutoSize = true;
            this.label_address.Location = new System.Drawing.Point(6, 16);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(35, 13);
            this.label_address.TabIndex = 2;
            this.label_address.Text = "Jump:";
            // 
            // combo_bpp
            // 
            this.combo_bpp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_bpp.FormattingEnabled = true;
            this.combo_bpp.Items.AddRange(new object[] {
            "1BPP",
            "2BPP",
            "3BPP",
            "4BPP"});
            this.combo_bpp.Location = new System.Drawing.Point(87, 21);
            this.combo_bpp.Name = "combo_bpp";
            this.combo_bpp.Size = new System.Drawing.Size(75, 21);
            this.combo_bpp.TabIndex = 2;
            this.combo_bpp.SelectedIndexChanged += new System.EventHandler(this.combo_bpp_SelectedIndexChanged);
            // 
            // textBox_current_address
            // 
            this.textBox_current_address.Location = new System.Drawing.Point(9, 78);
            this.textBox_current_address.Name = "textBox_current_address";
            this.textBox_current_address.ReadOnly = true;
            this.textBox_current_address.Size = new System.Drawing.Size(84, 20);
            this.textBox_current_address.TabIndex = 1;
            // 
            // button_export
            // 
            this.button_export.Location = new System.Drawing.Point(6, 19);
            this.button_export.Name = "button_export";
            this.button_export.Size = new System.Drawing.Size(75, 23);
            this.button_export.TabIndex = 11;
            this.button_export.Text = "Export";
            this.button_export.UseVisualStyleBackColor = true;
            this.button_export.Click += new System.EventHandler(this.button_export_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel.Location = new System.Drawing.Point(6, 67);
            this.flowLayoutPanel.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(90, 85);
            this.flowLayoutPanel.TabIndex = 16;
            // 
            // label_current_address
            // 
            this.label_current_address.AutoSize = true;
            this.label_current_address.Location = new System.Drawing.Point(6, 62);
            this.label_current_address.Name = "label_current_address";
            this.label_current_address.Size = new System.Drawing.Size(44, 13);
            this.label_current_address.TabIndex = 17;
            this.label_current_address.Text = "Current:";
            // 
            // button_import
            // 
            this.button_import.Location = new System.Drawing.Point(168, 19);
            this.button_import.Name = "button_import";
            this.button_import.Size = new System.Drawing.Size(75, 23);
            this.button_import.TabIndex = 12;
            this.button_import.Text = "Import";
            this.button_import.UseVisualStyleBackColor = true;
            this.button_import.Click += new System.EventHandler(this.button_import_Click);
            // 
            // button_goto_player
            // 
            this.button_goto_player.Location = new System.Drawing.Point(6, 19);
            this.button_goto_player.Name = "button_goto_player";
            this.button_goto_player.Size = new System.Drawing.Size(75, 23);
            this.button_goto_player.TabIndex = 21;
            this.button_goto_player.Text = "Main Sprites";
            this.button_goto_player.UseVisualStyleBackColor = true;
            this.button_goto_player.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button_goto_link_Click);
            // 
            // button_goto_font
            // 
            this.button_goto_font.Location = new System.Drawing.Point(87, 19);
            this.button_goto_font.Name = "button_goto_font";
            this.button_goto_font.Size = new System.Drawing.Size(75, 23);
            this.button_goto_font.TabIndex = 22;
            this.button_goto_font.Text = "Font GFX";
            this.button_goto_font.UseVisualStyleBackColor = true;
            this.button_goto_font.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button_goto_font_Click);
            // 
            // button_goto_misc
            // 
            this.button_goto_misc.Location = new System.Drawing.Point(168, 19);
            this.button_goto_misc.Name = "button_goto_misc";
            this.button_goto_misc.Size = new System.Drawing.Size(75, 23);
            this.button_goto_misc.TabIndex = 23;
            this.button_goto_misc.Text = "Misc GFX";
            this.button_goto_misc.UseVisualStyleBackColor = true;
            this.button_goto_misc.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button_goto_misc_Click);
            // 
            // combo_palette
            // 
            this.combo_palette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_palette.FormattingEnabled = true;
            this.combo_palette.Location = new System.Drawing.Point(6, 19);
            this.combo_palette.Name = "combo_palette";
            this.combo_palette.Size = new System.Drawing.Size(90, 21);
            this.combo_palette.TabIndex = 24;
            this.combo_palette.SelectedIndexChanged += new System.EventHandler(this.checkBox_trans_CheckedChanged);
            // 
            // checkBox_trans
            // 
            this.checkBox_trans.AutoSize = true;
            this.checkBox_trans.Checked = true;
            this.checkBox_trans.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_trans.Location = new System.Drawing.Point(6, 49);
            this.checkBox_trans.Name = "checkBox_trans";
            this.checkBox_trans.Size = new System.Drawing.Size(91, 17);
            this.checkBox_trans.TabIndex = 26;
            this.checkBox_trans.Text = "Transparency";
            this.checkBox_trans.UseVisualStyleBackColor = false;
            this.checkBox_trans.CheckedChanged += new System.EventHandler(this.checkBox_trans_CheckedChanged);
            // 
            // label_hexVal
            // 
            this.label_hexVal.AutoSize = true;
            this.label_hexVal.Location = new System.Drawing.Point(49, 88);
            this.label_hexVal.Name = "label_hexVal";
            this.label_hexVal.Size = new System.Drawing.Size(21, 13);
            this.label_hexVal.TabIndex = 30;
            this.label_hexVal.Text = "XX";
            // 
            // label_hex
            // 
            this.label_hex.AutoSize = true;
            this.label_hex.Location = new System.Drawing.Point(20, 88);
            this.label_hex.Name = "label_hex";
            this.label_hex.Size = new System.Drawing.Size(29, 13);
            this.label_hex.TabIndex = 29;
            this.label_hex.Text = "Hex:";
            // 
            // pixelBox_magnif
            // 
            this.pixelBox_magnif.BackgroundImage = global::Zeldix.Properties.Resources.transparency2;
            this.pixelBox_magnif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pixelBox_magnif.Image = null;
            this.pixelBox_magnif.Location = new System.Drawing.Point(16, 19);
            this.pixelBox_magnif.Name = "pixelBox_magnif";
            this.pixelBox_magnif.Size = new System.Drawing.Size(66, 66);
            this.pixelBox_magnif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelBox_magnif.TabIndex = 28;
            this.pixelBox_magnif.TabStop = false;
            this.pixelBox_magnif.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pixelBox_magnif_MouseMove);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_address);
            this.groupBox1.Controls.Add(this.textBox_address);
            this.groupBox1.Controls.Add(this.textBox_current_address);
            this.groupBox1.Controls.Add(this.label_current_address);
            this.groupBox1.Controls.Add(this.button_go);
            this.groupBox1.Location = new System.Drawing.Point(9, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(147, 136);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_save);
            this.groupBox2.Controls.Add(this.pixelBox_magnif);
            this.groupBox2.Controls.Add(this.label_hex);
            this.groupBox2.Controls.Add(this.label_hexVal);
            this.groupBox2.Location = new System.Drawing.Point(162, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(95, 136);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Edit";
            // 
            // button_save
            // 
            this.button_save.Enabled = false;
            this.button_save.Location = new System.Drawing.Point(16, 104);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(66, 23);
            this.button_save.TabIndex = 31;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.combo_palette);
            this.groupBox3.Controls.Add(this.flowLayoutPanel);
            this.groupBox3.Controls.Add(this.checkBox_trans);
            this.groupBox3.Location = new System.Drawing.Point(263, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(106, 176);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Palette";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button_goto_misc);
            this.groupBox5.Controls.Add(this.button_goto_font);
            this.groupBox5.Controls.Add(this.button_goto_player);
            this.groupBox5.Location = new System.Drawing.Point(9, 139);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(248, 50);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Go to";
            // 
            // flowLayoutPanel_arrows
            // 
            this.flowLayoutPanel_arrows.Controls.Add(this.button_large_up);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_big_up);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_little_up);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_minu);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_uppu);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_little_down);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_big_down);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_large_down);
            this.flowLayoutPanel_arrows.Location = new System.Drawing.Point(291, 37);
            this.flowLayoutPanel_arrows.Name = "flowLayoutPanel_arrows";
            this.flowLayoutPanel_arrows.Size = new System.Drawing.Size(28, 212);
            this.flowLayoutPanel_arrows.TabIndex = 20;
            // 
            // button_large_up
            // 
            this.button_large_up.BackColor = System.Drawing.Color.Transparent;
            this.button_large_up.FlatAppearance.BorderSize = 0;
            this.button_large_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_large_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_large_up.Image = global::Zeldix.Properties.Resources.gfx2;
            this.button_large_up.Location = new System.Drawing.Point(3, 3);
            this.button_large_up.Name = "button_large_up";
            this.button_large_up.Size = new System.Drawing.Size(20, 20);
            this.button_large_up.TabIndex = 3;
            this.button_large_up.UseVisualStyleBackColor = false;
            this.button_large_up.Click += new System.EventHandler(this.large_up_Click);
            // 
            // button_big_up
            // 
            this.button_big_up.BackColor = System.Drawing.Color.Transparent;
            this.button_big_up.FlatAppearance.BorderSize = 0;
            this.button_big_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_big_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_big_up.Image = global::Zeldix.Properties.Resources.gfx3;
            this.button_big_up.Location = new System.Drawing.Point(3, 29);
            this.button_big_up.Name = "button_big_up";
            this.button_big_up.Size = new System.Drawing.Size(20, 20);
            this.button_big_up.TabIndex = 4;
            this.button_big_up.UseVisualStyleBackColor = false;
            this.button_big_up.Click += new System.EventHandler(this.button_big_up_Click);
            // 
            // button_little_up
            // 
            this.button_little_up.BackColor = System.Drawing.Color.Transparent;
            this.button_little_up.FlatAppearance.BorderSize = 0;
            this.button_little_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_little_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_little_up.Image = global::Zeldix.Properties.Resources.gfx4;
            this.button_little_up.Location = new System.Drawing.Point(3, 55);
            this.button_little_up.Name = "button_little_up";
            this.button_little_up.Size = new System.Drawing.Size(20, 20);
            this.button_little_up.TabIndex = 5;
            this.button_little_up.UseVisualStyleBackColor = false;
            this.button_little_up.Click += new System.EventHandler(this.button_little_up_Click);
            // 
            // button_minu
            // 
            this.button_minu.BackColor = System.Drawing.Color.Transparent;
            this.button_minu.FlatAppearance.BorderSize = 0;
            this.button_minu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_minu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_minu.Image = global::Zeldix.Properties.Resources.gfx5;
            this.button_minu.Location = new System.Drawing.Point(3, 81);
            this.button_minu.Name = "button_minu";
            this.button_minu.Size = new System.Drawing.Size(20, 20);
            this.button_minu.TabIndex = 6;
            this.button_minu.UseVisualStyleBackColor = false;
            this.button_minu.Click += new System.EventHandler(this.button_minu_Click);
            // 
            // button_uppu
            // 
            this.button_uppu.BackColor = System.Drawing.Color.Transparent;
            this.button_uppu.FlatAppearance.BorderSize = 0;
            this.button_uppu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_uppu.Image = global::Zeldix.Properties.Resources.gfx6;
            this.button_uppu.Location = new System.Drawing.Point(3, 107);
            this.button_uppu.Name = "button_uppu";
            this.button_uppu.Size = new System.Drawing.Size(20, 20);
            this.button_uppu.TabIndex = 7;
            this.button_uppu.UseVisualStyleBackColor = false;
            this.button_uppu.Click += new System.EventHandler(this.button_uppu_Click);
            // 
            // button_little_down
            // 
            this.button_little_down.BackColor = System.Drawing.Color.Transparent;
            this.button_little_down.FlatAppearance.BorderSize = 0;
            this.button_little_down.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_little_down.Image = global::Zeldix.Properties.Resources.gfx7;
            this.button_little_down.Location = new System.Drawing.Point(3, 133);
            this.button_little_down.Name = "button_little_down";
            this.button_little_down.Size = new System.Drawing.Size(20, 20);
            this.button_little_down.TabIndex = 8;
            this.button_little_down.UseVisualStyleBackColor = false;
            this.button_little_down.Click += new System.EventHandler(this.button_little_down_Click);
            // 
            // button_big_down
            // 
            this.button_big_down.BackColor = System.Drawing.Color.Transparent;
            this.button_big_down.FlatAppearance.BorderSize = 0;
            this.button_big_down.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_big_down.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_big_down.Image = global::Zeldix.Properties.Resources.gfx8;
            this.button_big_down.Location = new System.Drawing.Point(3, 159);
            this.button_big_down.Name = "button_big_down";
            this.button_big_down.Size = new System.Drawing.Size(20, 20);
            this.button_big_down.TabIndex = 9;
            this.button_big_down.UseVisualStyleBackColor = false;
            this.button_big_down.Click += new System.EventHandler(this.button_big_down_Click);
            // 
            // button_large_down
            // 
            this.button_large_down.BackColor = System.Drawing.Color.Transparent;
            this.button_large_down.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_large_down.FlatAppearance.BorderSize = 0;
            this.button_large_down.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_large_down.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_large_down.Image = global::Zeldix.Properties.Resources.gfx9;
            this.button_large_down.Location = new System.Drawing.Point(3, 185);
            this.button_large_down.Name = "button_large_down";
            this.button_large_down.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_large_down.Size = new System.Drawing.Size(20, 20);
            this.button_large_down.TabIndex = 10;
            this.button_large_down.UseVisualStyleBackColor = false;
            this.button_large_down.Click += new System.EventHandler(this.large_down_Click);
            // 
            // pixelBox
            // 
            this.pixelBox.BackgroundImage = global::Zeldix.Properties.Resources.transparency;
            this.pixelBox.Image = null;
            this.pixelBox.Location = new System.Drawing.Point(6, 14);
            this.pixelBox.Name = "pixelBox";
            this.pixelBox.Size = new System.Drawing.Size(256, 256);
            this.pixelBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelBox.TabIndex = 15;
            this.pixelBox.TabStop = false;
            this.pixelBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pixelBox_Click);
            // 
            // vScrollBar
            // 
            this.vScrollBar.LargeChange = 1;
            this.vScrollBar.Location = new System.Drawing.Point(265, 13);
            this.vScrollBar.Name = "vScrollBar";
            this.vScrollBar.Size = new System.Drawing.Size(17, 257);
            this.vScrollBar.TabIndex = 19;
            this.vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar_Scroll);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.vScrollBar);
            this.groupBox4.Controls.Add(this.pixelBox);
            this.groupBox4.Controls.Add(this.flowLayoutPanel_arrows);
            this.groupBox4.Location = new System.Drawing.Point(9, 195);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(325, 282);
            this.groupBox4.TabIndex = 34;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Viewer";
            // 
            // groupBox_actions
            // 
            this.groupBox_actions.Controls.Add(this.button_export);
            this.groupBox_actions.Controls.Add(this.button_import);
            this.groupBox_actions.Controls.Add(this.combo_bpp);
            this.groupBox_actions.Location = new System.Drawing.Point(9, 483);
            this.groupBox_actions.Name = "groupBox_actions";
            this.groupBox_actions.Size = new System.Drawing.Size(325, 49);
            this.groupBox_actions.TabIndex = 36;
            this.groupBox_actions.TabStop = false;
            this.groupBox_actions.Text = "Actions";
            // 
            // form_graphics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 538);
            this.Controls.Add(this.groupBox_actions);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Name = "form_graphics";
            this.Text = "Graphics";
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.form_graphics_MouseWheel);
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_magnif)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.flowLayoutPanel_arrows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox_actions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.TextBox textBox_address;
        private System.Windows.Forms.Button button_go;
        private System.Windows.Forms.Label label_address;
        private System.Windows.Forms.ComboBox combo_bpp;
        private System.Windows.Forms.TextBox textBox_current_address;
        private System.Windows.Forms.Button button_export;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.Label label_current_address;
        private System.Windows.Forms.Button button_import;
        private Button button_goto_player;
        private Button button_goto_font;
        private Button button_goto_misc;
        private ComboBox combo_palette;
        private CheckBox checkBox_trans;
        private Label label_hexVal;
        private Label label_hex;
        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox_magnif;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox5;
        private FlowLayoutPanel flowLayoutPanel_arrows;
        private Button button_large_up;
        private Button button_big_up;
        private Button button_little_up;
        private Button button_minu;
        private Button button_uppu;
        private Button button_little_down;
        private Button button_big_down;
        private Button button_large_down;
        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox;
        private VScrollBar vScrollBar;
        private GroupBox groupBox4;
        private GroupBox groupBox_actions;
        private Button button_save;
    }
}