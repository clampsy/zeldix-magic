﻿using Zeldix.Classes.Forms;
namespace Zeldix.Forms
{
    partial class form_monologue_help : form_template
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.richTextBox_help = new System.Windows.Forms.RichTextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox_chara = new System.Windows.Forms.GroupBox();
            this.listBox_table = new System.Windows.Forms.ListBox();
            this.groupBox_dte = new System.Windows.Forms.GroupBox();
            this.listBox_dte = new System.Windows.Forms.ListBox();
            this.pixelBox_preview = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.groupBox_preview = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox_chara.SuspendLayout();
            this.groupBox_dte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_preview)).BeginInit();
            this.groupBox_preview.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox_help
            // 
            this.richTextBox_help.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_help.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_help.Location = new System.Drawing.Point(6, 15);
            this.richTextBox_help.Name = "richTextBox_help";
            this.richTextBox_help.ReadOnly = true;
            this.richTextBox_help.ShortcutsEnabled = false;
            this.richTextBox_help.Size = new System.Drawing.Size(161, 142);
            this.richTextBox_help.TabIndex = 4;
            this.richTextBox_help.Text = "";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(8, 12);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox_chara);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox_dte);
            this.splitContainer1.Size = new System.Drawing.Size(211, 175);
            this.splitContainer1.SplitterDistance = 114;
            this.splitContainer1.TabIndex = 3;
            // 
            // groupBox_chara
            // 
            this.groupBox_chara.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_chara.Controls.Add(this.listBox_table);
            this.groupBox_chara.Location = new System.Drawing.Point(3, 3);
            this.groupBox_chara.Name = "groupBox_chara";
            this.groupBox_chara.Size = new System.Drawing.Size(108, 169);
            this.groupBox_chara.TabIndex = 5;
            this.groupBox_chara.TabStop = false;
            // 
            // listBox_table
            // 
            this.listBox_table.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_table.FormattingEnabled = true;
            this.listBox_table.Location = new System.Drawing.Point(6, 19);
            this.listBox_table.Name = "listBox_table";
            this.listBox_table.ScrollAlwaysVisible = true;
            this.listBox_table.Size = new System.Drawing.Size(96, 134);
            this.listBox_table.TabIndex = 0;
            this.listBox_table.SelectedIndexChanged += new System.EventHandler(this.ListBox_table_SelectedIndexChanged);
            // 
            // groupBox_dte
            // 
            this.groupBox_dte.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_dte.Controls.Add(this.listBox_dte);
            this.groupBox_dte.Location = new System.Drawing.Point(3, 3);
            this.groupBox_dte.Name = "groupBox_dte";
            this.groupBox_dte.Size = new System.Drawing.Size(87, 169);
            this.groupBox_dte.TabIndex = 5;
            this.groupBox_dte.TabStop = false;
            // 
            // listBox_dte
            // 
            this.listBox_dte.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_dte.FormattingEnabled = true;
            this.listBox_dte.Location = new System.Drawing.Point(6, 19);
            this.listBox_dte.Name = "listBox_dte";
            this.listBox_dte.ScrollAlwaysVisible = true;
            this.listBox_dte.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox_dte.Size = new System.Drawing.Size(75, 134);
            this.listBox_dte.TabIndex = 2;
            // 
            // pixelBox_preview
            // 
            this.pixelBox_preview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pixelBox_preview.Image = null;
            this.pixelBox_preview.Location = new System.Drawing.Point(6, 17);
            this.pixelBox_preview.Name = "pixelBox_preview";
            this.pixelBox_preview.Size = new System.Drawing.Size(352, 32);
            this.pixelBox_preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pixelBox_preview.TabIndex = 5;
            this.pixelBox_preview.TabStop = false;
            // 
            // groupBox_preview
            // 
            this.groupBox_preview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_preview.Controls.Add(this.pixelBox_preview);
            this.groupBox_preview.Location = new System.Drawing.Point(8, 196);
            this.groupBox_preview.Name = "groupBox_preview";
            this.groupBox_preview.Size = new System.Drawing.Size(390, 53);
            this.groupBox_preview.TabIndex = 6;
            this.groupBox_preview.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.richTextBox_help);
            this.groupBox1.Location = new System.Drawing.Point(225, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 175);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // form_monologue_help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 261);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_preview);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(300, 200);
            this.Name = "form_monologue_help";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox_chara.ResumeLayout(false);
            this.groupBox_dte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_preview)).EndInit();
            this.groupBox_preview.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_table;
        private System.Windows.Forms.ListBox listBox_dte;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBox_help;
        private System.Windows.Forms.GroupBox groupBox_chara;
        private System.Windows.Forms.GroupBox groupBox_dte;
        private Classes.GraphicBoxSelect.PixelBox pixelBox_preview;
        private System.Windows.Forms.GroupBox groupBox_preview;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}