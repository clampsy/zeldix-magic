﻿namespace Zeldix.Forms
{
    partial class form_GFXScheme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tab_main = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.numeric_main = new System.Windows.Forms.NumericUpDown();
            this.flowPanel_main = new System.Windows.Forms.FlowLayoutPanel();
            this.tab_room = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.numeric_room = new System.Windows.Forms.NumericUpDown();
            this.flowPanel_room = new System.Windows.Forms.FlowLayoutPanel();
            this.tab_sprite = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.numeric_sprite = new System.Windows.Forms.NumericUpDown();
            this.flowPanel_sprite = new System.Windows.Forms.FlowLayoutPanel();
            this.tabControl.SuspendLayout();
            this.tab_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_main)).BeginInit();
            this.tab_room.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_room)).BeginInit();
            this.tab_sprite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_sprite)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tab_main);
            this.tabControl.Controls.Add(this.tab_room);
            this.tabControl.Controls.Add(this.tab_sprite);
            this.tabControl.Location = new System.Drawing.Point(12, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(382, 292);
            this.tabControl.TabIndex = 9;
            // 
            // tab_main
            // 
            this.tab_main.Controls.Add(this.label1);
            this.tab_main.Controls.Add(this.numeric_main);
            this.tab_main.Controls.Add(this.flowPanel_main);
            this.tab_main.Location = new System.Drawing.Point(4, 22);
            this.tab_main.Name = "tab_main";
            this.tab_main.Padding = new System.Windows.Forms.Padding(3);
            this.tab_main.Size = new System.Drawing.Size(374, 266);
            this.tab_main.TabIndex = 0;
            this.tab_main.Text = "Main";
            this.tab_main.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Blockset ID:";
            // 
            // numeric_main
            // 
            this.numeric_main.Location = new System.Drawing.Point(78, 8);
            this.numeric_main.Name = "numeric_main";
            this.numeric_main.Size = new System.Drawing.Size(61, 20);
            this.numeric_main.TabIndex = 2;
            this.numeric_main.ValueChanged += new System.EventHandler(this.numeric_main_ValueChanged);
            // 
            // flowPanel_main
            // 
            this.flowPanel_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowPanel_main.AutoScroll = true;
            this.flowPanel_main.Location = new System.Drawing.Point(7, 34);
            this.flowPanel_main.Name = "flowPanel_main";
            this.flowPanel_main.Size = new System.Drawing.Size(361, 226);
            this.flowPanel_main.TabIndex = 0;
            // 
            // tab_room
            // 
            this.tab_room.Controls.Add(this.label2);
            this.tab_room.Controls.Add(this.numeric_room);
            this.tab_room.Controls.Add(this.flowPanel_room);
            this.tab_room.Location = new System.Drawing.Point(4, 22);
            this.tab_room.Name = "tab_room";
            this.tab_room.Padding = new System.Windows.Forms.Padding(3);
            this.tab_room.Size = new System.Drawing.Size(374, 266);
            this.tab_room.TabIndex = 1;
            this.tab_room.Text = "Room";
            this.tab_room.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Blockset ID:";
            // 
            // numeric_room
            // 
            this.numeric_room.Location = new System.Drawing.Point(78, 8);
            this.numeric_room.Name = "numeric_room";
            this.numeric_room.Size = new System.Drawing.Size(61, 20);
            this.numeric_room.TabIndex = 3;
            this.numeric_room.ValueChanged += new System.EventHandler(this.numeric_main_ValueChanged);
            // 
            // flowPanel_room
            // 
            this.flowPanel_room.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowPanel_room.AutoScroll = true;
            this.flowPanel_room.Location = new System.Drawing.Point(7, 34);
            this.flowPanel_room.Name = "flowPanel_room";
            this.flowPanel_room.Size = new System.Drawing.Size(361, 220);
            this.flowPanel_room.TabIndex = 1;
            // 
            // tab_sprite
            // 
            this.tab_sprite.Controls.Add(this.label3);
            this.tab_sprite.Controls.Add(this.numeric_sprite);
            this.tab_sprite.Controls.Add(this.flowPanel_sprite);
            this.tab_sprite.Location = new System.Drawing.Point(4, 22);
            this.tab_sprite.Name = "tab_sprite";
            this.tab_sprite.Padding = new System.Windows.Forms.Padding(3);
            this.tab_sprite.Size = new System.Drawing.Size(374, 266);
            this.tab_sprite.TabIndex = 2;
            this.tab_sprite.Text = "Sprite";
            this.tab_sprite.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Blockset ID:";
            // 
            // numeric_sprite
            // 
            this.numeric_sprite.Location = new System.Drawing.Point(78, 8);
            this.numeric_sprite.Name = "numeric_sprite";
            this.numeric_sprite.Size = new System.Drawing.Size(61, 20);
            this.numeric_sprite.TabIndex = 5;
            this.numeric_sprite.ValueChanged += new System.EventHandler(this.numeric_main_ValueChanged);
            // 
            // flowPanel_sprite
            // 
            this.flowPanel_sprite.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowPanel_sprite.AutoScroll = true;
            this.flowPanel_sprite.Location = new System.Drawing.Point(7, 34);
            this.flowPanel_sprite.Name = "flowPanel_sprite";
            this.flowPanel_sprite.Size = new System.Drawing.Size(361, 220);
            this.flowPanel_sprite.TabIndex = 4;
            // 
            // form_GFXScheme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 300);
            this.Controls.Add(this.tabControl);
            this.Name = "form_GFXScheme";
            this.Text = "Graphic Schemes";
            this.tabControl.ResumeLayout(false);
            this.tab_main.ResumeLayout(false);
            this.tab_main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_main)).EndInit();
            this.tab_room.ResumeLayout(false);
            this.tab_room.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_room)).EndInit();
            this.tab_sprite.ResumeLayout(false);
            this.tab_sprite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_sprite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tab_main;
        private System.Windows.Forms.TabPage tab_room;
        private System.Windows.Forms.TabPage tab_sprite;
        private System.Windows.Forms.FlowLayoutPanel flowPanel_main;
        private System.Windows.Forms.NumericUpDown numeric_main;
        private System.Windows.Forms.NumericUpDown numeric_room;
        private System.Windows.Forms.FlowLayoutPanel flowPanel_room;
        private System.Windows.Forms.NumericUpDown numeric_sprite;
        private System.Windows.Forms.FlowLayoutPanel flowPanel_sprite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}