﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Zeldix.Forms
{
    partial class form_hexEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.hexBox = new Zeldix.Classes.HexBox.HexBox_c();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.encodingToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.textBox_adressGoto = new System.Windows.Forms.ToolStripTextBox();
            this.button_go = new System.Windows.Forms.ToolStripButton();
            this.button_save = new System.Windows.Forms.ToolStripButton();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // hexBox
            // 
            this.hexBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hexBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.hexBox.ColumnInfoVisible = true;
            this.hexBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.hexBox.LineInfoVisible = true;
            this.hexBox.Location = new System.Drawing.Point(4, 28);
            this.hexBox.Name = "hexBox";
            this.hexBox.ReadOnly = true;
            this.hexBox.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexBox.Size = new System.Drawing.Size(637, 320);
            this.hexBox.StringViewVisible = true;
            this.hexBox.TabIndex = 8;
            this.hexBox.VScrollBarVisible = true;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.encodingToolStripComboBox,
            this.textBox_adressGoto,
            this.button_go,
            this.button_save});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(644, 25);
            this.toolStrip.TabIndex = 7;
            this.toolStrip.Text = "toolStrip1";
            // 
            // encodingToolStripComboBox
            // 
            this.encodingToolStripComboBox.BackColor = System.Drawing.SystemColors.Control;
            this.encodingToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.encodingToolStripComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.encodingToolStripComboBox.Name = "encodingToolStripComboBox";
            this.encodingToolStripComboBox.Size = new System.Drawing.Size(100, 25);
            this.encodingToolStripComboBox.Visible = false;
            // 
            // textBox_adressGoto
            // 
            this.textBox_adressGoto.MaxLength = 6;
            this.textBox_adressGoto.Name = "textBox_adressGoto";
            this.textBox_adressGoto.Size = new System.Drawing.Size(100, 25);
            // 
            // button_go
            // 
            this.button_go.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.button_go.Name = "button_go";
            this.button_go.Size = new System.Drawing.Size(26, 22);
            this.button_go.Text = "Go";
            // 
            // button_save
            // 
            this.button_save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(35, 22);
            this.button_save.Text = "Save";
            this.button_save.Visible = false;
            // 
            // form_hexEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 349);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.hexBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "form_hexEditor";
            this.Text = "Hex Viewer";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Zeldix.Classes.HexBox.HexBox_c hexBox;
        private ToolStrip toolStrip;
        private ToolStripComboBox encodingToolStripComboBox;
        private ToolStripTextBox textBox_adressGoto;
        private ToolStripButton button_save;
        private ToolStripButton button_go;
    }
}