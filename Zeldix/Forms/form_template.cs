﻿using System.Threading;
using System.Windows.Forms;
namespace Zeldix.Classes.Forms
{
    public partial class form_template : Form
    {
        private const double number = .03D;
        internal form_template()
        { InitializeComponent(); }

        internal void fadeout() { fade(false); }

        internal void fadein(double num) { fade(true, num); }

        private void fade(bool fadein, double num = 0)
        {
            double temp = number;
            if (fadein)
            {
                Opacity = 0;
                temp = -number;
            }

            object lockObject = new object();
            while ((fadein? (Opacity < num) : (Opacity > num)))
            {
                lock (lockObject)
                {
                    Opacity -= temp;
                    Monitor.Wait(lockObject, 1);
                }
            }
            Opacity = num;
        }
    }
}