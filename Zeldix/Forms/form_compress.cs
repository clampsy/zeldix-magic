﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Forms
{
    public partial class form_compress : form_template
    {
        private int currentAddress_;
        private const int
            BPPformat = 3;

        private readonly DecompressBuffer decomp;
        private Color[] pal;
       
        private int currentAddress
        {
            get
            { return currentAddress_; }
            set
            {
                textBox_current_address.Text = MyMath.decToHex(currentAddress_ = value, 1);
                textBox_current.Text = decomp.getCurrentIndex().ToString();
                read();
            }
        }

        public form_compress()
        {
            decomp = new DecompressBuffer();
            InitializeComponent();
            upDatePalette();
            button_little_up_Click(null, null);
            label_max.Text += decomp.getSize;
            comboBox_palette.DataSource = (new Get_Pallette()).getList(BPPformat);
        }
        private void button_little_up_Click(object sender, EventArgs e)
        { currentAddress = decomp.goUp(); }
        private void button_little_down_Click(object sender, EventArgs e)
        { currentAddress = decomp.goDown(); }
        private void read()
        {
            try
            { pixelBox.Image = decomp.getBMP(paly: pal); }
            catch
            { pixelBox.Image = null; }
        }

        private void button_go_Click(object sender, EventArgs e)
        {
            int result;
            if (int.TryParse(textBox_current.Text, out result))
                currentAddress = decomp.setCurrent(result);
        }

        private void upDatePalette(object sender = null, EventArgs e = null)
        {
            if (pixelBox.Image != null)
            {
                Bitmap myImage = pixelBox.Image;
                pal = (new Get_Pallette()).getPalette(BPPformat, comboBox_palette.SelectedIndex, checkBox_trans.Checked);

                if (pal == null)
                    throw new ArgumentNullException();

                if (myImage != null) 
                    Palette.paletteSet(ref myImage, pal);

                pixelBox.Image = myImage;

                /*
                 * V set palette V
                 */
                flowLayoutPanel.Controls.Clear();

                foreach (Color bb in pal)
                    flowLayoutPanel.Controls.Add(new Panel()
                    {
                        BackColor = bb,
                        BorderStyle = BorderStyle.FixedSingle,
                        Width = 10,
                        Height = 10
                    }
                    );
            }
        }
    }
}