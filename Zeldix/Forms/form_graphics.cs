﻿/*
 * Class        :   form_graphics.cs
 * Author       :   clampsy
 * Description  :   
 */
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using Zeldix.Classes;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Forms
{
    public partial class form_graphics : form_template
    {
        #region variables
        private const short
            bigTileNo = 256,
            teeney = 1,
            blockLW = 16,
            paletteSquareDim = 10;
        #endregion
        #region objects
        private readonly ScrollManager scrolly;
        private readonly Get_Pallette getPal;
        private int
            selectedAddress,
            selectedPaletteBox;

        private Panel[] paletteBoxes;
        #endregion
        #region constructor
        /// <summary>
        /// Initializers for form
        /// </summary>
        /// <param name="rom"></param>
        internal form_graphics()
        {
            getPal = new Get_Pallette();

            scrolly = new ScrollManager(blockLW, blockLW, RomIO.size - RomIO.getHeaderOffset(), 8);
            InitializeComponent();
            currentFormat = 1;
            vScrollBar.Minimum = vScrollBar.Maximum = 0;
            button_goto_link_Click();
            paletteBox();
            updatePaletteSelector();
            updateScrollbar();
        }

        private void paletteBox()
        {
            paletteBoxes = new Panel[currentPalette.Length];
            for (int i = 0; i < BPPPack.getLargestPaletteEntry(); i++)
            {
                paletteBoxes[i] = new Panel()
                {
                    BackColor = currentPalette[i],
                    BorderStyle = BorderStyle.None,
                    Width = paletteSquareDim,
                    Height = paletteSquareDim,
                    Name = i.ToString(),
                };
                paletteBoxes[i].Click += new EventHandler(PaletteBox_Click);
            }
            flowLayoutPanel.Controls.AddRange(paletteBoxes);
        }
        #endregion
        #region palettes
        /// <summary>
        /// Update the boxes that show all the palettes
        /// </summary>
        private void updatePaletteSelector()
        {
            if (paletteBoxes != null)
            {
                combo_palette.DataSource = getPal.getList(currentFormat);
                if (currentPalette == null)
                    throw new ArgumentNullException();
                paletteBoxes[selectedPaletteBox].BorderStyle = BorderStyle.None;

                for (int i = 0; i < paletteBoxes.Length; i++)
                {
                    if (i < currentPalette.Length)
                    {
                        paletteBoxes[i].BackColor = currentPalette[i];
                        paletteBoxes[i].Visible = true;
                    }
                    else paletteBoxes[i].Visible = false;
                }

                selectedPaletteBox = 0;
                paletteBoxes[0].BorderStyle = BorderStyle.Fixed3D;
            }
        }

        private void PaletteBox_Click(object sender = null, EventArgs e = null)
        {
            paletteBoxes[selectedPaletteBox].BorderStyle = BorderStyle.None;
            selectedPaletteBox = int.Parse(((Panel)sender).Name);
            paletteBoxes[selectedPaletteBox].BorderStyle = BorderStyle.Fixed3D;
        }

        private int currentFormat
        {
            get
            {
                if (combo_bpp.SelectedIndex + 1 < 1)
                    return 1;
                return combo_bpp.SelectedIndex + 1;
            }

            set
            { combo_bpp.SelectedIndex = value - 1; }
        }

        private Color[] currentPalette
        {
            get
            {
                return getPal.getPalette(currentFormat,
                 combo_palette.SelectedIndex != -1 ? combo_palette.SelectedIndex : 0,
                 checkBox_trans.Checked);
            }
        }
        #endregion
        private int tilesize
        { get { return BPPPack.getBytesPerBlock(currentFormat); } }
        private void updateTileSize()
        { scrolly.setWordSize(tilesize); }
        private void setCursor(int i)
        { textBox_current_address.Text = MyMath.decToHex(scrolly.cursor = i, 1); }
        #region address events
        private void button_go_Click(object sender, EventArgs e)
        {
            if (MyMath.isHex(textBox_address.Text))
                if (MyMath.hexToDec(textBox_address.Text) < scrolly.maxToScroll)
                {
                    setCursor(MyMath.hexToDec(textBox_address.Text));
                    UpdateView();
                }
                else ErrorMessage.Show("Address don't exist yo.");
            else ErrorMessage.Show("Not a valid address.");
        }

        private void textBox_address_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);
            button_go.Enabled = textBox_address.Text.Length - (char.IsControl(e.KeyChar) ? 1 : -1) > 0;
        }
        #endregion
        private void combo_bpp_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateTileSize();
            combo_palette.DataSource = getPal.getList(currentFormat);
            UpdateView();
            updateScrollbar();

        }
        private void updateMagnify()
        {
            if (selectedAddress != -1)
            {
                pixelBox_magnif.Image = read(selectedAddress, 1);
                button_save.Enabled = false;
            }
        }
        #region Scroll button click events
        private void button_minu_Click(object sender, EventArgs e)
        { upup(teeney); }
        private void button_uppu_Click(object sender, EventArgs e)
        { downdown(teeney); }
        private void button_little_up_Click(object sender, EventArgs e)
        { upup(tilesize); }
        private void button_little_down_Click(object sender, EventArgs e)
        { downdown(tilesize); }
        private void button_big_up_Click(object sender, EventArgs e)
        { upup(scrolly.lineSize); }
        private void button_big_down_Click(object sender, EventArgs e)
        { downdown(scrolly.lineSize); }
        private void large_up_Click(object sender, EventArgs e)
        { upup(scrolly.pageSize); }
        private void large_down_Click(object sender, EventArgs e)
        { downdown(scrolly.pageSize); }
        #endregion
        #region Export and import buttons
        private void button_export_Click(object sender, EventArgs e)
        {
            pixelBox.Image.Save(Constants.PATH + currentFormat + "BPP.png", ImageFormat.Png);
            ((form_parent)MdiParent).updateStatus("Exported sucessfully. Check the resources folder.");
        }

        private void button_import_Click(object sender, EventArgs e)
        {
            OpenFileDialog FD = new OpenFileDialog();
            FD.Filter = "PNG|*.png";

            if (FD.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(FD.FileName))
                {
                    BMPImport.importBitmap(FD.FileName, scrolly.cursor, pixelBox.Image.Height, pixelBox.Image.Width, currentFormat);
                    UpdateView();
                }
            }
        }
        #endregion
        #region scroll events
        private void vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (vScrollBar.Value != vScrollBar.Maximum)
            {
                int val = vScrollBar.Value * scrolly.lineSize - scrolly.cursor;

                if (val > 0)
                    downdown(Math.Abs(val));
                else if (val < 0)
                    upup(Math.Abs(val));
            }
            else
            {
                setCursor(scrolly.maxToScroll);
                UpdateView();
            }
        }

        private void upup(int amount)
        {
            scrolly.upup(amount);
            scrolly.updateScrollbar(ref vScrollBar);
            UpdateView();
        }

        private void downdown(int amount)
        {
            scrolly.downdown(amount);
            scrolly.updateScrollbar(ref vScrollBar);
            UpdateView();
        }

        private void updateScrollbar()
        {
            int number = scrolly.maxToScroll / scrolly.lineSize;
            if (vScrollBar.Maximum != number)
                vScrollBar.Maximum = number;
            vScrollBar.Value = scrolly.cursor / scrolly.lineSize;
        }

        /// <summary>
        /// Does an action when the user uses the mousewheel on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_graphics_MouseWheel(object sender, MouseEventArgs e)
        {
            int temp = 40 * Math.Abs(e.Delta) / 4500;
            if (e.Delta > 0) //scroll up
                upup(scrolly.lineSize * temp);
            else downdown(scrolly.lineSize * temp);
        }

        #endregion
        #region GFX shortcuts
        private void button_goto_link_Click(object sender = null, MouseEventArgs e = null)
        { loadSecial(Constants.GFX_link_address_ALL, 4); }
        private void button_goto_font_Click(object sender = null, MouseEventArgs e = null)
        {
            const int stub = -1;
            int jump_loc = stub;
            switch (RegionId.myRegion)
            {
                case ((int)RegionId.region.Europe):
                case ((int)RegionId.region.USA):
                    jump_loc = Constants.GFX_font_address_USA_EU;
                    break;
                case ((int)RegionId.region.Japan):
                    break;
                case ((int)RegionId.region.Canada):
                    jump_loc = Constants.GFX_font_address_CAN;
                    break;
                case ((int)RegionId.region.German):
                    jump_loc = Constants.GFX_font_address_GER;
                    break;
                case ((int)RegionId.region.France):
                    jump_loc = Constants.GFX_font_address_FRA;
                    break;
                default:
                    throw new NotImplementedException();
            }

            if (jump_loc != stub)
                loadSecial(jump_loc, 2);
        }
        private void button_goto_misc_Click(object sender = null, MouseEventArgs e = null)
        { loadSecial(Constants.GFX_misc_address_ALL, 3); }


        private void loadSecial(int address, int format)
        {
            setCursor(address);
            if (currentFormat != format)
                currentFormat = format;
            UpdateView();
        }
        #endregion
        #region click events

        private void pixelBox_Click(object sender, MouseEventArgs e)
        {
            byte tileNumber = (byte)pixelBox.getTileNumber(e);
            selectedAddress = tileNumber * BPPPack.getBytesPerBlock(currentFormat) + scrolly.cursor;
            if (pixelBox.withInBorder(e))
            {
                label_hexVal.Text = MyMath.decToHex(tileNumber, 1);
                updateMagnify();
            }

        }
        private void checkBox_trans_CheckedChanged(object sender, EventArgs e)
        {
            updatePaletteSelector();
            if (currentPalette != null && pixelBox.Image != null)
            {
                pixelBox.Image = Palette.paletteSet(pixelBox.Image, currentPalette);
                pixelBox_magnif.Image = Palette.paletteSet(pixelBox_magnif.Image, currentPalette);
            }
        }
        private void pixelBox_magnif_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                if (e.X < pixelBox_magnif.Location.X + pixelBox_magnif.Width && e.Y < pixelBox_magnif.Location.Y + pixelBox_magnif.Height)
                {
                    int x, y;
                    if (pixelBox_magnif.picImage_MouseDown(e, out x, out y))
                    {
                        Bitmap bmp = pixelBox_magnif.Image;
                        LockBitmap lockBitmap = new LockBitmap(bmp);
                        lockBitmap.LockBits();
                        lockBitmap.SetPixel(x, y, selectedPaletteBox);
                        lockBitmap.UnlockBits();
                        pixelBox_magnif.Image = bmp;

                        if (!button_save.Enabled)
                            button_save.Enabled = true;
                    }
                }

        }

        private void pixelBox_magnif_MouseDown(object sender, MouseEventArgs e)
        {
            int x, y;
            if (pixelBox_magnif.picImage_MouseDown(e, out x, out y))
            {
                Bitmap bmp = pixelBox_magnif.Image;
                LockBitmap lockBitmap = new LockBitmap(bmp);
                lockBitmap.LockBits();
                lockBitmap.SetPixel(x, y, selectedPaletteBox);
                lockBitmap.UnlockBits();
                pixelBox_magnif.Image = bmp;

                if (!button_save.Enabled)
                    button_save.Enabled = true;
            }
        }
        private void button_save_Click(object sender, EventArgs e)
        {
            LockBitmap locky = new LockBitmap(pixelBox_magnif.Image);
            locky.LockBits();
            byte[] unpacked_pixels = locky.Pixels;
            locky.UnlockBits();
            byte[] packed_pixels = BPPPack.pack_bpp_tile(unpacked_pixels, currentFormat);
            RomIO.writeToArray(selectedAddress, packed_pixels.Length, packed_pixels);
            UpdateView();
        }
        #endregion
        private void UpdateView()
        {
            setCursor(scrolly.cursor);
            updateScrollbar();
            pixelBox.Image = read(scrolly.cursor, bigTileNo);
            updateMagnify();
        }
        private Bitmap read(int cursor, int tileNo)
        { return BPPRender.getBitmap(blockLW, currentPalette, BPPPack.unpackTiles(currentFormat, cursor, tileNo)); }
    }
}