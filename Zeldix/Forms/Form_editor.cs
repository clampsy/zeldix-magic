﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Forms;
using Zeldix.Classes.Monologue.Smallhacker;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Forms
{
    public partial class form_editor : form_template
    {
        private duck dd;
        private List<duck.Tile> tile;

        public form_editor()
        {
            InitializeComponent();
            dd = new duck();
            tile = dd.load();

            List<Bitmap> bbb = new List<Bitmap>();
            Get_Pallette pal = new Get_Pallette();

            foreach (duck.Tile a in tile)
                bbb.Add(BPPRender.getBitmap(1, pal.getPalette(2, 3), a.pixels));
            pixelBox1.Image = BMPMerge.mergeBitmaps(bbb, 8, 16);


        }
    }
}