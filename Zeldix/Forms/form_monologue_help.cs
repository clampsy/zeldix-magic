﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Forms;
using Zeldix.Classes.GraphicBoxSelect;
using Zeldix.Classes.Monologue;
using Zeldix.Classes.Monologue.Image;

namespace Zeldix.Forms
{
    public partial class form_monologue_help : form_template
    {
        public form_monologue_help()
        {
            InitializeComponent();
            Text = Settings.getString(this, "header");

            groupBox_chara.Text = Settings.getString(this, "chara");
            groupBox_dte.Text = Settings.getString(this, "dte");
            richTextBox_help.Rtf = @Settings.getString(this, "help");
            groupBox_preview.Text = Settings.getString("form_monologue_preview");

            listBox_table.DataSource = TableFile.tableFileList;
            listBox_dte.DataSource = (new DTE()).readDictionary();
        }

        
        private string SelectedItem
        {
            get { return listBox_table.SelectedItem.ToString(); }
        }

        private void ListBox_table_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            myFont f = new myFont();
            List<object> aa = f.ConvertToImage(SelectedItem);
            List<PixelBox> dialogueLines = DisplayText.Display(aa, 2);

            switch (dialogueLines.Count)
            {
                case 0:
                    pixelBox_preview.Image = null;
                    break;
                case 1:
                    pixelBox_preview.Image = dialogueLines[0].Image;
                    break;
                default:
                    MessageBox.Show(Settings.getString(this, "weird_error"));
                    break;
            }
        }
    }
}