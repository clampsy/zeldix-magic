﻿/*
 * Class        :   form_overworld.cs
 * Author       :   clampsy
 * Description  :   
 */
using System;
using Zeldix.Classes.Forms;
using Zeldix.Classes.Overworld;

namespace Zeldix.Forms
{
    public partial class form_overworld : form_template
    {
        private MapList mappy;
        internal form_overworld()
        {
            mappy = new MapList();
            InitializeComponent();
            numericUpDown.Maximum = mappy.getNumberOfMaps() - 1;
            numericUpDown_ValueChanged();
        }
        private void numericUpDown_ValueChanged(object sender = null, EventArgs e = null)
        {
            mappy.currentMapNo = getIndex;
            textBox_gfx_num.Text = mappy.getMapGraphicsIndex().ToString();

            pixelBox1.Image = mappy.drawMap();
        }

        private byte getIndex
        { get { return (byte)numericUpDown.Value; } }
    }
}