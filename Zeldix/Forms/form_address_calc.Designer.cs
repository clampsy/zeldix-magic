﻿namespace Zeldix.Forms
{
    partial class form_address_calc
    {     
        /// <summary>
        /// /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
        {
            this.textBox_input = new System.Windows.Forms.TextBox();
            this.radioButton_SnesToPc = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_pcToSnes = new System.Windows.Forms.RadioButton();
            this.label_input = new System.Windows.Forms.Label();
            this.label_output = new System.Windows.Forms.Label();
            this.textBox_output = new System.Windows.Forms.TextBox();
            this.button_calc = new System.Windows.Forms.Button();
            this.button_clear = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_input
            // 
            this.textBox_input.Location = new System.Drawing.Point(3, 3);
            this.textBox_input.MaxLength = 10;
            this.textBox_input.Name = "textBox_input";
            this.textBox_input.Size = new System.Drawing.Size(100, 20);
            this.textBox_input.TabIndex = 0;
            this.textBox_input.TextChanged += new System.EventHandler(this.textBox_input_TextChanged);
            // 
            // radioButton_SneesToPc
            // 
            this.radioButton_SnesToPc.AutoSize = true;
            this.radioButton_SnesToPc.Checked = true;
            this.radioButton_SnesToPc.Location = new System.Drawing.Point(13, 4);
            this.radioButton_SnesToPc.Name = "radioButton_SneesToPc";
            this.radioButton_SnesToPc.Size = new System.Drawing.Size(171, 17);
            this.radioButton_SnesToPc.TabIndex = 4;
            this.radioButton_SnesToPc.TabStop = true;
            this.radioButton_SnesToPc.Text = "SNEES LoROM to PC Address";
            this.radioButton_SnesToPc.UseVisualStyleBackColor = true;
            this.radioButton_SnesToPc.CheckedChanged += new System.EventHandler(this.radioButton_SneesToPc_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.Controls.Add(this.radioButton_pcToSnes);
            this.panel1.Controls.Add(this.radioButton_SnesToPc);
            this.panel1.Location = new System.Drawing.Point(61, 66);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(187, 47);
            this.panel1.TabIndex = 2;
            // 
            // radioButton_pcToSnees
            // 
            this.radioButton_pcToSnes.AutoSize = true;
            this.radioButton_pcToSnes.Location = new System.Drawing.Point(13, 27);
            this.radioButton_pcToSnes.Name = "radioButton_pcToSnees";
            this.radioButton_pcToSnes.Size = new System.Drawing.Size(171, 17);
            this.radioButton_pcToSnes.TabIndex = 5;
            this.radioButton_pcToSnes.TabStop = true;
            this.radioButton_pcToSnes.Text = "PC to SNEES LoROM Address";
            this.radioButton_pcToSnes.UseVisualStyleBackColor = true;
            // 
            // label_input
            // 
            this.label_input.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_input.AutoSize = true;
            this.label_input.Location = new System.Drawing.Point(6, 6);
            this.label_input.Margin = new System.Windows.Forms.Padding(6);
            this.label_input.Name = "label_input";
            this.label_input.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_input.Size = new System.Drawing.Size(34, 13);
            this.label_input.TabIndex = 3;
            this.label_input.Text = "Input:";
            // 
            // label_output
            // 
            this.label_output.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_output.AutoSize = true;
            this.label_output.Location = new System.Drawing.Point(6, 31);
            this.label_output.Margin = new System.Windows.Forms.Padding(6);
            this.label_output.Name = "label_output";
            this.label_output.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_output.Size = new System.Drawing.Size(42, 13);
            this.label_output.TabIndex = 5;
            this.label_output.Text = "Output:";
            this.label_output.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox_output
            // 
            this.textBox_output.Location = new System.Drawing.Point(3, 29);
            this.textBox_output.Name = "textBox_output";
            this.textBox_output.ReadOnly = true;
            this.textBox_output.Size = new System.Drawing.Size(100, 20);
            this.textBox_output.TabIndex = 2;
            // 
            // button_calc
            // 
            this.button_calc.Enabled = false;
            this.button_calc.Location = new System.Drawing.Point(3, 3);
            this.button_calc.Name = "button_calc";
            this.button_calc.Size = new System.Drawing.Size(75, 23);
            this.button_calc.TabIndex = 1;
            this.button_calc.Text = "Calculate";
            this.button_calc.UseVisualStyleBackColor = true;
            this.button_calc.Click += new System.EventHandler(this.button_calc_Click);
            // 
            // button_clear
            // 
            this.button_clear.Enabled = false;
            this.button_clear.Location = new System.Drawing.Point(3, 32);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(75, 23);
            this.button_clear.TabIndex = 3;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.textBox_input);
            this.flowLayoutPanel1.Controls.Add(this.textBox_output);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(83, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(110, 52);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.button_calc);
            this.flowLayoutPanel2.Controls.Add(this.button_clear);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(213, 4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(83, 59);
            this.flowLayoutPanel2.TabIndex = 8;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label_input);
            this.flowLayoutPanel3.Controls.Add(this.label_output);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(74, 49);
            this.flowLayoutPanel3.TabIndex = 9;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel1);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(12, 4);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(197, 59);
            this.flowLayoutPanel4.TabIndex = 6;
            // 
            // form_address_calc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 122);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "form_address_calc";
            this.Text = "Address Calc";
            this.Load += new System.EventHandler(this.form_address_calc_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_input;
        private System.Windows.Forms.RadioButton radioButton_SnesToPc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_input;
        private System.Windows.Forms.Label label_output;
        private System.Windows.Forms.TextBox textBox_output;
        private System.Windows.Forms.Button button_calc;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.RadioButton radioButton_pcToSnes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
    }
}