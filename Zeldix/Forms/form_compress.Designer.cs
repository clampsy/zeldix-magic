﻿using Zeldix.Classes.Forms;
namespace Zeldix.Forms
{
    partial class form_compress : form_template
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.pixelBox = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.flowLayoutPanel_arrows = new System.Windows.Forms.FlowLayoutPanel();
            this.button_little_up = new System.Windows.Forms.Button();
            this.button_little_down = new System.Windows.Forms.Button();
            this.label_hexVal = new System.Windows.Forms.Label();
            this.label_hex = new System.Windows.Forms.Label();
            this.pixelBox_magnif = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.checkBox_trans = new System.Windows.Forms.CheckBox();
            this.label_current_address = new System.Windows.Forms.Label();
            this.textBox_current_address = new System.Windows.Forms.TextBox();
            this.label_entry = new System.Windows.Forms.Label();
            this.textBox_current = new System.Windows.Forms.TextBox();
            this.label_max = new System.Windows.Forms.Label();
            this.button_go = new System.Windows.Forms.Button();
            this.comboBox_palette = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox)).BeginInit();
            this.flowLayoutPanel_arrows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_magnif)).BeginInit();
            this.SuspendLayout();
            // 
            // pixelBox
            // 
            this.pixelBox.BackgroundImage = global::Zeldix.Properties.Resources.transparency;
            this.pixelBox.Image = null;
            this.pixelBox.Location = new System.Drawing.Point(14, 97);
            this.pixelBox.Name = "pixelBox";
            this.pixelBox.Size = new System.Drawing.Size(257, 257);
            this.pixelBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelBox.TabIndex = 0;
            this.pixelBox.TabStop = false;
            // 
            // flowLayoutPanel_arrows
            // 
            this.flowLayoutPanel_arrows.Controls.Add(this.button_little_up);
            this.flowLayoutPanel_arrows.Controls.Add(this.button_little_down);
            this.flowLayoutPanel_arrows.Location = new System.Drawing.Point(277, 182);
            this.flowLayoutPanel_arrows.Name = "flowLayoutPanel_arrows";
            this.flowLayoutPanel_arrows.Size = new System.Drawing.Size(28, 55);
            this.flowLayoutPanel_arrows.TabIndex = 21;
            // 
            // button_little_up
            // 
            this.button_little_up.BackColor = System.Drawing.Color.Transparent;
            this.button_little_up.FlatAppearance.BorderSize = 0;
            this.button_little_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_little_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_little_up.Image = global::Zeldix.Properties.Resources.gfx4;
            this.button_little_up.Location = new System.Drawing.Point(3, 3);
            this.button_little_up.Name = "button_little_up";
            this.button_little_up.Size = new System.Drawing.Size(20, 20);
            this.button_little_up.TabIndex = 5;
            this.button_little_up.UseVisualStyleBackColor = false;
            this.button_little_up.Click += new System.EventHandler(this.button_little_up_Click);
            // 
            // button_little_down
            // 
            this.button_little_down.BackColor = System.Drawing.Color.Transparent;
            this.button_little_down.FlatAppearance.BorderSize = 0;
            this.button_little_down.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_little_down.Image = global::Zeldix.Properties.Resources.gfx7;
            this.button_little_down.Location = new System.Drawing.Point(3, 29);
            this.button_little_down.Name = "button_little_down";
            this.button_little_down.Size = new System.Drawing.Size(20, 20);
            this.button_little_down.TabIndex = 8;
            this.button_little_down.UseVisualStyleBackColor = false;
            this.button_little_down.Click += new System.EventHandler(this.button_little_down_Click);
            // 
            // label_hexVal
            // 
            this.label_hexVal.AutoSize = true;
            this.label_hexVal.Location = new System.Drawing.Point(265, 81);
            this.label_hexVal.Name = "label_hexVal";
            this.label_hexVal.Size = new System.Drawing.Size(21, 13);
            this.label_hexVal.TabIndex = 33;
            this.label_hexVal.Text = "XX";
            // 
            // label_hex
            // 
            this.label_hex.AutoSize = true;
            this.label_hex.Location = new System.Drawing.Point(240, 81);
            this.label_hex.Name = "label_hex";
            this.label_hex.Size = new System.Drawing.Size(29, 13);
            this.label_hex.TabIndex = 32;
            this.label_hex.Text = "Hex:";
            // 
            // pixelBox_magnif
            // 
            this.pixelBox_magnif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pixelBox_magnif.Image = null;
            this.pixelBox_magnif.Location = new System.Drawing.Point(233, 12);
            this.pixelBox_magnif.Name = "pixelBox_magnif";
            this.pixelBox_magnif.Size = new System.Drawing.Size(66, 66);
            this.pixelBox_magnif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelBox_magnif.TabIndex = 31;
            this.pixelBox_magnif.TabStop = false;
            // 
            // checkBox_trans
            // 
            this.checkBox_trans.AutoSize = true;
            this.checkBox_trans.Checked = true;
            this.checkBox_trans.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_trans.Location = new System.Drawing.Point(104, 36);
            this.checkBox_trans.Name = "checkBox_trans";
            this.checkBox_trans.Size = new System.Drawing.Size(91, 17);
            this.checkBox_trans.TabIndex = 36;
            this.checkBox_trans.Text = "Transparency";
            this.checkBox_trans.UseVisualStyleBackColor = true;
            this.checkBox_trans.CheckedChanged += new System.EventHandler(this.upDatePalette);
            // 
            // label_current_address
            // 
            this.label_current_address.AutoSize = true;
            this.label_current_address.Location = new System.Drawing.Point(11, 15);
            this.label_current_address.Name = "label_current_address";
            this.label_current_address.Size = new System.Drawing.Size(87, 13);
            this.label_current_address.TabIndex = 35;
            this.label_current_address.Text = "Current address: ";
            // 
            // textBox_current_address
            // 
            this.textBox_current_address.Location = new System.Drawing.Point(104, 12);
            this.textBox_current_address.Name = "textBox_current_address";
            this.textBox_current_address.ReadOnly = true;
            this.textBox_current_address.Size = new System.Drawing.Size(84, 20);
            this.textBox_current_address.TabIndex = 34;
            // 
            // label_entry
            // 
            this.label_entry.AutoSize = true;
            this.label_entry.Location = new System.Drawing.Point(12, 65);
            this.label_entry.Name = "label_entry";
            this.label_entry.Size = new System.Drawing.Size(54, 13);
            this.label_entry.TabIndex = 37;
            this.label_entry.Text = "Entry No.:";
            // 
            // textBox_current
            // 
            this.textBox_current.Location = new System.Drawing.Point(104, 62);
            this.textBox_current.MaxLength = 3;
            this.textBox_current.Name = "textBox_current";
            this.textBox_current.Size = new System.Drawing.Size(34, 20);
            this.textBox_current.TabIndex = 38;
            // 
            // label_max
            // 
            this.label_max.AutoSize = true;
            this.label_max.Location = new System.Drawing.Point(144, 65);
            this.label_max.Name = "label_max";
            this.label_max.Size = new System.Drawing.Size(15, 13);
            this.label_max.TabIndex = 39;
            this.label_max.Text = "/ ";
            // 
            // button_go
            // 
            this.button_go.Location = new System.Drawing.Point(184, 60);
            this.button_go.Name = "button_go";
            this.button_go.Size = new System.Drawing.Size(31, 23);
            this.button_go.TabIndex = 40;
            this.button_go.Text = "Go";
            this.button_go.UseVisualStyleBackColor = true;
            this.button_go.Click += new System.EventHandler(this.button_go_Click);
            // 
            // comboBox_palette
            // 
            this.comboBox_palette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_palette.FormattingEnabled = true;
            this.comboBox_palette.Location = new System.Drawing.Point(12, 36);
            this.comboBox_palette.Name = "comboBox_palette";
            this.comboBox_palette.Size = new System.Drawing.Size(74, 21);
            this.comboBox_palette.TabIndex = 41;
            this.comboBox_palette.SelectedIndexChanged += new System.EventHandler(this.upDatePalette);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flowLayoutPanel.Location = new System.Drawing.Point(309, 97);
            this.flowLayoutPanel.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(37, 257);
            this.flowLayoutPanel.TabIndex = 42;
            // 
            // form_compress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 360);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.comboBox_palette);
            this.Controls.Add(this.button_go);
            this.Controls.Add(this.label_max);
            this.Controls.Add(this.textBox_current);
            this.Controls.Add(this.label_entry);
            this.Controls.Add(this.checkBox_trans);
            this.Controls.Add(this.label_current_address);
            this.Controls.Add(this.textBox_current_address);
            this.Controls.Add(this.label_hexVal);
            this.Controls.Add(this.label_hex);
            this.Controls.Add(this.pixelBox_magnif);
            this.Controls.Add(this.flowLayoutPanel_arrows);
            this.Controls.Add(this.pixelBox);
            this.Name = "form_compress";
            this.Text = "ZCompress";
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox)).EndInit();
            this.flowLayoutPanel_arrows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox_magnif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_arrows;
        private System.Windows.Forms.Button button_little_up;
        private System.Windows.Forms.Button button_little_down;
        private System.Windows.Forms.Label label_hexVal;
        private System.Windows.Forms.Label label_hex;
        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox_magnif;
        private System.Windows.Forms.CheckBox checkBox_trans;
        private System.Windows.Forms.Label label_current_address;
        private System.Windows.Forms.TextBox textBox_current_address;
        private System.Windows.Forms.Label label_entry;
        private System.Windows.Forms.TextBox textBox_current;
        private System.Windows.Forms.Label label_max;
        private System.Windows.Forms.Button button_go;
        private System.Windows.Forms.ComboBox comboBox_palette;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
    }
}