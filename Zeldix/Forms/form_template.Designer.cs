﻿namespace Zeldix.Classes.Forms
{
    partial class form_template
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        public virtual void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_template));
            this.SuspendLayout();
            // 
            // form_template
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "form_template";
            this.Opacity = 0.95D;
            this.ResumeLayout(false);

        }
    }
}