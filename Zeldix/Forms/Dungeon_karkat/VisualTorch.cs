﻿using System.Drawing;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Dungeony.Torches;

namespace Zeldix.Forms.Dungeon_karkat
{
    public class VisualTorch : VisualMovable
    {
        public VisualTorch(i_torch torch) { myObject = torch; }

        public Bitmap Image => new Bitmap(Dungeon_constants.image_torch);

        public override string DisplayName => "Torch";
    }
}