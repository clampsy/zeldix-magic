﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Enemies;
using Zeldix.Classes.Forms;

namespace Zeldix.Forms.Dungeon_karkat
{
    public partial class form_Sandbox : form_template
    {
        private Dungeon _dungeon { get; } = new Dungeon();
        private List<string>
            room_datasource,
            original_room_list;

        private ushort MYroomIndex;
        private ushort RoomIndex
        {
            get { return MYroomIndex; }
            set { MYroomIndex = value; }
        }

        public form_Sandbox()
        {
            Dungeon_constants.generateStrings();
            InitializeComponent();
            UpdateRoomList();
        }


        private void UpdateRoomList()
        {
            UpdateList
                (
                ref ListBox_room_list,
                ref original_room_list,
                ref room_datasource,
                "form_dungeon_editor_room_name_op",
                Dungeon.maxRoomNo + 1
                );
        }

        private string FormatNumber(int i)
        { return i.ToString("000"); }

        private void UpdateList(ref ListBox listbox, ref List<string> original_list, ref List<string> datasource, string name, int entryNo)
        {
            listbox.Items.Clear();
            original_list = new List<string>();

            for (int i = 0; i < entryNo; i++)
            {
                string number = FormatNumber(i);
                string text = Settings.getString(name + number);

                if (text != "")
                    text = divider + text;
                text = number + text;

                original_list.Add(text);
            }

            datasource = new List<string>();
            for (int i = 0; i < original_list.Count; i++)
                datasource.Add(original_list[i]);
            listbox.DataSource = datasource;
        }

        private const int substr = 3;
        private const string divider = " - ";

        private void ListBox_rooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = ListBox_room_list.SelectedItem.ToString();

            RoomIndex = ushort.Parse(name.Substring(0, substr));

            if (name.Length > substr + divider.Length)
                textBox_room_name_edit.Text = name.Substring(substr + divider.Length);
            else textBox_room_name_edit.Text = "";

            visualRoom1.Load(_dungeon, RoomIndex);
        }

        private void Button_edit_Click(object sender, EventArgs e)
        {
            string number = FormatNumber(RoomIndex);
            string entry = number;
            if (textBox_room_name_edit.Text != "")
                entry += divider + textBox_room_name_edit.Text;

            int iii = RoomIndex;

            original_room_list[RoomIndex] = entry;

            Settings.AddOrUpdateResource("form_dungeon_editor_room_name_op" + number, textBox_room_name_edit.Text);

            int select = ListBox_room_list.SelectedIndex;
            SearchTextBox_TextChanged();
            ListBox_room_list.SelectedIndex = select;
        }

        /// <summary>
        /// This code is supposed to resize the dungeon canvas
        /// but doesn't work.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel1_Resize(object sender, EventArgs e)
        {
            int gen = (int)(((double)panelEditor.Width * 1.5) / VisualRoom.dim);

            if (gen == 0)
                gen = 1;

            visualRoom1.Width = visualRoom1.Height = VisualRoom.dim * gen;
        }

        private void CheckLayers_CheckedChanged(object sender, EventArgs e)
        {
            //visualRoom1.ShowLayer1 = checkLayersL1.Checked;
            //visualRoom1.ShowLayer2 = checkLayersL2.Checked;
            //visualRoom1.ShowSprites = checkLayerSprites.Checked;
        }

        private void SearchTextBox_TextChanged(object sender = null, EventArgs e = null)
        {
            room_datasource = SearchBoxTools.ListBoxRefinement(original_room_list, SearchTextBox.Text.ToLower());
            ListBox_room_list.DataSource = room_datasource;
        }
    }
}
