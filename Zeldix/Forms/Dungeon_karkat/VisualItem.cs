﻿using System.Drawing;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Dungeony.Items;

namespace Zeldix.Forms.Dungeon_karkat
{
    public class VisualItem : VisualMovable
    {
        public VisualItem(i_item sprite)
        { myObject = sprite; }

        public Bitmap Image
        {
            get
            {
                Image a = new Bitmap(Dungeon_constants.image_item);
                int i = ((i_item)(myObject)).itemID;

                string s = "";
                if (((i_item)(myObject)).isSpecialItem)
                    s = Dungeon_constants.pot_item_special_names[i];
                else s = Dungeon_constants.pot_item_names[i];

                return drawText(s, a);
            }
        }

        public override string DisplayName
        {
            get
            {
                int i = ((i_item)(myObject)).itemID;
                return ((i_item)(myObject)).isSpecialItem ? Dungeon_constants.pot_item_special_names[i] : Dungeon_constants.pot_item_names[i];
            }
        }
    }
}
