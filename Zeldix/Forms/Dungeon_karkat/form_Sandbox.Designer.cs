﻿namespace Zeldix.Forms.Dungeon_karkat
{
    partial class form_Sandbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.visualRoom1 = new Zeldix.Forms.Dungeon_karkat.VisualRoom();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox_room = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.ListBox_room_list = new System.Windows.Forms.ListBox();
            this.flowLayoutPanel_room_current = new System.Windows.Forms.FlowLayoutPanel();
            this.label_room_name = new System.Windows.Forms.Label();
            this.textBox_room_name_edit = new System.Windows.Forms.TextBox();
            this.button_room_name_edit = new System.Windows.Forms.Button();
            this.groupBox_editor = new System.Windows.Forms.GroupBox();
            this.tableEditor = new System.Windows.Forms.TableLayoutPanel();
            this.panelEditor = new System.Windows.Forms.Panel();
            this.groupLayers = new System.Windows.Forms.GroupBox();
            this.flowLayers = new System.Windows.Forms.FlowLayoutPanel();
            this.checkLayersL1 = new System.Windows.Forms.CheckBox();
            this.checkLayersL2 = new System.Windows.Forms.CheckBox();
            this.checkLayerSprites = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox_room.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel_room_current.SuspendLayout();
            this.groupBox_editor.SuspendLayout();
            this.tableEditor.SuspendLayout();
            this.panelEditor.SuspendLayout();
            this.groupLayers.SuspendLayout();
            this.flowLayers.SuspendLayout();
            this.SuspendLayout();
            // 
            // visualRoom1
            // 
            this.visualRoom1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.visualRoom1.Location = new System.Drawing.Point(0, 0);
            this.visualRoom1.MaximumSize = new System.Drawing.Size(512, 512);
            this.visualRoom1.MinimumSize = new System.Drawing.Size(512, 512);
            this.visualRoom1.Name = "visualRoom1";
            this.visualRoom1.ShowLayer1 = true;
            this.visualRoom1.ShowLayer2 = true;
            this.visualRoom1.ShowSprites = true;
            this.visualRoom1.Size = new System.Drawing.Size(512, 512);
            this.visualRoom1.TabIndex = 1;
            this.visualRoom1.Text = "visualRoom1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.13514F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.86486F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox_room, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox_editor, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(904, 519);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // groupBox_room
            // 
            this.groupBox_room.Controls.Add(this.flowLayoutPanel5);
            this.groupBox_room.Controls.Add(this.ListBox_room_list);
            this.groupBox_room.Controls.Add(this.flowLayoutPanel_room_current);
            this.groupBox_room.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_room.Location = new System.Drawing.Point(3, 3);
            this.groupBox_room.Name = "groupBox_room";
            this.groupBox_room.Size = new System.Drawing.Size(266, 513);
            this.groupBox_room.TabIndex = 7;
            this.groupBox_room.TabStop = false;
            this.groupBox_room.Text = "Room";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel5.Controls.Add(this.SearchLabel);
            this.flowLayoutPanel5.Controls.Add(this.SearchTextBox);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(7, 19);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(195, 26);
            this.flowLayoutPanel5.TabIndex = 9;
            // 
            // SearchLabel
            // 
            this.SearchLabel.AutoSize = true;
            this.SearchLabel.Location = new System.Drawing.Point(3, 6);
            this.SearchLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(44, 13);
            this.SearchLabel.TabIndex = 3;
            this.SearchLabel.Text = "Search:";
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchTextBox.Location = new System.Drawing.Point(53, 3);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(108, 20);
            this.SearchTextBox.TabIndex = 4;
            this.SearchTextBox.TextChanged += new System.EventHandler(this.SearchTextBox_TextChanged);
            // 
            // ListBox_room_list
            // 
            this.ListBox_room_list.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBox_room_list.FormattingEnabled = true;
            this.ListBox_room_list.Location = new System.Drawing.Point(3, 88);
            this.ListBox_room_list.Name = "ListBox_room_list";
            this.ListBox_room_list.Size = new System.Drawing.Size(202, 407);
            this.ListBox_room_list.TabIndex = 5;
            this.ListBox_room_list.SelectedIndexChanged += new System.EventHandler(this.ListBox_rooms_SelectedIndexChanged);
            // 
            // flowLayoutPanel_room_current
            // 
            this.flowLayoutPanel_room_current.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel_room_current.Controls.Add(this.label_room_name);
            this.flowLayoutPanel_room_current.Controls.Add(this.textBox_room_name_edit);
            this.flowLayoutPanel_room_current.Controls.Add(this.button_room_name_edit);
            this.flowLayoutPanel_room_current.Location = new System.Drawing.Point(7, 48);
            this.flowLayoutPanel_room_current.Name = "flowLayoutPanel_room_current";
            this.flowLayoutPanel_room_current.Size = new System.Drawing.Size(195, 29);
            this.flowLayoutPanel_room_current.TabIndex = 8;
            // 
            // label_room_name
            // 
            this.label_room_name.AutoSize = true;
            this.label_room_name.Location = new System.Drawing.Point(3, 6);
            this.label_room_name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label_room_name.Name = "label_room_name";
            this.label_room_name.Size = new System.Drawing.Size(38, 13);
            this.label_room_name.TabIndex = 6;
            this.label_room_name.Text = "Name:";
            // 
            // textBox_room_name_edit
            // 
            this.textBox_room_name_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_room_name_edit.Location = new System.Drawing.Point(47, 4);
            this.textBox_room_name_edit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.textBox_room_name_edit.Name = "textBox_room_name_edit";
            this.textBox_room_name_edit.Size = new System.Drawing.Size(99, 20);
            this.textBox_room_name_edit.TabIndex = 7;
            // 
            // button_room_name_edit
            // 
            this.button_room_name_edit.Location = new System.Drawing.Point(152, 3);
            this.button_room_name_edit.Name = "button_room_name_edit";
            this.button_room_name_edit.Size = new System.Drawing.Size(33, 23);
            this.button_room_name_edit.TabIndex = 8;
            this.button_room_name_edit.Text = "Edit";
            this.button_room_name_edit.UseVisualStyleBackColor = true;
            this.button_room_name_edit.Click += new System.EventHandler(this.Button_edit_Click);
            // 
            // groupBox_editor
            // 
            this.groupBox_editor.Controls.Add(this.tableEditor);
            this.groupBox_editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_editor.Location = new System.Drawing.Point(275, 3);
            this.groupBox_editor.Name = "groupBox_editor";
            this.groupBox_editor.Size = new System.Drawing.Size(626, 513);
            this.groupBox_editor.TabIndex = 8;
            this.groupBox_editor.TabStop = false;
            this.groupBox_editor.Text = "Editor";
            // 
            // tableEditor
            // 
            this.tableEditor.AutoSize = true;
            this.tableEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableEditor.ColumnCount = 1;
            this.tableEditor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableEditor.Controls.Add(this.panelEditor, 0, 0);
            this.tableEditor.Controls.Add(this.groupLayers, 0, 1);
            this.tableEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableEditor.Location = new System.Drawing.Point(3, 16);
            this.tableEditor.Name = "tableEditor";
            this.tableEditor.RowCount = 2;
            this.tableEditor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableEditor.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableEditor.Size = new System.Drawing.Size(620, 494);
            this.tableEditor.TabIndex = 3;
            // 
            // panelEditor
            // 
            this.panelEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelEditor.AutoScroll = true;
            this.panelEditor.AutoSize = true;
            this.panelEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelEditor.Controls.Add(this.visualRoom1);
            this.panelEditor.Location = new System.Drawing.Point(3, 3);
            this.panelEditor.Name = "panelEditor";
            this.panelEditor.Size = new System.Drawing.Size(614, 440);
            this.panelEditor.TabIndex = 2;
            this.panelEditor.Resize += new System.EventHandler(this.Panel1_Resize);
            // 
            // groupLayers
            // 
            this.groupLayers.AutoSize = true;
            this.groupLayers.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupLayers.Controls.Add(this.flowLayers);
            this.groupLayers.Location = new System.Drawing.Point(3, 449);
            this.groupLayers.Name = "groupLayers";
            this.groupLayers.Size = new System.Drawing.Size(158, 42);
            this.groupLayers.TabIndex = 3;
            this.groupLayers.TabStop = false;
            this.groupLayers.Text = "Layer View";
            // 
            // flowLayers
            // 
            this.flowLayers.AutoSize = true;
            this.flowLayers.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayers.Controls.Add(this.checkLayersL1);
            this.flowLayers.Controls.Add(this.checkLayersL2);
            this.flowLayers.Controls.Add(this.checkLayerSprites);
            this.flowLayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayers.Location = new System.Drawing.Point(3, 16);
            this.flowLayers.Name = "flowLayers";
            this.flowLayers.Size = new System.Drawing.Size(152, 23);
            this.flowLayers.TabIndex = 0;
            this.flowLayers.WrapContents = false;
            // 
            // checkLayersL1
            // 
            this.checkLayersL1.AutoSize = true;
            this.checkLayersL1.Checked = true;
            this.checkLayersL1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayersL1.Location = new System.Drawing.Point(0, 3);
            this.checkLayersL1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.checkLayersL1.Name = "checkLayersL1";
            this.checkLayersL1.Size = new System.Drawing.Size(47, 17);
            this.checkLayersL1.TabIndex = 1;
            this.checkLayersL1.Text = "BG1";
            this.checkLayersL1.UseVisualStyleBackColor = true;
            this.checkLayersL1.CheckedChanged += new System.EventHandler(this.CheckLayers_CheckedChanged);
            // 
            // checkLayersL2
            // 
            this.checkLayersL2.AutoSize = true;
            this.checkLayersL2.Checked = true;
            this.checkLayersL2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayersL2.Location = new System.Drawing.Point(47, 3);
            this.checkLayersL2.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.checkLayersL2.Name = "checkLayersL2";
            this.checkLayersL2.Size = new System.Drawing.Size(47, 17);
            this.checkLayersL2.TabIndex = 2;
            this.checkLayersL2.Text = "BG2";
            this.checkLayersL2.UseVisualStyleBackColor = true;
            this.checkLayersL2.CheckedChanged += new System.EventHandler(this.CheckLayers_CheckedChanged);
            // 
            // checkLayerSprites
            // 
            this.checkLayerSprites.AutoSize = true;
            this.checkLayerSprites.Checked = true;
            this.checkLayerSprites.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkLayerSprites.Location = new System.Drawing.Point(94, 3);
            this.checkLayerSprites.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.checkLayerSprites.Name = "checkLayerSprites";
            this.checkLayerSprites.Size = new System.Drawing.Size(58, 17);
            this.checkLayerSprites.TabIndex = 3;
            this.checkLayerSprites.Text = "Sprites";
            this.checkLayerSprites.UseVisualStyleBackColor = true;
            this.checkLayerSprites.CheckedChanged += new System.EventHandler(this.CheckLayers_CheckedChanged);
            // 
            // form_Sandbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 519);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "form_Sandbox";
            this.Text = "Sandbox";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox_room.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel_room_current.ResumeLayout(false);
            this.flowLayoutPanel_room_current.PerformLayout();
            this.groupBox_editor.ResumeLayout(false);
            this.groupBox_editor.PerformLayout();
            this.tableEditor.ResumeLayout(false);
            this.tableEditor.PerformLayout();
            this.panelEditor.ResumeLayout(false);
            this.groupLayers.ResumeLayout(false);
            this.groupLayers.PerformLayout();
            this.flowLayers.ResumeLayout(false);
            this.flowLayers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Zeldix.Forms.Dungeon_karkat.VisualRoom visualRoom1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox_room;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label SearchLabel;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.ListBox ListBox_room_list;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_room_current;
        private System.Windows.Forms.Label label_room_name;
        private System.Windows.Forms.TextBox textBox_room_name_edit;
        private System.Windows.Forms.Button button_room_name_edit;
        private System.Windows.Forms.GroupBox groupBox_editor;
        private System.Windows.Forms.Panel panelEditor;
        private System.Windows.Forms.TableLayoutPanel tableEditor;
        private System.Windows.Forms.GroupBox groupLayers;
        private System.Windows.Forms.FlowLayoutPanel flowLayers;
        private System.Windows.Forms.CheckBox checkLayersL1;
        private System.Windows.Forms.CheckBox checkLayersL2;
        private System.Windows.Forms.CheckBox checkLayerSprites;
    }
}