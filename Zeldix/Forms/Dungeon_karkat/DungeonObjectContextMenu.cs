﻿using System;
using System.Windows.Forms;

namespace Zeldix.Forms.Dungeon_karkat
{
    public class DungeonObjectContextMenu : ContextMenu
    {
        private readonly VisualRoom _target;

        public DungeonObjectContextMenu() : base() { }

        public DungeonObjectContextMenu(VisualRoom target) : base()
        {
            _target = target;
            MenuItems.Add(target.ClickHighlight.DisplayName);
            MenuItems.Add("-");
            MenuItems.Add("Add", OnAdd);
            MenuItems.Add("Edit", OnEdit);
            MenuItems.Add("Remove", OnRemove);
        }
        private void OnAdd(object sender, EventArgs e)
        {
            //_target.AddObject(something);
        }

        private void OnEdit(object sender, EventArgs e)
        {
            //doSomething(_target.ClickHighlight);
        }

        private void OnRemove(object sender, EventArgs e)
        {
            _target.RemoveObject(_target.ClickHighlight);
        }
    }
}