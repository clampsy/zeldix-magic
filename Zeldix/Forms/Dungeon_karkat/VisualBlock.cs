﻿using System.Drawing;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Dungeony.Blocks;

namespace Zeldix.Forms.Dungeon_karkat
{
    public class VisualBlock : VisualMovable
    {
        public VisualBlock(i_block block)
        { myObject = block; }

        public Bitmap Image => new Bitmap(Dungeon_constants.image_block);

        public override string DisplayName => "Block";
    }
}