﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Dungeony.Blocks;
using Zeldix.Classes.Dungeony.Items;
using Zeldix.Classes.Dungeony.Sprites;
using Zeldix.Classes.Dungeony.Torches;

namespace Zeldix.Forms.Dungeon_karkat
{
    public class VisualRoom : Control
    {
        public const int dim = 512;

        public readonly Rectangle ROOM_SIZE = new Rectangle(0, 0, dim, dim);

        public IList<VisualSprite> Sprites { get; } = new List<VisualSprite>();
        public IList<VisualBlock> Blocks { get; } = new List<VisualBlock>();
        public IList<VisualTorch> Torches { get; } = new List<VisualTorch>();
        public IList<VisualItem> Items { get; } = new List<VisualItem>();

        public VisualMovable ClickHighlight { get; private set; } = null;
        private VisualMovable _drag_focus = null;

        private Point? _drag_origin = null;
        private Point? _drag_focus_origin = null;

        private bool _show_layer1 = true;
        public bool ShowLayer1
        {
            get { return _show_layer1; }
            set
            {
                _show_layer1 = value;
                Invalidate();
            }
        }

        private bool _show_layer2 = true;
        public bool ShowLayer2
        {
            get { return _show_layer2; }
            set
            {
                _show_layer2 = value;
                Invalidate();
            }
        }

        private bool _show_sprites = true;
        public bool ShowSprites
        {
            get { return _show_sprites; }
            set
            {
                _show_sprites = value;
                Invalidate();
            }
        }

        const int
            SPRITEBASE = 16,
            BLOCKBASE = 8;

        public VisualRoom() { }

        public void AddObject(VisualMovable toAdd)
        {
            if (toAdd is VisualSprite) { Sprites.Add((VisualSprite)toAdd); }
            else if (toAdd is VisualBlock) { Blocks.Add((VisualBlock)toAdd); }
            else if (toAdd is VisualTorch) { Torches.Add((VisualTorch)toAdd); }
            else if (toAdd is VisualItem) { Items.Add((VisualItem)toAdd); }
            Invalidate();
        }

        public void RemoveObject(VisualMovable toRemove)
        {
            if (toRemove is VisualSprite) { Sprites.Remove((VisualSprite)toRemove); }
            else if (toRemove is VisualBlock) { Blocks.Remove((VisualBlock)toRemove); }
            else if (toRemove is VisualTorch) { Torches.Remove((VisualTorch)toRemove); }
            else if (toRemove is VisualItem) { Items.Remove((VisualItem)toRemove); }
            Invalidate();
        }

        public void Load(Dungeon d, ushort room)
        {
            Sprites.Clear();
            Blocks.Clear();
            Torches.Clear();
            Items.Clear();
            foreach (i_sprite next in d.sprite.readAllSprites()[room])
                Sprites.Add(new VisualSprite(next));

            foreach (i_block next in d.block.readBlocks()[room])
                Blocks.Add(new VisualBlock(next));

            foreach (i_torch next in d.torches.readAllTorches()[room])
                Torches.Add(new VisualTorch(next));

            foreach (i_item next in d.item.readAllItems()[room])
                Items.Add(new VisualItem(next));

            Invalidate();
        }

        public void Save()
        {
            //todo copy all object lists back to dungeon object
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
            //Graphics g = e.Graphics;

            //g.FillRectangle(Brushes.Black, 0, 0, Width, Height);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            float x = e.X;
            float y = e.Y;
            x = (x / Width) * ROOM_SIZE.Width;
            y = (y / Height) * ROOM_SIZE.Height;

            Point clickLoc = new Point((int)x, (int)y);
            _drag_origin = clickLoc;

            foreach (VisualSprite sprite in Sprites)
            {
                if (!ShouldDisplay(sprite)) { continue; }
                if ((new Rectangle(sprite.X * SPRITEBASE, sprite.Y * SPRITEBASE, 16, 16)).Contains(clickLoc))
                {
                    _drag_focus = ClickHighlight = sprite;
                    _drag_focus_origin = new Point(sprite.X, sprite.Y);
                    return;
                }
            }
            foreach (VisualBlock bb in Blocks)
            {
                if (!ShouldDisplay(bb)) { continue; }
                if ((new Rectangle(bb.X * BLOCKBASE, bb.Y * BLOCKBASE, 16, 16)).Contains(clickLoc))
                {
                    _drag_focus = ClickHighlight = bb;
                    _drag_focus_origin = new Point(bb.X, bb.Y);
                    return;
                }
            }
            foreach (VisualTorch bb in Torches)
            {
                if (!ShouldDisplay(bb)) { continue; }
                if ((new Rectangle(bb.X * BLOCKBASE, bb.Y * BLOCKBASE, 16, 16)).Contains(clickLoc))
                {
                    _drag_focus = ClickHighlight = bb;
                    _drag_focus_origin = new Point(bb.X, bb.Y);
                    return;
                }
            }
            foreach (VisualItem bb in Items)
            {
                if (!ShouldDisplay(bb)) { continue; }
                if ((new Rectangle(bb.X * BLOCKBASE, bb.Y * BLOCKBASE, 16, 16)).Contains(clickLoc))
                {
                    _drag_focus = ClickHighlight = bb;
                    _drag_focus_origin = new Point(bb.X, bb.Y);
                    return;
                }
            }
            _drag_focus = ClickHighlight = null;
            _drag_focus_origin = null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (_drag_focus == null) { return; }
            if (_drag_origin == null) { return; }
            if (_drag_focus_origin == null) { return; }
            if (ClickHighlight == null) { return; }

            float x = e.X;
            float y = e.Y;
            x = (x / Width) * ROOM_SIZE.Width;
            y = (y / Height) * ROOM_SIZE.Height;

            int multiplier;
            if (ClickHighlight is VisualSprite) { multiplier = SPRITEBASE; }
            else { multiplier = BLOCKBASE; }

            float dx = (x - _drag_origin.Value.X) / multiplier;
            float dy = (y - _drag_origin.Value.Y) / multiplier;
            ClickHighlight.X = (byte)(_drag_focus_origin.Value.X + dx);
            ClickHighlight.Y = (byte)(_drag_focus_origin.Value.Y + dy);

            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            //TODO
            _drag_focus = null;
            _drag_origin = _drag_focus_origin = null;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (e.Button == MouseButtons.Right)
            {
                if (ClickHighlight != null)
                {
                    DungeonObjectContextMenu docm = new DungeonObjectContextMenu(this);
                    docm.Show(this, e.Location);
                }
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (e.Button != MouseButtons.Left) { return; }
            if (ClickHighlight == null) { return; }
            MessageBox.Show(ClickHighlight.DisplayName);
        }

        protected bool ShouldDisplay(VisualMovable mv)
        {
            if (mv is VisualSprite) { return ShowSprites; }
            if (mv.Layer2) { return ShowLayer2; }
            return ShowLayer1;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;

            //Graphics g = e.Graphics;
            Bitmap b = new Bitmap(Dungeon_constants.image_bg);
            using (Graphics g = Graphics.FromImage(b))
            {
                foreach (VisualSprite sprite in Sprites)
                {
                    if (!ShouldDisplay(sprite)) { continue; }
                    g.DrawImage(sprite.Image, new Point(sprite.X * SPRITEBASE, sprite.Y * SPRITEBASE));
                }
                foreach (VisualBlock bb in Blocks)
                {
                    if (!ShouldDisplay(bb)) { continue; }
                    g.DrawImage(bb.Image, new Point(bb.X * BLOCKBASE, bb.Y * BLOCKBASE));
                }
                foreach (VisualTorch bb in Torches)
                {
                    if (!ShouldDisplay(bb)) { continue; }
                    g.DrawImage(bb.Image, new Point(bb.X * BLOCKBASE, bb.Y * BLOCKBASE));
                }
                foreach (VisualItem bb in Items)
                {
                    if (!ShouldDisplay(bb)) { continue; }
                    g.DrawImage(bb.Image, new Point(bb.X * BLOCKBASE, bb.Y * BLOCKBASE));
                }

                e.Graphics.DrawImage(b, new Rectangle(0, 0, Width, Height), new Rectangle(0, 0, b.Width, b.Height), GraphicsUnit.Pixel);
            }
        }
    }
}