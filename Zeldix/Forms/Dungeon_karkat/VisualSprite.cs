﻿using System.Drawing;
using Zeldix.Classes.Dungeony;
using Zeldix.Classes.Dungeony.Sprites;
namespace Zeldix.Forms.Dungeon_karkat
{
    public class VisualSprite : VisualMovable
    {
        public VisualSprite(i_sprite sprite)
        { myObject = sprite; }

        public Bitmap Image
        {
            get
            {
                Image a = new Bitmap(Dungeon_constants.image_sprite);
                int ii = (((i_sprite)(myObject)).spriteId);
                string s = Dungeon_constants.sprite_names[ii];
                return drawText(s, a);
            }
        }

        public override string DisplayName => Dungeon_constants.sprite_names[(((i_sprite)(myObject)).spriteId)];
    }
}