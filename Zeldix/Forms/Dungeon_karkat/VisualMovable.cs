﻿using System.Drawing;
using Zeldix.Classes.Dungeony;

namespace Zeldix.Forms.Dungeon_karkat
{
    public abstract class VisualMovable
    {
        protected LOZobject myObject;
        private const int fontSize = 8;

        public byte X
        {
            get { return myObject.rawX; }
            set { myObject.rawX = value; }
        }

        public byte Y
        {
            get { return myObject.rawY; }
            set { myObject.rawY = value; }
        }

        public abstract string DisplayName { get; }

        public bool Layer2 => myObject.isLayer2;

        public Bitmap drawText(string s, Image a)
        {
            Bitmap result = new Bitmap(a.Width * (s.Length + 1 * fontSize), a.Height);

            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(a, new Point(0, 0));

                for (int i = 1; i >= -1; i -= 2)
                {
                    g.DrawString(s, new Font(FontFamily.GenericMonospace, fontSize), Brushes.Black, i, i);
                    g.DrawString(s, new Font(FontFamily.GenericMonospace, fontSize), Brushes.Black, -i, i);
                    g.DrawString(s, new Font(FontFamily.GenericMonospace, fontSize), Brushes.Black, i, -i);
                    g.DrawString(s, new Font(FontFamily.GenericMonospace, fontSize), Brushes.Black, i, 0);

                }
                g.DrawString(s, new Font(FontFamily.GenericMonospace, fontSize), Brushes.White, 0, 0);
            }
            return result;
        }
    }
}