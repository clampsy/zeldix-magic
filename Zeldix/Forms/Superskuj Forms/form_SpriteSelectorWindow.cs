﻿/*
 * Class        :   form_SpriteSelectorWindow.cs
 * Author       :   Superskuj
 * Description  :   
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Enemies;

namespace AlttpSpriteEditor
{
    public partial class SpriteSelectorWindow : Form
    {
        private List<string> originalList;
        private int index;

        // <Summary>
        // Constructor for this form. Receives a list to display and a reference to an Item to edit.
        // Displays the list in a listbox and determines which list-item to select based on a property of the referenced Item
        // </Summary>
        public SpriteSelectorWindow(List<String> originalList, int index)
        {
            InitializeComponent();
            
            this.originalList = originalList;
            this.index = index;

            for (int i = 0; i < this.originalList.Count; i++)
                SpriteSelectorListBox.Items.Add(this.originalList[i]);

            SpriteSelectorListBox.SelectedIndex = this.index;
        }

        // <Summary>
        // List box refiner, uses a method from SearchBoxTools
        // </Summary>
        private void SearchTextBox_TextChanged(object sender, EventArgs e)
        {
            SpriteSelectorListBox.Items.Clear();

            List<string> refinedList = SearchBoxTools.ListBoxRefinement(originalList, SearchTextBox.Text.ToLower());

            for (int i = 0; i < refinedList.Count; i++)
                SpriteSelectorListBox.Items.Add(refinedList[i]);
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            index = MyMath.hexToDec(SpriteSelectorListBox.SelectedItem.ToString().Substring(1, 2));
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        { this.Close(); }

        public int getIndex()
        { return index; }
    }
}
