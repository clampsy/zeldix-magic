﻿/*
 * Class        :   form_Form1.cs
 * Author       :   Superskuj
 * Description  :   
 */
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Enemies;
using Zeldix.Classes.Forms;

namespace AlttpSpriteEditor
{
    public partial class form_Form1 : form_template
    {
        private List<string> original_list = new List<string>();

        private int[] propertyTableLocation;
        private byte[]
            propertArray,
            prizepack_drop;
        private int
            hp_table_location,

            damage_type_table_location,
            damage_type_definitions_table_location,

            sprite_index,
            damage_type_index,
            prize_pack_index,

            header_offset,

            prizepack_table_location,
            prizepack_drop_chance_table_location,
            prizepack_drops_table_location;
        private TextBox[] myarmor;
        private Button[] dropArray;

        // <Summary>
        // Initialize form and set some default values into some forms
        // </Summary>
        public form_Form1()
        {
            InitializeComponent();

            //SpriteListBox.DataSource = CollectionsLists._sprites_collection;
            for (int i = 0; i < CollectionsLists._sprites_collection.Count; i++)
            {
                original_list.Add(CollectionsLists._sprites_collection[i]);
                SpriteListBox.Items.Add(CollectionsLists._sprites_collection[i]);
            }

            myarmor = new TextBox[3] { Armor1TextBox, Armor2TextBox, Armor3TextBox };
            propertArray = new byte[6];
            prizepack_drop = new byte[8];
            dropArray = new Button[8] { Drop1Button, Drop2Button, Drop3Button, Drop4Button, Drop5Button, Drop6Button, Drop7Button, Drop8Button };
            propertyTableLocation = new int[6];

            ROM();
        }

        // <summary>
        // this menu item loads a rom, converts it to a filestream,
        // copies it to a memorystream for to be edited. Then disposes of the filestream
        // </summary>
        private void ROM()
        {
            header_offset = (RomIO.isHeaderless() == true) ? 0x00 : 0x0200;

            if (RomIO.CheckRegion() != 0)
            {
                int[] peppy = new int[6] { 0x6B080, 0x6B359, 0x6B44C, 0x6B53F, 0x6B632, 0x6B725 };

                for (int i = 0; i < propertyTableLocation.Length; i++)
                    propertyTableLocation[i] = peppy[i] + header_offset;

                hp_table_location = 0x6B173 + header_offset;
                damage_type_table_location = 0x6B266 + header_offset;
                prizepack_table_location = 0x6B632 + header_offset;

                damage_type_definitions_table_location = ((RomIO.CheckRegion() == 1) ? 0x37427 : 0x3742D) + header_offset;
                prizepack_drop_chance_table_location = ((RomIO.CheckRegion() == 1) ? 0x37A5C : 0x37A62) + header_offset;
                prizepack_drops_table_location = ((RomIO.CheckRegion() == 1) ? 0x37A72 : 0x37A78) + header_offset;

                SpriteListBox.SelectedIndex = DamageTypesComboBox.SelectedIndex = PrizePacksComboBox.SelectedIndex = 0;

                TabControlMain.Enabled = true;
            }
        }

        // <Summary>
        // sets a byte in the rom memory stream when it's changed in a form in the sprite editor
        // </Summary>

        private bool mob(byte b, int penpen)
        { return ((b & penpen) == 0x00) ? false : true; }

        private void SpriteListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
                sprite_index = MyMath.hexToDec(SpriteListBox.SelectedItem.ToString().Substring(1, 2));

                HealthTextBox.Text = RomIO.read(hp_table_location + sprite_index).ToString();
                DamageTypeSelector.Value = RomIO.read(damage_type_table_location + sprite_index) & 0x0F;
                PrizePackSelector.Value = RomIO.read(prizepack_table_location + sprite_index) & 0x0F;

                for (int i = 0; i < propertArray.Length; i++)
                    propertArray[i] = RomIO.read(propertyTableLocation[i] + sprite_index);

                //1
                OamSlotsSelector.Value = propertArray[0] & 0x1F;
                CollideLessCheckBox.Checked = mob(propertArray[0], 0x20);
                HarmlessCheckBox.Checked = mob(propertArray[0], 0x80);

                //2
                NoDeathAnimationCheckBox.Checked = mob(propertArray[1], 0x80);
                InvulnerableCheckBox.Checked = mob(propertArray[1], 0x40);
                AdjustChildCoordinatesCheckBox.Checked = mob(propertArray[1], 0x20);
                DrawShadowCheckBox.Checked = mob(propertArray[1], 0x00);
                PaletteSelector.Value = (propertArray[1] & 0x0E) >> 1;

                //3
                IgnoreCollisionCheckBox.Checked = mob(propertArray[2], 0x80);
                StatisCheckBox.Checked = mob(propertArray[2], 0x40);
                PersistCheckBox.Checked = mob(propertArray[2], 0x20);
                HitBoxDimensionsSelector.Value = propertArray[2] & 0x1F;

                //4
                InteractiveTileHitBoxSelector.Value = (propertArray[3] & 0xF0) >> 4;
                DiesLikeABossCheckBox.Checked = mob(propertArray[3], 0x02);
                FallsInHolesCheckBox.Checked = mob(propertArray[3], 0x01);

                //5
                DisableTileInteractionsCheckBox.Checked = mob(propertArray[4], 0x80);
                IsShieldBlockableCheckBox.Checked = mob(propertArray[4], 0x20);
                AlternateDamageSoundCheckBox.Checked = mob(propertArray[4], 0x10);

                //6
                DeflectsProjectilesCheckBox.Checked = mob(propertArray[5], 0x10);
                ImperviousToSwordCheckBox.Checked = mob(propertArray[5], 0x04);
                ImperviousToArrowsCheckBox.Checked = mob(propertArray[5], 0x02);
                CollideLessCheckBox.Checked = mob(propertArray[5], 0x01);
            
        }

        // <Summary>
        // Refines the sprite list as text is entered into a search box
        // </Summary>
        private void SearchTextBox_TextChanged(object sender, EventArgs e)
        {
            SpriteListBox.Items.Clear();

            List<string> refinedList = SearchBoxTools.ListBoxRefinement(original_list, SearchTextBox.Text.ToLower());

            for (int i = 0; i < refinedList.Count; i++)
                SpriteListBox.Items.Add(refinedList[i]);

        }

        private void valNull(ref int? val)
        {
            if (val == null)
                val = 0;
        }

        // </Summary
        // controller for the hp editor text box, checks if an input is valid then updates the hp of the selected
        // sprite in the main memorystream of the rom
        // </Summary>
        private void HealthTextBox_TextChanged(object sender, EventArgs e)
        {
            int? val = TextBoxExceptionHandler.CheckNumericRange(HealthTextBox.Text, 255);
            HealthTextBox.Text = val.ToString();
            valNull(ref val);
            RomIO.writeToArray(hp_table_location + sprite_index, (byte)val);
        }

        // <Summary>
        // controller for the damage type selector. Updates the damage type of the selected sprite
        // when the damage type is changed, within a set range of values
        // </Summary>
        private void burp(NumericUpDown zombieNation, int loc)
        {
            int val = (int)zombieNation.Value,
                val2 = RomIO.read(loc + sprite_index) & 0xF0;
            byte val3 = (byte)(val2 + val);
            RomIO.writeToArray(loc + sprite_index, val3);
        }

        private void DamageTypeSelector_ValueChanged(object sender, EventArgs e)
        { burp(DamageTypeSelector, damage_type_table_location); }
        private void PrizePackSelector_ValueChanged(object sender, EventArgs e)
        { burp(PrizePackSelector, prizepack_table_location); }

        private void javert(NumericUpDown zombieNation, int penpen, int _property_table_location)
        {
            int val = (int)zombieNation.Value;
            byte val2 = (byte)((RomIO.read(_property_table_location + sprite_index) & penpen) | (val << 1));
            RomIO.writeToArray(_property_table_location + sprite_index, val2);
        }

        private void OamSlotsSelector_ValueChanged(object sender, EventArgs e)
        { javert(OamSlotsSelector, 0xE0, propertyTableLocation[0]); }
        private void PaletteSelector_ValueChanged(object sender, EventArgs e)
        { javert(PaletteSelector, 0xF1, propertyTableLocation[1]); }
        private void HitBoxDimensionsSelector_ValueChanged(object sender, EventArgs e)
        { javert(HitBoxDimensionsSelector, 0xE0, propertyTableLocation[2]); }
        private void InteractiveTileHitBoxSelector_ValueChanged(object sender, EventArgs e)
        { javert(InteractiveTileHitBoxSelector, 0x0F, propertyTableLocation[3]); }

        private void checkurPrivelage(CheckBox zombieNation, int penpen, int penpen2, int property_table_location)
        {
            int val = (zombieNation.Checked == true) ? penpen : 0x00;
            byte val2 = (byte)((RomIO.read(property_table_location + sprite_index) & penpen2) | val);
            RomIO.writeToArray(property_table_location + sprite_index, val2);
        }
        private void HarmlessCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(HarmlessCheckBox, 0x80, 0x7F, propertyTableLocation[0]); }
        private void InvulnerableCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(InvulnerableCheckBox, 0x40, 0xBF, propertyTableLocation[1]); }
        private void AdjustChildCoordinatesCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(AdjustChildCoordinatesCheckBox, 0x20, 0xDF, propertyTableLocation[1]); }
        private void DrawShadowCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(DrawShadowCheckBox, 0x10, 0xEF, propertyTableLocation[1]); }
        private void NoDeathAnimationCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(DrawShadowCheckBox, 0x80, 0x7F, propertyTableLocation[1]); }
        private void DiesLikeABossCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(DiesLikeABossCheckBox, 0x02, 0xFD, propertyTableLocation[3]); }
        private void IsShieldBlockableCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(IsShieldBlockableCheckBox, 0x20, 0xDF, propertyTableLocation[4]); }
        private void StatisCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(IsShieldBlockableCheckBox, 0x40, 0xBF, propertyTableLocation[2]); }
        private void PersistCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(PersistCheckBox, 0x20, 0xDF, propertyTableLocation[2]); }
        private void FallsInHolesCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(FallsInHolesCheckBox, 0x01, 0xFE, propertyTableLocation[3]); }
        private void AlternateDamageSoundCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(AlternateDamageSoundCheckBox, 0x10, 0xEF, propertyTableLocation[4]); }
        private void IgnoreCollisionCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(IgnoreCollisionCheckBox, 0x80, 0x7F, propertyTableLocation[2]); }
        private void DisableTileInteractionsCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(DisableTileInteractionsCheckBox, 0x80, 0x7F, propertyTableLocation[4]); }
        private void ImperviousToSwordCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(ImperviousToSwordCheckBox, 0x04, 0xFB, propertyTableLocation[5]); }
        private void DeflectsProjectilesCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(ImperviousToSwordCheckBox, 0x10, 0xEF, propertyTableLocation[5]); }
        private void ImperviousToArrowsCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(ImperviousToArrowsCheckBox, 0x02, 0xFD, propertyTableLocation[5]); }
        private void CollideLessCheckBox_CheckedChanged(object sender, EventArgs e)
        { checkurPrivelage(ImperviousToArrowsCheckBox, 0x01, 0xFE, propertyTableLocation[5]); }

        // <Summary>
        // This is the selector for the damage type currently being edited. Updates editable fields when changed.
        // </Summary>
        private void DamageTypesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            damage_type_index = DamageTypesComboBox.SelectedIndex;
            for (int i = 0; i < myarmor.Length; i++)
                myarmor[i].Text = RomIO.read(damage_type_definitions_table_location + (damage_type_index * 3) + i).ToString();
        }

        // <Summary>
        // Controller for text box which receives damage a sprite does to link with the selected damage type
        // when link is wearing level # armor. Checks for valid inupt then updates the memorystream when changed.
        // </Summary>
        private void armor(TextBox sender, int i)
        {
            int? val = TextBoxExceptionHandler.CheckNumericRange(sender.Text, 255);
            sender.Text = val.ToString();
            valNull(ref val);
            RomIO.writeToArray(damage_type_definitions_table_location + (damage_type_index * 3) + i, (byte)val);
        }

        private void Armor1TextBox_TextChanged(object sender, EventArgs e)
        { armor((TextBox)sender, 0); }
        private void Armor2TextBox_TextChanged(object sender, EventArgs e)
        { armor((TextBox)sender, 1); }
        private void Armor3TextBox_TextChanged(object sender, EventArgs e)
        { armor((TextBox)sender, 2); }

        private void PrizePacksComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            prize_pack_index = PrizePacksComboBox.SelectedIndex;

            int dropPercent = RomIO.read(prizepack_drop_chance_table_location + prize_pack_index);

            if (dropPercent == 1)
                FiftyPercentRadioButton.Checked = true;
            else
                HundredPercentRadioButton.Checked = true;

            for (int i = 0; i < prizepack_drop.Length; i++)
            {
                prizepack_drop[i] = RomIO.read(prizepack_drops_table_location + (prize_pack_index * 8) + i);
                dropArray[i].Text = CollectionsLists._sprites_collection[prizepack_drop[i]].Substring(6);
            }
        }

        private void bob(RadioButton radioBTN)
        {
            int val = (radioBTN.Checked == true) ? 0x01 : 0x00;
            RomIO.writeToArray(prizepack_drop_chance_table_location + prize_pack_index, (byte)val);
        }

        private void FiftyPercentRadioButton_CheckedChanged(object sender, EventArgs e)
        { bob((RadioButton)sender); }
        private void HundredPercentRadioButton_CheckedChanged(object sender, EventArgs e)
        { bob((RadioButton)sender); }

        private void droppy(int i, ref Button btn)
        {
            try
            {
                SpriteSelectorWindow selectorWindow = new SpriteSelectorWindow(original_list, prizepack_drop[i]);
                //selectorWindow.MdiParent = this.MdiParent;
                //selectorWindow.Show();
                selectorWindow.ShowDialog(this);
                dropArray[i].Text = selectorWindow.getIndex().ToString();
                //selectorWindow.Dispose();
                RomIO.writeToArray(prizepack_drops_table_location + (prize_pack_index * 8 + i), Convert.ToByte(dropArray[i].Text));
                btn.Text = CollectionsLists._sprites_collection[Convert.ToInt16(dropArray[i].Text)].Substring(6);
            }
            catch (Exception e) { ErrorMessage.Show("Error: " + e); }
        }

        private void Drop1Button_Click(object sender, EventArgs e)
        { droppy(0, ref Drop1Button); }
        private void Drop2Button_Click(object sender, EventArgs e)
        { droppy(1, ref Drop2Button); }
        private void Drop3Button_Click(object sender, EventArgs e)
        { droppy(2, ref Drop3Button); }
        private void Drop4Button_Click(object sender, EventArgs e)
        { droppy(3, ref Drop4Button); }
        private void Drop5Button_Click(object sender, EventArgs e)
        { droppy(4, ref Drop5Button); }
        private void Drop6Button_Click(object sender, EventArgs e)
        { droppy(5, ref Drop6Button); }
        private void Drop7Button_Click(object sender, EventArgs e)
        { droppy(6, ref Drop7Button); }
        private void Drop8Button_Click(object sender, EventArgs e)
        { droppy(7, ref Drop8Button); }
    }
}