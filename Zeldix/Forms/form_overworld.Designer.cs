﻿namespace Zeldix.Forms
{
    partial class form_overworld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private new void InitializeComponent()
        {
            this.label_room_id = new System.Windows.Forms.Label();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox_gfx_num = new System.Windows.Forms.TextBox();
            this.label_gfx_num = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pixelBox1 = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_room_id
            // 
            this.label_room_id.AutoSize = true;
            this.label_room_id.Location = new System.Drawing.Point(3, 0);
            this.label_room_id.Name = "label_room_id";
            this.label_room_id.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label_room_id.Size = new System.Drawing.Size(52, 19);
            this.label_room_id.TabIndex = 3;
            this.label_room_id.Text = "Room ID:";
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(61, 3);
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(43, 20);
            this.numericUpDown.TabIndex = 5;
            this.numericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 52);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(351, 319);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            // 
            // textBox_gfx_num
            // 
            this.textBox_gfx_num.Location = new System.Drawing.Point(173, 3);
            this.textBox_gfx_num.Name = "textBox_gfx_num";
            this.textBox_gfx_num.Size = new System.Drawing.Size(33, 20);
            this.textBox_gfx_num.TabIndex = 7;
            // 
            // label_gfx_num
            // 
            this.label_gfx_num.AutoSize = true;
            this.label_gfx_num.Location = new System.Drawing.Point(110, 0);
            this.label_gfx_num.Name = "label_gfx_num";
            this.label_gfx_num.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label_gfx_num.Size = new System.Drawing.Size(57, 19);
            this.label_gfx_num.TabIndex = 8;
            this.label_gfx_num.Text = "Graphic #:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label_room_id);
            this.flowLayoutPanel1.Controls.Add(this.numericUpDown);
            this.flowLayoutPanel1.Controls.Add(this.label_gfx_num);
            this.flowLayoutPanel1.Controls.Add(this.textBox_gfx_num);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(351, 34);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // pixelBox1
            // 
            this.pixelBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pixelBox1.Image = null;
            this.pixelBox1.Location = new System.Drawing.Point(370, 52);
            this.pixelBox1.Name = "pixelBox1";
            this.pixelBox1.Size = new System.Drawing.Size(180, 319);
            this.pixelBox1.TabIndex = 10;
            this.pixelBox1.TabStop = false;
            // 
            // form_overworld
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 383);
            this.Controls.Add(this.pixelBox1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.richTextBox1);
            this.Name = "form_overworld";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Overworld";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label_room_id;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox_gfx_num;
        private System.Windows.Forms.Label label_gfx_num;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox1;
    }
}