﻿using System;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Dungeony;

namespace Zeldix.Classes.Forms
{
    public partial class form_dungeon_repointer : form_template
    {
        private readonly Dungeon dung;

        internal form_dungeon_repointer(): base()
        {
            dung = new Dungeon();

            InitializeComponent();
            updateRoomPointers();
            updateSpritePointers();
        }

        private void updateRoomPointers()
        { textBox_rm_pnt.Text = MyMath.decToHex(dung.room.getPrimaryPointer(), 1); }

        private void updateSpritePointers()
        { textBox_sp_pnt.Text = MyMath.decToHex(dung.sprite.getPointer(), 1); }

        private void button_room_repoint_go_Click(object sender, EventArgs e)
        {
            try
            {
                dung.room.movePointers(MyMath.hexToDec(textBox_rm_pnt_mv.Text));
                updateRoomPointers();
            }
            catch(Exception ee) { ErrorMessage.Show(ee); }
        }

        private void button_sp_pnt_go_Click(object sender, EventArgs e)
        {
            try { dung.sprite.movePointers(MyMath.hexToDec(textBox_sp_pnt_mv.Text)); }
            catch (Exception ee) { ErrorMessage.Show(ee); }
        }
    }
}