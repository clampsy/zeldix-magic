﻿using Zeldix.Classes.Forms;

namespace Zeldix.Forms
{
    partial class form_monologue : form_template
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private new void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ListBox_room_list = new System.Windows.Forms.ListBox();
            this.button_set = new System.Windows.Forms.Button();
            this.richTextBox_selected = new System.Windows.Forms.RichTextBox();
            this.pixelBox1 = new Zeldix.Classes.GraphicBoxSelect.PixelBox();
            this.button_scroll_up = new System.Windows.Forms.Button();
            this.button_scroll_down = new System.Windows.Forms.Button();
            this.groupBox_edit = new System.Windows.Forms.GroupBox();
            this.Button_commit = new System.Windows.Forms.Button();
            this.flowLayoutPanel_text = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox_entry = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel_room_current = new System.Windows.Forms.FlowLayoutPanel();
            this.label_room_name = new System.Windows.Forms.Label();
            this.textBox_room_name_edit = new System.Windows.Forms.TextBox();
            this.button_room_name_edit = new System.Windows.Forms.Button();
            this.groupBox_preview = new System.Windows.Forms.GroupBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).BeginInit();
            this.groupBox_edit.SuspendLayout();
            this.groupBox_entry.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel_room_current.SuspendLayout();
            this.groupBox_preview.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListBox_room_list
            // 
            this.ListBox_room_list.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBox_room_list.FormattingEnabled = true;
            this.ListBox_room_list.Location = new System.Drawing.Point(3, 84);
            this.ListBox_room_list.Name = "ListBox_room_list";
            this.ListBox_room_list.ScrollAlwaysVisible = true;
            this.ListBox_room_list.Size = new System.Drawing.Size(173, 251);
            this.ListBox_room_list.TabIndex = 0;
            this.ListBox_room_list.SelectedIndexChanged += new System.EventHandler(this.ListBox_rooms_SelectedIndexChanged);
            // 
            // button_set
            // 
            this.button_set.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_set.Location = new System.Drawing.Point(180, 356);
            this.button_set.Name = "button_set";
            this.button_set.Size = new System.Drawing.Size(75, 23);
            this.button_set.TabIndex = 1;
            this.button_set.Text = "Commit edit";
            this.button_set.UseVisualStyleBackColor = true;
            // 
            // richTextBox_selected
            // 
            this.richTextBox_selected.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_selected.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_selected.Location = new System.Drawing.Point(14, 19);
            this.richTextBox_selected.Name = "richTextBox_selected";
            this.richTextBox_selected.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox_selected.Size = new System.Drawing.Size(410, 143);
            this.richTextBox_selected.TabIndex = 7;
            this.richTextBox_selected.Text = "";
            // 
            // pixelBox1
            // 
            this.pixelBox1.AccessibleDescription = "";
            this.pixelBox1.Image = global::Zeldix.Properties.Resources.window;
            this.pixelBox1.Location = new System.Drawing.Point(6, 19);
            this.pixelBox1.Name = "pixelBox1";
            this.pixelBox1.Size = new System.Drawing.Size(380, 124);
            this.pixelBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pixelBox1.TabIndex = 21;
            this.pixelBox1.TabStop = false;
            // 
            // button_scroll_up
            // 
            this.button_scroll_up.Enabled = false;
            this.button_scroll_up.Image = global::Zeldix.Properties.Resources.gfx3;
            this.button_scroll_up.Location = new System.Drawing.Point(392, 45);
            this.button_scroll_up.Name = "button_scroll_up";
            this.button_scroll_up.Size = new System.Drawing.Size(27, 23);
            this.button_scroll_up.TabIndex = 22;
            this.button_scroll_up.UseVisualStyleBackColor = true;
            this.button_scroll_up.Click += new System.EventHandler(this.Button_scroll_up_Click);
            // 
            // button_scroll_down
            // 
            this.button_scroll_down.Enabled = false;
            this.button_scroll_down.Image = global::Zeldix.Properties.Resources.gfx8;
            this.button_scroll_down.Location = new System.Drawing.Point(392, 74);
            this.button_scroll_down.Name = "button_scroll_down";
            this.button_scroll_down.Size = new System.Drawing.Size(27, 23);
            this.button_scroll_down.TabIndex = 23;
            this.button_scroll_down.UseVisualStyleBackColor = true;
            this.button_scroll_down.Click += new System.EventHandler(this.Button_scroll_down_Click);
            // 
            // groupBox_edit
            // 
            this.groupBox_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_edit.Controls.Add(this.Button_commit);
            this.groupBox_edit.Controls.Add(this.richTextBox_selected);
            this.groupBox_edit.Controls.Add(this.button_set);
            this.groupBox_edit.Location = new System.Drawing.Point(7, 32);
            this.groupBox_edit.Name = "groupBox_edit";
            this.groupBox_edit.Size = new System.Drawing.Size(430, 197);
            this.groupBox_edit.TabIndex = 29;
            this.groupBox_edit.TabStop = false;
            this.groupBox_edit.Text = "Edit";
            // 
            // Button_commit
            // 
            this.Button_commit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_commit.Location = new System.Drawing.Point(180, 168);
            this.Button_commit.Name = "Button_commit";
            this.Button_commit.Size = new System.Drawing.Size(75, 23);
            this.Button_commit.TabIndex = 8;
            this.Button_commit.Text = "Commit edit";
            this.Button_commit.UseVisualStyleBackColor = true;
            this.Button_commit.Click += new System.EventHandler(this.Button_commit_Click);
            // 
            // flowLayoutPanel_text
            // 
            this.flowLayoutPanel_text.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel_text.Location = new System.Drawing.Point(20, 33);
            this.flowLayoutPanel_text.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel_text.Name = "flowLayoutPanel_text";
            this.flowLayoutPanel_text.Size = new System.Drawing.Size(352, 96);
            this.flowLayoutPanel_text.TabIndex = 24;
            // 
            // groupBox_entry
            // 
            this.groupBox_entry.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_entry.Controls.Add(this.flowLayoutPanel5);
            this.groupBox_entry.Controls.Add(this.flowLayoutPanel_room_current);
            this.groupBox_entry.Controls.Add(this.ListBox_room_list);
            this.groupBox_entry.Location = new System.Drawing.Point(445, 32);
            this.groupBox_entry.Name = "groupBox_entry";
            this.groupBox_entry.Size = new System.Drawing.Size(184, 358);
            this.groupBox_entry.TabIndex = 30;
            this.groupBox_entry.TabStop = false;
            this.groupBox_entry.Text = "Select Entry";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel5.Controls.Add(this.SearchLabel);
            this.flowLayoutPanel5.Controls.Add(this.SearchTextBox);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(7, 19);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(171, 26);
            this.flowLayoutPanel5.TabIndex = 11;
            // 
            // SearchLabel
            // 
            this.SearchLabel.AutoSize = true;
            this.SearchLabel.Location = new System.Drawing.Point(3, 6);
            this.SearchLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(44, 13);
            this.SearchLabel.TabIndex = 3;
            this.SearchLabel.Text = "Search:";
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchTextBox.Location = new System.Drawing.Point(53, 3);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(108, 20);
            this.SearchTextBox.TabIndex = 4;
            this.SearchTextBox.TextChanged += new System.EventHandler(this.SearchTextBox_TextChanged);
            // 
            // flowLayoutPanel_room_current
            // 
            this.flowLayoutPanel_room_current.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel_room_current.Controls.Add(this.label_room_name);
            this.flowLayoutPanel_room_current.Controls.Add(this.textBox_room_name_edit);
            this.flowLayoutPanel_room_current.Controls.Add(this.button_room_name_edit);
            this.flowLayoutPanel_room_current.Location = new System.Drawing.Point(7, 48);
            this.flowLayoutPanel_room_current.Name = "flowLayoutPanel_room_current";
            this.flowLayoutPanel_room_current.Size = new System.Drawing.Size(171, 29);
            this.flowLayoutPanel_room_current.TabIndex = 10;
            // 
            // label_room_name
            // 
            this.label_room_name.AutoSize = true;
            this.label_room_name.Location = new System.Drawing.Point(3, 6);
            this.label_room_name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label_room_name.Name = "label_room_name";
            this.label_room_name.Size = new System.Drawing.Size(38, 13);
            this.label_room_name.TabIndex = 6;
            this.label_room_name.Text = "Name:";
            // 
            // textBox_room_name_edit
            // 
            this.textBox_room_name_edit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_room_name_edit.Location = new System.Drawing.Point(47, 4);
            this.textBox_room_name_edit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.textBox_room_name_edit.Name = "textBox_room_name_edit";
            this.textBox_room_name_edit.Size = new System.Drawing.Size(78, 20);
            this.textBox_room_name_edit.TabIndex = 7;
            // 
            // button_room_name_edit
            // 
            this.button_room_name_edit.Location = new System.Drawing.Point(131, 3);
            this.button_room_name_edit.Name = "button_room_name_edit";
            this.button_room_name_edit.Size = new System.Drawing.Size(33, 23);
            this.button_room_name_edit.TabIndex = 8;
            this.button_room_name_edit.Text = "Edit";
            this.button_room_name_edit.UseVisualStyleBackColor = true;
            this.button_room_name_edit.Click += new System.EventHandler(this.Button_edit_Click);
            // 
            // groupBox_preview
            // 
            this.groupBox_preview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_preview.Controls.Add(this.flowLayoutPanel_text);
            this.groupBox_preview.Controls.Add(this.pixelBox1);
            this.groupBox_preview.Controls.Add(this.button_scroll_down);
            this.groupBox_preview.Controls.Add(this.button_scroll_up);
            this.groupBox_preview.Location = new System.Drawing.Point(7, 235);
            this.groupBox_preview.Name = "groupBox_preview";
            this.groupBox_preview.Size = new System.Drawing.Size(433, 155);
            this.groupBox_preview.TabIndex = 25;
            this.groupBox_preview.TabStop = false;
            this.groupBox_preview.Text = "Preview";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(632, 24);
            this.menuStrip1.TabIndex = 33;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // form_monologue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 393);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox_preview);
            this.Controls.Add(this.groupBox_entry);
            this.Controls.Add(this.groupBox_edit);
            this.DoubleBuffered = true;
            this.HelpButton = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "form_monologue";
            this.Text = "Text";
            ((System.ComponentModel.ISupportInitialize)(this.pixelBox1)).EndInit();
            this.groupBox_edit.ResumeLayout(false);
            this.groupBox_entry.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel_room_current.ResumeLayout(false);
            this.flowLayoutPanel_room_current.PerformLayout();
            this.groupBox_preview.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ListBox_room_list;
        private System.Windows.Forms.Button button_set;
        private System.Windows.Forms.RichTextBox richTextBox_selected;
        private Zeldix.Classes.GraphicBoxSelect.PixelBox pixelBox1;
        private System.Windows.Forms.Button button_scroll_up;
        private System.Windows.Forms.Button button_scroll_down;
        private System.Windows.Forms.GroupBox groupBox_edit;
        private System.Windows.Forms.GroupBox groupBox_entry;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_text;
        private System.Windows.Forms.GroupBox groupBox_preview;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label SearchLabel;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_room_current;
        private System.Windows.Forms.Label label_room_name;
        private System.Windows.Forms.TextBox textBox_room_name_edit;
        private System.Windows.Forms.Button button_room_name_edit;
        private System.Windows.Forms.Button Button_commit;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
    }
}