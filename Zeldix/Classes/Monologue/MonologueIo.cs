﻿/* Class        : MonologueIo.cs
 * Author       : clampsy
 * Description  : Deals with data structuring for monologue editor
 */

using System.Collections.Generic;
using Zeldix.Classes.Crap;
using System.Linq;
using System;

namespace Zeldix.Classes.Monologue
{
    internal class MonologueIo
    {
        private readonly int
            bank1_address,
            bank2_address,

            bank1EntryNo,
            bank2EntryNo;


        private readonly CharacterTable chara;

        private List<List<byte>> scriptAsBytes;

        public MonologueIo()
        {
            chara = new CharacterTable(true);

            bank1_address = AddressLoROM.snesToPc(
                RomIO.read(Constants.mono_bank01_U_lo),
                RomIO.read(Constants.mono_bank01_U_hi),
                RomIO.read(Constants.mono_bank01_U_bank));

            bank2_address = AddressLoROM.snesToPc(
                RomIO.read(Constants.mono_bank02_U_lo),
                RomIO.read(Constants.mono_bank02_U_hi),
                RomIO.read(Constants.mono_bank02_U_bank));

            int
                indexOfNextLineChar = chara.getValueOfChar(Constants.SEP.ToString()),
                indexOfBlank = chara.getValueOfChar(Constants.FREESPACE);

            chara = new CharacterTable(true);


            List<List<byte>>
                bank1 = GetScriptInBytes(bank1_address, Constants.mono_bank01_U_size),
                bank2 = GetScriptInBytes(bank2_address, Constants.mono_bank02_U_size);

            bank1EntryNo = bank1.Count;
            bank2EntryNo = bank2.Count;

            scriptAsBytes = bank1.Concat(bank2).ToList();
        }

        public List<List<byte>> GetScriptInBytes(int pos, int size)
        {
            int indexOfNextMonologueChar = chara.getValueOfChar(Constants.SEP.ToString()),
                indexOfBlank = chara.getValueOfChar(Constants.FREESPACE);

            List<byte> st = new List<byte>();
            List<List<byte>> byteList = new List<List<byte>>();

            foreach (byte b in RomIO.read(pos, size))
            {
                if (b.Equals((byte)indexOfNextMonologueChar))
                {
                    byteList.Add(st);
                    st = new List<byte>();
                }
                else st.Add(b);
            }

            return byteList;
        }

        public int NumberOfMonologueEntries
        { get { return scriptAsBytes.Count; } }

        public string ByteToText(int MonologueNumber)
        {
            if (MonologueNumber >= 0 && MonologueNumber <= NumberOfMonologueEntries)
                return ByteToText(scriptAsBytes[MonologueNumber]);
            else throw new ArgumentOutOfRangeException();
        }

        public string ByteToText(byte b)
        { return ByteToText(new List<byte> { b }); }

        public string ByteToText(List<byte> ByteStream)
        {
            string s = chara.toChara(ByteStream).Replace(Constants.SEP.ToString(), "");
            return s.Replace(Constants.NEWLINE, "\n").Replace("\r", "");
        }

        public void InsertMonologueEntry(int MonologueNumber, string s)
        {
            if (MonologueNumber >= 0 && MonologueNumber <= NumberOfMonologueEntries)
                scriptAsBytes[MonologueNumber] = ConvertScriptToHex(s);
            else throw new ArgumentOutOfRangeException();
        }

        private List<byte> ConvertScriptToHex(string line)
        {
            line = line.Replace("\n", Constants.NEWLINE);
            List<string[]> list;
            string s;
            List<byte> output = new List<byte>();

            /* The final game's first entry is blank, so I can't throw an exception
             * for a blank line.
             */

            if (!line.Equals(""))
                for (int i = 0; i < line.Length; i++)
                {
                    list = chara.getDictionaryListForChar(line[i]);
                    for (int j = Math.Min(chara.max(list), line.Length); j > 0; j--)
                    {
                        s = ((i + j < line.Length) ? line.Substring(i, j) : line.Substring(i, line.Length - i));
                        foreach (string[] str in list)
                            if (str[1].Equals(s))
                            {
                                int aa = MyMath.hexToDec(str[0]);

                                //If aa is larger than a byte
                                //Then we have to split it into two bytes
                                if (aa > byte.MaxValue)
                                {
                                    //This place previously inserted "aa" twice 
                                    //looked like a logical bug so here's this code.
                                    Conversion.ToBytes(aa, out byte hi, out byte lo);
                                    output.Add(hi);
                                    output.Add(lo);
                                }
                                else output.Add(MyMath.hexToByte(str[0]));
                                i += s.Length - 1;
                                goto Getout;
                                /* A case of goto used correctly
                                 * (Exiting out of a loop). Hope a T-Rex doesn't
                                 * jump out of my computer for this.
                                 * If you are bigoted against the goto
                                 * and think less of me for using it correctly,
                                 * then screw you. :D
                                 */
                            }
                    }
                    throw new KeyNotFoundException("Character not in table.");
                    Getout:;
                }
            //output.Add((byte)chara.getValueOfChar(Constants.SEP.ToString()));
            return output;
        }

        public bool WriteListToArray()
        {
            List<List<byte>> scriptAsBytes1 = new List<List<byte>>(), scriptAsBytes2 = new List<List<byte>>();

            int i = 0;
            foreach(List<byte> a in scriptAsBytes)
            {
                if (i++ < bank1EntryNo)
                    scriptAsBytes1.Add(a);
                else scriptAsBytes2.Add(a);
            }

            if (WriteListToArray(bank1_address, bank1EntryNo, Constants.mono_bank01_U_size, scriptAsBytes1))
                return WriteListToArray(bank2_address, bank2EntryNo, Constants.mono_bank02_U_size, scriptAsBytes2);
            else return false;
        }

        /// <summary>
        /// Writes the stored monologues to the ROM array.
        /// </summary>
        /// <returns></returns>
        private bool WriteListToArray(int address, int entryNumbers, int byteSize, List<List<byte>> byteList)
        {
            byte[] b = new byte[byteSize];
            int k = 0;
            byte emptySpace = (byte)chara.getValueOfChar(Constants.FREESPACE);
            foreach (List<byte> l in byteList)
            {
                foreach (byte bb in l)
                {
                    if (!(bb.Equals(emptySpace)))
                        b[k++] = bb;
                }
                b[k++] = (byte)chara.getValueOfChar(Constants.SEP.ToString());
            }
            bool pass = false;

            if (b.Length <= byteSize)
            {
                for (int i = 0; i < byteSize; i++)
                    if (i <= b.Length)
                        RomIO.writeToArray(i + address, b[i]);
                    else
                        //if the monologues don't fill up all the space, write 0xFF
                        RomIO.writeToArray(i + address, emptySpace);


                pass = true;
            }
            else throw new IndexOutOfRangeException("Data is too big to insert.");
            return pass;
        }
    }
}