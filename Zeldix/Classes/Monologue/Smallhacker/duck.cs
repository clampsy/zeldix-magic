﻿using System.Collections.Generic;
using Zeldix.Classes.Crap;
namespace Zeldix.Classes.Monologue.Smallhacker
{
    public class duck
    {
        public int
            TILE_COUNT = 512,
            WIDTH = 11,
            HEIGHT = 16,

            START_OF_TILE_DATA = 0x70000,
            START_OF_TILE_BITMASK = 0x73844,
            MAX_TILE_DATA_LENGTH = 0x3844;


        public duck() { }

        public List<Tile> load()
        {
            int tilePointer = START_OF_TILE_DATA,
                bitmaskPointer = START_OF_TILE_BITMASK;

            List<Tile> tiles = new List<Tile>();

            for (int tile = 0; tile < TILE_COUNT; tile++)
            {
                long bitmask = 0;
                for (int i = 0; i < 5; i++)
                {
                    bitmask <<= 8;
                    bitmask |= RomIO.read(bitmaskPointer++);
                }

                byte[] packed = new byte[44];

                for (int i = 0; i < 40; i++)
                {
                    if ((bitmask & 0x8000000000L) != 0)
                        packed[i + 4] = RomIO.read(tilePointer++);
                    bitmask <<= 1;
                }
                tiles.Add(new Tile(tile, unpack(packed)));
            }
            return tiles;
        }

        public struct Tile
        {
            public int index;
            public byte[] pixels;

            public Tile(int index, byte[] pixels)
            {
                this.index = index;
                this.pixels = pixels;
            }
        }

        private byte[] unpack(byte[] packed)
        {
            int LENGTH = WIDTH * HEIGHT / 8;
            byte[] unpacked = new byte[WIDTH * HEIGHT];
            for (int i = 0; i < LENGTH; i++)
            {
                byte low = packed[i * 2];
                byte high = packed[i * 2 + 1];
                for (int bit = 0; bit < 8; bit++)
                {
                    byte pixel = (byte)(((low >> 7) & 1) | ((high >> 6) & 2));
                    int index = toCoords(i, bit);

                    unpacked[index] = pixel;

                    low <<= 1;
                    high <<= 1;
                }
            }
            return unpacked;
        }

        private byte[] pack()
        {
            const int thing = 44,
                number = 2;

            byte[] packed = new byte[thing];
            for (byte i = 0; i < thing; i += number)
            {
                byte
                    low = 0,
                    high = 0;

                byte x, y;

                byte j = (byte)(i / number);
                if (j < 11)
                {
                    if (j < 8)
                    {
                        x = 0;
                        y = j;
                    }
                    else
                    {
                        x = j;
                        y = 0;
                    }
                }
                else
                {
                    if (j < 19)
                    {
                        x = 0;
                        y = (byte)(j - 3);
                    }
                    else
                    {
                        x = (byte)(j - 11);
                        y = 8;
                    }
                }

                byte
                    dx = (byte)(x == 0 ? 1 : 0),
                    dy = (byte)(1 - dx);

                for (int bit = 0; bit < 8; bit++)
                {
                    low <<= 1;
                    high <<= 1;
                    int val = packed[coordsToIndex(x, y)];
                    low |= (byte)(val & 1);
                    val >>= 1;
                    high |= (byte)(val & 1);

                    x += dx;
                    y += dy;
                }
                packed[i] = low;
                packed[i + 1] = high;
            }
            return packed;
        }

        private int toCoords(int row, int bit)
        {
            int ret = 0;

            if (row < 8)
                ret = coordsToIndex(bit, row);
            else if (row < 11)
                ret = coordsToIndex(row, bit);
            else if (row < 19)
                ret = coordsToIndex(bit, row - 3);
            else
                ret = coordsToIndex(row - 19 + 8, bit + 8);
            return ret;
        }

        public int coordsToIndex(int x, int y)
        { return x + y * WIDTH; }
    }
}
