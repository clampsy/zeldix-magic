﻿using System;
using System.Collections.Generic;
using Zeldix.Classes.Crap;

delegate int multiply(int x);
namespace Zeldix.Classes.Monologue
{
    internal class DTE
    {
        private const int
            pointerStartAddress = 0x74703,
            numberOfEntries = 0x61;

        /// <summary>
        /// Initilializes a new instance of DTE
        /// </summary>
        /// <param name="rom"></param>
        internal DTE()
        {

        }

        internal List<string> readDictionary()
        {
            multiply mm = x => x * 2;


            /* This looks like a bug. 
             * but correcting it crashes the program. So
             * I guess if this works, leave it...
             */
            int bank = AddressLoROM.pcToSnes_Bank(pointerStartAddress);

            List<int> pointers = new List<int>();
            List<string> strings = new List<string>();

            for (int i = 0; i < numberOfEntries; i++)
                pointers.Add(AddressLoROM.snesToPc(
                    RomIO.read(pointerStartAddress + mm(i)),
                    RomIO.read(pointerStartAddress + mm(i) + 1),
                    bank)); //bug?

            CharacterTable charT = new CharacterTable(false);
            for (int i = 0; i < numberOfEntries; i++)
                if (i != numberOfEntries - 1)
                    strings.Add(charT.toChara(RomIO.read(pointers[i], pointers[i + 1] - pointers[i])));
                else strings.Add(charT.toChara(RomIO.read(pointers[i], 3)));
            return strings;
        }

        internal void insertDTEIntoTable(ref SortedDictionary<int, string> table)
        {
            List<string> aa = readDictionary();
            for (int i = 0x80; i < byte.MaxValue; i++)
                table[i] = Constants.INVALID;
            const int temp = 0x88;
            for (int i = temp; i < temp + aa.Count; i++)
                table[i] = aa[i - temp];
        }
    }
}
