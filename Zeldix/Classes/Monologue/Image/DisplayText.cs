﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;
using Zeldix.Classes.GraphicBoxSelect;

/* GLITCH:
 * The line drawing doesn't glitch out if it is
 * 1 character too long. It will simply cut it off.
 * If it is 2characters too long, the line will glitch.
 * The game also glitches out if you try to write a too
 * long of a line, so this is okay.
 */

namespace Zeldix.Classes.Monologue.Image
{
    /// <summary>
    /// Displays the preview text like it would appear in-game
    /// </summary>
    public static class DisplayText
    {
        private static int
            height = Constants.BLOCK_DIM * 2,
            width = charactersPerLine * Constants.BLOCK_DIM,
            scale;
        private const int charactersPerLine = 22;

        /// <summary>
        /// Return list of pixelboxes for each monologue line.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static List<PixelBox> Display(List<object> list, int scale = 1)
        {
            /* I really shouldn't be using GUI components (PixelBoxes)
             * in a class. It's a remnant of what the GUI
             * was originally going to be.
             */

            if (scale < 1)
                throw new ArgumentOutOfRangeException();
            if (list == null)
                throw new ArgumentNullException();

            DisplayText.scale = scale;
            List<Bitmap> tile = new List<Bitmap>();
            List<PixelBox> lines = new List<PixelBox>();
            Bitmap b;
            foreach (object o in list)
            {
                b = o as Bitmap;
                if (b != null)
                    tile.Add(b);
                else
                {
                    if ((o as string).Contains(Constants.NEWLINE))
                    {
                        add(tile, ref lines);
                        tile = new List<Bitmap>();
                    }
                }
            }

            if (tile.Count > 0)
                add(tile, ref lines);
            return lines;
        }
        private static void add(List<Bitmap> tile, ref List<PixelBox> lines)
        {
            if (tile == null)
                throw new ArgumentNullException();

            //Again, I shouldn't be using GUI components in a class. 
            PixelBox pic = new PixelBox(width * scale, height * scale);
            pic.Margin = new Padding(0);
            pic.SizeMode = PictureBoxSizeMode.Zoom;
            if (tile.Count > 0)
                pic.Image = BMPMerge.mergeBitmaps(tile, Constants.BLOCK_DIM, width / Constants.BLOCK_DIM);
            else pic.Image = new Bitmap(width, height);
            lines.Add(pic);
        }
    }
}