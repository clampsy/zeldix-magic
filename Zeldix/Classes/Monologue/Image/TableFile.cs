﻿using System;
using System.Collections.Generic;
using System.IO;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.Monologue.Image
{
    /// <summary>
    /// Reads the table file from the resource folder.
    /// </summary>
    public static class TableFile
    {
        delegate string GetString(string str);
        delegate int GetHexValue(string str);
        delegate string[] SplitString(string str);
        private const string filename = "charset.tbl";

        /// <summary>
        /// Returns a sorted DTE table for fast time
        /// </summary>
        public static SortedDictionary<int, string> getTableFileDictionary()
        {
            GetString getstringFromFileLine = str => str.Substring(str.IndexOf(Constants.SEPERATOR) + Constants.SEPERATOR.Length).ToString();
            SortedDictionary<int, string> table = new SortedDictionary<int, string>();
            SplitString split = str => str.Split(Constants.newLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            GetHexValue getHexValueFromFileLine = str => MyMath.hexToDec(str.Substring(0, str.IndexOf(Constants.SEPERATOR)));

            if (!File.Exists(Constants.PATH + filename))
                throw new FileNotFoundException(
                    "The Table file doesn't exist in the path: " + Constants.PATH + filename +
                    "\nIf you are compiling the utility yourself, make sure the debug " +
                    "variable in the Crap.Global class is set to false");

            using (StreamReader sr = new StreamReader(Constants.PATH + filename))
                foreach (string fileLine in split(sr.ReadToEnd()))
                    if (!table.ContainsKey(getHexValueFromFileLine(fileLine)))
                        //if (!table.ContainsValue(getstringFromFileLine(fileLine)))
                        table[getHexValueFromFileLine(fileLine)] = getstringFromFileLine(fileLine);
                    //else throw new Exception("Duplicate dictionary words.");
                    else throw new Exception("Duplicate key.");
            return table;
        }

        public static List<string> tableFileList
        {
            get
            {
                SortedDictionary<int, string> table = getTableFileDictionary();
                List<string> retMe = new List<string>();
                foreach (int i in table.Keys)
                    retMe.Add(table[i]);
                return retMe;
            }
        }
    }
}