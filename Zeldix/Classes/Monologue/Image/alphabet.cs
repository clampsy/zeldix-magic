﻿using System.Collections.Generic;
namespace Zeldix.Classes.Monologue.Image
{
    /// <summary>
    /// Returns the tile IDs of characters for tables used in the game.
    /// </summary>
    public static class alphabet
    {
        /// <summary>
        /// Specifies for the get methods what ids to generate.
        /// </summary>
        /// <example>
        /// First field: The starting location
        /// Second field: how long to read
        /// 
        /// { 0x64, 0xA },
        /// 
        /// Read 10 values counting up from $64
        /// So:
        /// $64, $65, $64, ... , $6D, $6E
        /// </example>

        public static byte[,] alphabetForCredits = new byte[,]
        {   //value //length
            { 0x64, 0xA },
            { 0x00, 0x10 },
            { 0x20, 0xA },
            { 0xA1, 0x1 },
            { 0x6E, 0x1 }
        };

        /// <summary>
        /// Specifies for the get methods what ids to generate.
        /// </summary>
        public static byte[,] alphabetForDialogue = new byte[,]
        {   //value //length
            { 0x00, 0x10 },
            { 0x20, 0x10 },
            { 0x40, 0x10 },
            { 0x60, 0x10 },
            { 0x80, 0x10 },
            { 0xA0, 0x10 }/*,*/
            /*{ 0xC0, 0x02 }*/
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static List<byte> transcribe(byte[,] byteArray)
        {
            List<byte> alphabet = new List<byte>();
            for (int i = 0; i < byteArray.Length / 2; i++)
                for (int j = 0; j < byteArray[i, 1]; j++)
                    alphabet.Add((byte)(byteArray[i, 0] + j));
            return alphabet;
        }

        /// <summary>
        /// The credits uses a different table for text
        /// </summary>
        /// <returns></returns>
        public static List<byte> getHexValueForCredits()
        { return transcribe(alphabetForCredits); }

        /// <summary>
        /// The monologues uses a different table for text
        /// </summary>
        /// <returns></returns>
        public static List<byte> getHexValueForDialogue()
        { return transcribe(alphabetForDialogue); }
    }
}
