﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Classes.Monologue.Image
{
    delegate Bitmap GetBMP(int i);
    delegate Bitmap Crop(Bitmap b, int width);
    /// <summary>
    /// Converts string text to a list of game characters
    /// </summary>
    public class myFont
    {
        private Bitmap[] fontCHR;
        private CharaDiction charaTable;

        /// <summary>
        /// Initializes a new instance of myFont
        /// </summary>
        /// <param name="rom"></param>
        public myFont()
        {
            Get_Pallette pall = new Get_Pallette();

            //copied from credits code. Shame on me.
            fontCHR = BPPPack.getGFXAsBMPList(BPPPack.unpackTiles(2, 0x70000, 256), pall.getPalette(2, 6, true));

            //bitmp.read(0x70000, (int)Math.Pow(bitmp.blocksPerRow, 2)

            SortedDictionary<int, string> table = TableFile.getTableFileDictionary();
            charaTable = new CharaDiction(Constants.BLOCK_DIM);
            int q = 0;

            //Reads the top and bottom part of the character.
            //0x10 makes you read a line lower in the CHR.

            int qq = 0;
            foreach (byte b in alphabet.getHexValueForDialogue())
                charaTable.add(table[q++], fontCHR[b], fontCHR[b + 0x10], qq++);
        }

        /// <summary>
        /// Convert text to a List of objects. 
        /// To put simply the list contains commands that another class
        /// uses to convert the list to images.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="rom"></param>
        /// <returns></returns>
        public List<object> ConvertToImage(string line)
        {
            line = line.Replace(Constants.NAME, "LINK");
            SortedDictionary<int, string> dict = TableFile.getTableFileDictionary();
            bool special, ignore, add;
            special = ignore = add = false;
            string word = "";

            List<object> dialogueLine = new List<object>();
            foreach (char chara in line)
            {
                if (special || ignore)
                {
                    word += chara;
                    if (dict.ContainsValue(word))
                    {
                        if (!word.Contains(Constants.ACTION1.ToString()) && !ignore)
                            dialogueLine.Add(charaTable.getCharacter(word));
                        else dialogueLine.Add(word);
                        ignore = special = add = false;
                        word = "";
                    }
                }
                else
                {
                    switch (chara)
                    {
                        case Constants.IGNORE1:
                            ignore = true;
                            break;
                        case Constants.SPECIAL1:
                        case Constants.ACTION1:
                            special = true;
                            break;
                        default:
                            if (charaTable.Contains(chara))
                                add = true;
                            break;
                    }
                    if (ignore || special)
                        word += chara;
                }
                if (add)
                {
                    dialogueLine.Add(charaTable.getCharacter(chara));
                    add = false;
                }
            }
            return dialogueLine;
        }

        /// <summary>
        /// Stores images for the characters.
        /// </summary>
        internal class CharaDiction
        {
            [DebuggerDisplay("Count = {Count}")]
            private SortedDictionary<string, Bitmap> table;
            private readonly int maxHeight;
            private byte[] characterWidths;
            private SortedDictionary<int, string> dict;
            public CharaDiction(int maxHeight)
            {
                table = new SortedDictionary<string, Bitmap>();
                dict = TableFile.getTableFileDictionary();
                this.maxHeight = maxHeight;

                //read VWF widths
                characterWidths = RomIO.read(0x74ADF, 0x62);
            }

            private Bitmap crop(Bitmap b, int hexKey)
            {
                int width = (hexKey < characterWidths.Length ? characterWidths[hexKey] : Constants.BLOCK_DIM);
                return b.Clone(new Rectangle(0, 0, width, b.Height), b.PixelFormat);
            }

            public int Count
            { get { return table.Count; } }
            public bool Contains(string s)
            { return table.ContainsKey(s); }
            public bool Contains(char c)
            { return Contains(c.ToString()); }
            public void add(string chara, Bitmap top, Bitmap bottom, int hexKey)
            { table.Add(chara, crop(BMPMerge.mergeBitmaps(top, bottom, top.Width), hexKey)); }
            public Bitmap getCharacter(string s)
            { return table[s]; }
            public Bitmap getCharacter(char c)
            { return getCharacter(c.ToString()); }
        }
    }
}