﻿/* 
 * Class        :   CharacterTable.cs
 * Author       :   clampsy
 * Description  :   
 */
using System;
using System.Collections.Generic;
using Zeldix.Classes.Crap;
using Zeldix.Classes.Monologue.Image;

namespace Zeldix.Classes.Monologue
{
    delegate void sortCharactertable();
    /// <summary>
    /// A class that stores and manages the dictionary for
    /// the monologues. It also makes interpretting
    /// and compressing faster.
    /// </summary>
    public class CharacterTable
    {
        private SortedDictionary<int, string> table;
        private const int characterTableSize = byte.MaxValue;
        private int maxLength;
        private List<string[]>[] sortTable;

        /// <summary>
        /// Initilializes a new instance of CharacterTable
        /// </summary>
        /// <param name="rom"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public CharacterTable(bool readROM)
        {
            if (readROM)
            {
                constructor(true);
            }
            else constructor(false);
        }

        /// <summary>
        /// Initilializes a new instance of CharacterTable
        /// </summary>

        /// <summary>
        /// Initilializes a new instance of CharacterTable
        /// </summary>
        /// <param name="rom"></param>
        /// <param name="withDTE"></param>
        private void constructor(bool withDTE = true)
        {
            maxLength = 1;
            sortCharactertable sortCharTable = delegate ()
            {
                sortTable = new List<string[]>[characterTableSize];
                string[] str;
                foreach (int i in table.Keys)
                {
                    str = new string[2] { MyMath.decToHex(i), table[i] };

                    if (sortTable[str[1][0]] == null)
                        sortTable[str[1][0]] = new List<string[]>();
                    //Put string into index. The index is
                    //the word's first letter.
                    //This speeds up the process of compression
                    maxLength = Math.Max(maxLength, str[1].Length);
                    sortTable[str[1][0]].Add(str);
                }
            };

            table = new SortedDictionary<int, string>();
            table = TableFile.getTableFileDictionary();
            if (withDTE)
                (new DTE()).insertDTEIntoTable(ref table);
            sortCharTable();
        }

        /// <summary>
        /// Returns a list of all entries that start with the
        /// parameter's character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns>List of character entries.</returns>
        /// <example>
        /// A parameter with value 'a' will return entries
        /// starting with 'a'
        /// 
        /// entries = obj.getDictionaryListForChar('a')
        /// </example>
        public List<string[]> getDictionaryListForChar(char character)
        { return sortTable[character]; }

        /// <summary>
        /// Get the key for a certain word in the dictionary.
        /// </summary>
        /// <param name="word"></param>
        /// <returns>
        /// Integer value of the word in the dictionary
        /// </returns>
        /// <exception cref="Exception"></exception>
        /// <exception cref="KeyNotFoundException"></exception>
        public int getValueOfChar(string word)
        {
            if (string.IsNullOrEmpty(word))
                throw new Exception("Empty strings not allowed");

            foreach (int i in table.Keys)
                    if (table[i].Equals(word))
                        return i;
            throw new KeyNotFoundException("Word not found in dictionary.");

        }

        /// <summary>
        /// Checks if the word is stored in the dictionary.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public bool contains(string word)
        {
            if (string.IsNullOrEmpty((word)))
                throw new ArgumentNullException();
            return table.ContainsValue(word);
        }

        /// <summary>
        /// Gets the size of the biggest entry in a
        /// sorted dictionary entry.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public int max(List<string[]> list)
        {
            int max = 0;
            foreach (string[] s in list)
                max = Math.Max(s[1].Length, max);
            return max;
        }

        //Stole this from FCandChill because he/she stole from me!
        /// <summary>
        /// Converts a list of bytes to text like it would appear
        /// in-game as a string
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public string toChara(List<byte> b)
        {
            Queue<byte> q = new Queue<byte>();
            foreach (byte by in b)
                q.Enqueue(by);
            byte temp;
            string s = "";
            int aa = -1;
            while (q.Count != 0)
            {
                temp = q.Dequeue();
                if (q.Count != 0)
                {
                    aa = Conversion.toInt(temp, q.Peek());
                    if (table.ContainsKey(aa) && temp != 00)
                    {
                        s += toChara(aa);
                        q.Dequeue();
                    }
                    else s += toChara(temp);
                }
                else s += toChara(temp);
            }
            return s;
        }

        /// <summary>
        /// Converts a single integer value to a dictionary value
        /// </summary>
        /// <param name="b">Integer value of dictionary text</param>
        /// <returns>Either the word or the hex value in brackets
        /// if it's not in thedictionary.</returns>
        public string toChara(int b)
        { return ((table.ContainsKey(b)) ? table[b] : sandwich(b)); }

        /// <summary>
        /// Converts a byte array to a string.
        /// Serves as a way to read the DTE table for the game.
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        public string toChara(byte[] binary)
        {
            string output = "";
            foreach (byte b in binary)
                output += ((table[b] != null) ? table[b] : sandwich(b));
            return output;
        }

        /// <summary>
        /// Adds brackets to a number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string sandwich(int number)
        { return "{" + MyMath.decToHex(number) + "}"; }

        /// <summary>
        /// Adds brackets to a number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string sandwich(byte number)
        { return sandwich((int)number); }
    }
}