﻿/*
 * Class        :   SearchBoxTools.cs
 * Author       :   Superskuj
 * Description  :   
 */
using System.Collections.Generic;
namespace Zeldix.Classes.Enemies
{
    public static class SearchBoxTools
    {
        public static List<string> ListBoxRefinement(List<string> originalList, string searchString)
        {
            List<string> refinedList = new List<string>();
            for (int i = 0; i < originalList.Count; i++)
                if (originalList[i].ToLower().Contains(searchString))
                    refinedList.Add(originalList[i]);
            return refinedList;
        }
    }
}