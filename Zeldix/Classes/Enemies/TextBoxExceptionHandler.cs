﻿/*
 * Class        :   TextBoxExceptionHandler.cs
 * Author       :   Superskuj
 * Description  :   
 */
namespace Zeldix.Classes.Enemies
{
    public class TextBoxExceptionHandler
    {
        public static int? CheckNumericRange(string s, int max)
        {
            int s2;
            int.TryParse(s, out s2);

            if (s2 > max)
                s2 = max;
            return s2;
        }
    }
}
