﻿using System.Windows.Forms;
namespace Zeldix.Classes.Crap
{
    class ScrollManager
    {
        private int myCursor, max, width, height, wordSize;
        public ScrollManager(int width, int height, int max, int wordSize)
        {
            this.max = max;
            this.width = width;
            this.height = height;
            this.wordSize = wordSize;
        }

        public void setWordSize(int wordSize)
        { this.wordSize = wordSize; }

        public void upup(int amount)
        {
            if (myCursor - amount >= 0)
                myCursor -= amount;
            else
                myCursor = 0;
        }

        public void downdown(int amount)
        {
            if (myCursor < maxToScroll - amount)
                myCursor += amount;
            else
                myCursor = maxToScroll;
        }

        public int cursor
        { get { return myCursor; } set { myCursor = value; } }

        public int tilesize
        { get { return wordSize; } }
        public int maxToScroll
        { get { return max - pageSize - 1; } }
        public int lineSize
        { get { return width * tilesize; } }
        public int getWidth
        { get { return width; } }


        public int pageSize
        { get { return lineSize * width; } }

        public void updateScrollbar(ref VScrollBar vScrollBar)
        {
            int number = maxToScroll / lineSize;
            if (vScrollBar.Maximum != number)
                vScrollBar.Maximum = number;
            vScrollBar.Value = myCursor / lineSize;
        }
    }
}