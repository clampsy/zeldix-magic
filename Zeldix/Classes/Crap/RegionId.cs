﻿using System;
using System.Linq;
namespace Zeldix.Classes.Crap
{
    public static class RegionId
    {
        public static int myRegion;

        public enum region
        {
            Invalid = -1,
            Japan,
            USA,
            German,
            France,
            Europe,
            Canada

        }
        private static byte[] bytes = new byte[]{ 0xF3, 0x68, 0xC8, 0x28, 0x7F, 0x38 }; //F368C8287F38

        private static int[] location = new int[]
        {
            0x77D30,
            0x75383,
            0x6678B,
            0x6676B,
            0x753A6,
            0x670FB
        };

        public static void generateRegion()
        {
            myRegion = (int)region.Invalid;
            for(int i = 0; i < bytes.Length; i++)
            {
                byte[] b = RomIO.read(location[i], bytes.Length);
                if (b.SequenceEqual(bytes))
                {
                    myRegion = i;
                    break;
                }
            }

            if (myRegion == (int)region.Invalid)
                throw new Exception();
        }
    }
}