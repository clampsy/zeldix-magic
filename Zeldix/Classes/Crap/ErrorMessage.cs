﻿/*
 * Class        :   ErrorMessage.cs
 * Author       :   clampsy
 * Description  :   
 */
using System;
using System.Diagnostics;
using System.Windows;

namespace Zeldix.Classes.Crap
{
    public static class ErrorMessage
    {
        public static void Show(string text, bool terminateProgram = false)
        {
            MessageBox.Show(text, terminateProgram ? "Fatal Error" : "Error");
            if (terminateProgram)
                Process.GetCurrentProcess().Kill();
        }


        public static void Show(Exception ee, bool terminateProgram = false)
        { Show(ee.ToString(), terminateProgram); }
    }
}
