﻿namespace Zeldix.Classes.Crap
{
    public static class ReadMixedNumbers
    {
        public static ushort readTwoByte(int address)
        {
           byte[] b = RomIO.read(address, 2);
           return Conversion.toUShort(b[1], b[0]);
        }
    }
}
