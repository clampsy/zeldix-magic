﻿namespace Zeldix.Classes.Crap
{
    public static class BitwiseMath
    {
        public static bool isFlagTrue(int data, int mask)
        { return ((data & mask) == mask); }
        public static int getMaskForBit(int bitNo)
        { return 0x1 << bitNo; }
    }
}