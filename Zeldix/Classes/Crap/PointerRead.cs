﻿using System;

namespace Zeldix.Classes.Crap
{
    public static class PointerRead
    {
        public static int LongReadLoHiBank(int address)
        { return LongReadLoHiBank(address, address + 1, address + 2); }

        public static int LongReadLoHiBank(int loAddress, int hiAddress, int bankAddress)
        {
            return AddressLoROM.snesToPc(
                RomIO.read(loAddress),
                RomIO.read(hiAddress),
                RomIO.read(bankAddress));
        }

        public static int ShortReadLoHi(int address)
        {
            return AddressLoROM.snesToPc
                (
                RomIO.read(address),
                RomIO.read(address + 1),
                AddressLoROM.pcToSnes_Bank(address)
                );
        }

        public const int maxAddressFor3BytePointer = 0xFFFFFF;

        public static void checkAddressWithinRangeOf3Byte(int address)
        {
            if (!(address < PointerRead.maxAddressFor3BytePointer))
                throw new ArgumentOutOfRangeException();
        }

        public static byte[] generatePointer3(int address)
        {
            return new byte[]
            {
                (byte)AddressLoROM.pcToSnes_Lo(address),
                (byte)AddressLoROM.pcToSnes_Hi(address),
                (byte)AddressLoROM.pcToSnes_Bank(address)
            };
        }

        public static byte[] generatePointer2(int address)
        {
            return new byte[]
            {
                (byte)AddressLoROM.pcToSnes_Lo(address),
                (byte)AddressLoROM.pcToSnes_Hi(address),
            };
        }
    }
}
