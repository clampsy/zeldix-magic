﻿/*   o════════════════════════════════════════════════════════o
 *   ║                                                        ║
 *   ║                   ~ Overworld Crap ~                   ║
 *   ║                 ¯ ¯ ¯¯¯¯¯¯¯¯¯ ¯¯¯¯ ¯ ¯                 ║
 *   º════════════════════════════════════════════════════════º
 *   
 *     - ─ ──────────────────────────────── ─ -
 *         Map32 to Map16 Conversion Values
 *     - ─ ──────────────────────────────── ─ -
 *       ● 0x18000 to 0x1B3FE (33FE) = Upper left  corners //end with $FF at 0x1B3FF
 *       ● 0x1B400 to 0x1E7FE (33FE) = Upper right corners //end with $FF at 0x1E7FF
 *       ● 0x20000 to 0x233FE (33FE) = Lower left  corners //end with $FF at 0x233FF
 *       ● 0x23400 to 0x267EE (33FE) = Lower right corners //end with $FF at 0x267EF
 *  
 *        Theses value come in pack of 4 bytes, followed by 2 bytes.
 *        The 2 bytes are 4 nibble value that are the extension of the 4 first
 *  ╔══════════════════Example══════════════════════╗
 *  ║  You have theses bytes: 05 06 07 08 30 1A     ║
 *  ║                         ¯¯ ¯¯ ¯¯ ¯¯ ## ##     ║
 *  ║       05 become $305                          ║
 *  ║       06 stays  $06                           ║
 *  ║       07 become $107                          ║
 *  ║       08 become $A08                          ║
 *  ╚═══════════════════════════════════════════════╝
 *
 *      - ─ ─────────────────────────────── ─ -
 *          Map8 to Map16 Conversion Values
 *      - ─ ─────────────────────────────── ─ -
 *       ● 0x78000 to 0x????? (470?) = Map8 to Map16 conversion values (8 bytes per tile16),
 *           2 byte each tile8
 *     
 *                 vhopppcc     cccccccc
 *                 
 *          ○ v = mirror vertical
 *          ○ h = mirror horizontal
 *          ○ p = priority tile
 *          ○ c = tile index
 *
 *      - ─ ──────── ─ -
 *          Map Size 
 *      - ─ ──────── ─ -
 *       ● 0x12844 to 0x12883 = 1byte each map
 *          ○ $00 =  512x512  pixel map
 *          ○ $20 = 1024x1024 pixel map
 *
 *      - ─ ────────────────────────── ─ -
 *          Compressed tile arragement
 *      - ─ ────────────────────────── ─ -
 *       ● 0x1794D = LONGPointers all tiles of maps [High] (mapid * 3)
 *       ● 0x17B2D = LONGPointers all tiles of maps [Low]  (mapid * 3)
 * 
 * ╔═══════════════════════════════════════════════════════════════════════════════╗
 * ║ Note: Data leading to is compressed using the LZ2 compression                 ║
 * ║ 0x05D97 ═ tiles gfx to load the map (correspond to GFX# in HM)                ║
 * ║ surprisingly overworld only use 32 to 47 LW, 48 to 63 DW, 4 bytes per gfx     ║
 * ╚═══════════════════════════════════════════════════════════════════════════════╝
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;
namespace Zeldix.Classes.Overworld
{
    public class MapList
    {
        delegate int Thing(int address, int i);

        /// <summary>
        /// Returns number of maps the game has.
        /// </summary>

        public byte currentMapNo
        { get; set; }

        private Overworld32Arrangment arrag;
        private Map32 map32;
        private Map16 map16;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rom"></param>
        public MapList()
        {
            arrag = new Overworld32Arrangment();
            map32 = new Map32();
            map16 = new Map16();
        }

        public byte getMapGraphicsIndex()
        { return arrag.maps[currentMapNo].mapGraphicsIndex; }

        public int getNumberOfMaps()
        { return Overworld32Arrangment.numberOfMaps; }


        public Bitmap drawMap()
        {
            int[] 
                tilesArrange = arrag.get32by32Tiles(currentMapNo),
                blocksets, sprgfx, sprpal;

            map32.dsfsdf(currentMapNo, getMapGraphicsIndex(), out blocksets, out sprgfx, out sprpal);

            DecompressBuffer a = new DecompressBuffer();
            Bitmap b = null;

            for(int i = 0; i < blocksets.Length; i++)
                b = BMPMerge.mergeBitmaps(b, a.getBMP(blocksets[i]));
            return b;
        }

        /// <summary>
        /// Stores data about 32x32 tiles which are compressed.
        /// </summary>
        internal class Overworld32Arrangment
        {
            public OverworldMaps[] maps;

            private const int
                sizeStart = 0x12844,
                mapTile_HiPointer_address = 0x1794D,
                mapTile_LoPointer_address = 0x17B2D,
                mapGraphicsIndex_address = 0x07C9C,
                mapGraphicsNumber = 0x80;
            public const int numberOfMaps = 63;

            public struct OverworldMaps
            {
                internal int size { get; set; }
                internal int tileHi { get; set; }
                internal int tileLo { get; set; }
                internal byte mapGraphicsIndex { set; get; }
            }

            public Overworld32Arrangment()
            {
                maps = new OverworldMaps[numberOfMaps];

                Thing thing = delegate (int address, int i)
                { return PointerRead.LongReadLoHiBank(address + i * 3); };

                for (int i = 0; i < numberOfMaps; i++)
                {
                    maps[i].tileHi = thing(mapTile_HiPointer_address, i);
                    maps[i].tileLo = thing(mapTile_LoPointer_address, i);

                    if (maps[i].tileHi < 0 && maps[i].tileLo < 0)
                        throw new ArgumentOutOfRangeException();

                    switch (RomIO.read(sizeStart + i))
                    {
                        case 0x00: maps[i].size = mapSize * 1; break;
                        case 0x20: maps[i].size = mapSize * 2; break;
                        default: throw new Exception("Invalid parameter for size.");
                    }
                    maps[i].mapGraphicsIndex = RomIO.read(mapGraphicsIndex_address + i);
                }
            }

            private const int mapSize = 512;

            public int[] get32by32Tiles(int index)
            {
                List<byte>
                    bytes1 = Decomp_final.Decompress(maps[index].tileHi),
                    bytes2 = Decomp_final.Decompress(maps[index].tileLo);

                Conversion.makeCountEqual(ref bytes1, ref bytes2);
                byte[]
                    bytes1A = bytes1.ToArray(),
                    bytes2A = bytes2.ToArray();

                int[] listOfTile = new int[bytes1.Count];
                for (int ttpos = 0; ttpos < bytes1.Count; ttpos++)
                    listOfTile[ttpos] = Conversion.toInt(bytes1A[ttpos], bytes2A[ttpos]);
                return listOfTile;
            }

        }
        internal class Map32
        {
            delegate short bitShift(byte i, byte shorty);
            delegate short add(byte i, short shorty);
            delegate short[] getTiles2(int address, int index);
            delegate void arrayManage(ref short[] array, short[] rawData, int cursor);
            //   - ─ ──────────────────────────────── ─ -
            //       Map32 to Map16 Conversion Values
            //   - ─ ──────────────────────────────── ─ -
            //               ┌────┬────┐
            //               | 16 | 16 |
            //               ├────┼────┤
            //               | 16 | 16 |
            //               └────┴────┘

            #region Addresses for tiles
            private const int
                upper_left = 0x18000,
                upper_right = 0x1B400,
                lower_left = 0x20000,
                lower_right = 0x23400,
                delimeter = 0xFF; //the real game doesn't care.
            private readonly int[] addressConst = new int[]
            {
                upper_left,
                upper_right,
                lower_left,
                lower_right
            };
            #endregion
            #region Sizes
            private const int
                sizeOfCornerData = 0x33FE,
                numberOf32Tiles = 0x866,
                sizeOfByteEntries = 6,
                noOfUncompressedEntries = 4;
            #endregion


            private tile32[] tile;  //all the 32x32 tiles for every map
            private struct tile32
            {
                //each of the 4 is called an orientation in thecpomm
                //internal short[] tile;
                internal short
                    upper_left_16,    //12-bits each
                    upper_right_16,
                    lower_left_16,
                    lower_right_16;
            }
            /*  The last two bytes are a four 4-bit nibble value that
             *  are the extension of the first four bytes
             *
             *  ╔══════════════════Example══════════════════════╗
             *  ║  You have theses bytes: 05 06 07 08 30 1A     ║
             *  ║                         ¯¯ ¯¯ ¯¯ ¯¯ ## ##     ║
             *  ║     05 (00000101) becomes $305 (001100000101) ║
             *  ║     06 stays  $06                             ║
             *  ║     07 becomes $107                           ║
             *  ║     08 becomes $A08                           ║
             *  ╚═══════════════════════════════════════════════╝ */

            private const int NoOf16tilesPer32 = 4;
            public Map32()
            {
                getTiles2 gettile = delegate (int address, int index)
                { return getTiles(RomIO.read(address + index * sizeOfByteEntries, sizeOfByteEntries)); };
                arrayManage manage = delegate (ref short[] array, short[] rawData, int cursor)
                {
                    for (int i = 0; i < rawData.Length; i++)
                        array[cursor + i] = rawData[i];
                };
                tile = new tile32[numberOf32Tiles];

                short[]
                    up_left = new short[numberOf32Tiles],
                    up_right = new short[numberOf32Tiles],
                    low_left = new short[numberOf32Tiles],
                    low_right = new short[numberOf32Tiles];

                for (int i = 0; i < numberOf32Tiles - noOfUncompressedEntries; i += noOfUncompressedEntries)
                {
                    manage(ref up_left, gettile(upper_left, i), i);
                    manage(ref up_right, gettile(upper_right, i), i);
                    manage(ref low_left, gettile(lower_left, i), i);
                    manage(ref low_right, gettile(lower_right, i), i);
                }

                for (int i = 0; i < numberOf32Tiles; i++)
                {
                    tile[i].upper_left_16 = up_left[i];
                    tile[i].upper_right_16 = up_right[i];
                    tile[i].lower_left_16 = low_left[i];
                    tile[i].lower_right_16 = low_right[i];
                }
            }

            private short[] getTiles(byte[] b)
            {
                const int
                    nibbleSize = 4,
                    mask = 0x0F,
                    byteLength = 8;

                add addNibble = delegate (byte val, short nibbleVal)
                { return (short)(val + (nibbleVal << byteLength)); };

                bitShift getFirst4Bits = delegate (byte vv, byte nibbleByte)
                { return addNibble(vv, (byte)(nibbleByte >> nibbleSize)); };

                bitShift getLast4Bits = delegate (byte vv, byte nibbleByte)
                { return addNibble(vv, (byte)(nibbleByte & mask)); };

                return new short[]
                {
                    getFirst4Bits(  b[0],   b[4]),
                    getLast4Bits(   b[1],   b[4]),
                    getFirst4Bits(  b[2],   b[5]),
                    getLast4Bits(   b[3],   b[5])
                };
            }


            public void dsfsdf(int areaNo, int gfxNo, out int[] blocksets, out int[] sprgfx, out int[] sprpal)
            {
                //line 20412 Hyrule Magic

                int i, // 
                    j, // the overworld area number to load.
                    k, // The offset for the 8 byte array of blockset information, for a particular area.
                    l, // and auxiliary offset for additional blocktypes that are swapped in sometimes.
                    m = gfxNo; // use as the graphics number.
                j = areaNo;

                blocksets = new int[16];

                int temp = -1;
                if (j == 0x88 || j == 0x93)
                    temp = 0x24;
                else temp = Convert.ToBoolean(j & 0x40) ? 0x21 : 0x20;

                k = 0x6073 + (temp << 3);

                if (j == 0x95)
                    m = 0x22;
                else if (j == 0x96)
                    m = 0x3B;
                else if (j == 0x88 || j == 0x93)
                    m = 0x51;
                else if (j == 0x9E)
                    m = 0x21;
                else if (j == 0x9C)
                    m = 0x41;
                else if (j > 0x7F)
                    m = 0x2F;

                l = 0x5d97 + (m << 2);

                //Write down the first eight blocksets this area will use
                for (i = 0; i < 8; i++)
                    blocksets[i] = RomIO.read(k++);

                // rewrite some of these blockset entries with the ones located at $5D97 + (4*gfxnumber)

                for (i = 3; i < 7; i++)
                    if ((m = RomIO.read(l++)) != 0)
                        blocksets[i] = m;

                // Tells us which area it is, without regard to light / dark world.
                m = j & 0x3f;


                // In certain cases, the 8th blockset is 0x58 or 0x5A
                if ((m >= 3 && m <= 7) || j == 149)
                {
                    blocksets[8] = 0x58;
                    l = 2;
                }
                else
                {
                    blocksets[8] = 0x5A;
                    l = 0;
                }

                // more blockset crap... blah blah.
                blocksets[9] = blocksets[8] + 1;
                blocksets[10] = Convert.ToBoolean(j & 0x40) ? 126 : 116;

                sprgfx = new int[8];
                sprpal = new int[8];


                // If we be in da dark world...
                if (j >= 0x40)
                {
                    sprgfx[2] = sprgfx[1] = sprgfx[0] = RomIO.read(0x7AC1 + j);
                    sprpal[2] = sprpal[1] = sprpal[0] = RomIO.read(0x7BC1 + j);
                }
                else
                {
                    sprgfx[0] = RomIO.read(0x7A41 + j);
                    sprgfx[1] = RomIO.read(0x7A81 + j);
                    sprgfx[2] = RomIO.read(0x7AC1 + j);

                    sprpal[0] = RomIO.read(0x7B41 + j);
                    sprpal[1] = RomIO.read(0x7B81 + j);
                    sprpal[2] = RomIO.read(0x7BC1 + j);
                }

                for (i = 0; i < 4; i++)
                    blocksets[i + 11] = RomIO.read(k + i) + 0x73;
            }
        }
        /*  ┌───┬───┐
         *  | 8 | 8 |
         *  ├───┼───┤
         *  | 8 | 8 |
         *  └───┴───┘
         */
        internal class Map16
        {
            //$3FF 8x8 blocks in all and $FF 16x16 blocks

            /*      - ─ ─────────────────────────────── ─ -
             *          Map8 to Map16 Conversion Values
             *      - ─ ─────────────────────────────── ─ -
             *     
             * ● 0x78000 to 0x????? (470?) = Map8 to Map16 conversion values
             *     (8 bytes per tile16)
             *     2 byte each tile8
             *
             * vhopppcc, cccccccc
             *                 
             *   v = mirror vertical
             *   h = mirror horizontal
             *   p = priority tile
             *   c = tile index
             *   o = ???
             */

            private const int
                startAddress = 0x78000,
                wordSize = 0x8,
                size = 470;
            private Map8Tile[] array;

            private struct Map8Tile
            {
                internal bool
                    mirror_vertical,
                    mirror_horizontal,
                    ooo;
                internal int priority_tile;
                internal short tile_index;
            }

            public Map16()
            {
                array = new Map8Tile[wordSize];
                for (int y = 0, cursor = startAddress; y < 470; y++)
                    for (int x = 0; x < wordSize; x++, cursor += 2)
                    {
                        short tile = (short)Conversion.toInt(RomIO.read(cursor, 2));
                        array[x].mirror_vertical = BitwiseMath.isFlagTrue(tile, 0x8000);
                        array[x].mirror_horizontal = BitwiseMath.isFlagTrue(tile, 0x4000);
                        array[x].ooo = BitwiseMath.isFlagTrue(tile, 0x2000);
                        array[x].priority_tile = (byte)((tile >> 10) & 0x07);
                        array[x].tile_index = (short)(tile & 0x3FF);
                    }
            }
        }
    }
}