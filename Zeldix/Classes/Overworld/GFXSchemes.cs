﻿/* Copyright 2017 clampsy
 * This file is part of the Zeldix Magic project.
 * Zeldix Magic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Zeldix Magic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zeldix Magic.  If not, see <http://www.gnu.org/licenses/>
 */

/*
 * The game load image sets in bulk by ID.
 * This file:
 * * returns ids of GFX sets
 * * Allows modifaction of GFX sets
 * 
 * Developer rant:
 * 
 * clampsy: Here's my code. Open source 'n crap
 * Euclid: OMG this source code is unstructured.
 *         I've been in the industry for over 10 years. 
 *         Good luck getting anywhere.
 * clampsy: Can you point to where and tell me how to improve?
 * Euclid: [Silence]
 * clampsy: t('-'t)
 */

using System;
using Zeldix.Classes.BPP;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.Overworld
{
    public static class GFXSchemes
    {
        public enum BlockSetType
        {
            MainBlockSet,
            RoomBlockSet,
            SpriteBlockSet
        }

        //the public status is intentional
        public static readonly BlockSetTemplate[] blocksets = new BlockSetTemplate[]
            {
                new BlockSetTemplate(0x25, 0x08, 0x6073),   //Main BlockSet
                new BlockSetTemplate(0x52, 0x04, 0x5D97),   //Room BlockSet
                new BlockSetTemplate(0x90, 0x04, 0x5B57)    //Sprite BlockSet
                //palette     max: 0x47    entry:  0x4
            };
        public static readonly DecompressBuffer gfxBuffer = new DecompressBuffer();

        public static int numberOfBlocksetTypes
        { get; private set; }

        public const int maxValOfEntries = 0xDB;

        public static void writeToArray()
        {
            foreach (BlockSetTemplate b in blocksets)
                b.writeToArray();
        }

        /// <summary>
        /// A class that's used to write and read
        /// blocksets.
        /// 
        /// The structuring is the same for each blockset, 
        /// so this class exists to eliminate redudant code.
        /// </summary>
        public class BlockSetTemplate
        {
            public readonly int
                numberOfBlockSets,
                entrySize,
                dataStart;

            public readonly DecompressBuffer gfxBuffer;

            public byte[][] blockSet;
            public BlockSetTemplate(int numberOfBlockSets, int entrySize, int dataStart)
            {
                this.numberOfBlockSets = numberOfBlockSets;
                this.entrySize = entrySize;
                this.dataStart = dataStart;

                blockSet = getEntryBytes();
                GFXSchemes.numberOfBlocksetTypes++;

                gfxBuffer = new DecompressBuffer();
            }
            private byte[][] getEntryBytes()
            {
                byte[][] values = new byte[numberOfBlockSets][];
                for (int i = 0; i < numberOfBlockSets; i++)
                    values[i] = RomIO.read(dataStart + i * entrySize, entrySize);
                return values;
            }

            public void writeToArray()
            {
                for (int i = 0; i < numberOfBlockSets; i++)
                    RomIO.writeToArray(dataStart + i * entrySize, entrySize, blockSet[i]);
            }

            public void setScheme(int blockSetID, int index, byte value)
            { blockSet[blockSetID][index] = value; }

            private byte[] getScheme(int blockSetID)
            {
                if (blockSetID < blockSet.Length)
                    return blockSet[blockSetID];
                else throw new IndexOutOfRangeException();
            }

            public byte getScheme(int blockSetID, int index)
            { return getScheme(blockSetID)[index]; }

            public byte[][] getVRAM()
            {
                byte[][] rawData = new byte[numberOfBlockSets][];
                for (int i = 0, j = 0; i < rawData.Length; i++)
                    foreach (byte b in blockSet[i])
                        foreach (byte[] c in gfxBuffer.getBytes(b))
                            rawData[j++] = c;
                return rawData;
            }
        }
    }
}