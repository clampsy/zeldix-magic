﻿/*
 * Class        :   BPPRender.cs
 * Author       :   clampsy
 * Description  :   Renders images from byte array
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using Zeldix.Classes.Crap;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Classes.BPP
{
    public static class BPPRender
    {
        private static int
            height, //4 bytes
            width;  //4 bytes
        private static Color[] palette;
        private static byte[][] image1D;
        private static int blocksPerRow;

        //https://stackoverflow.com/questions/6782489/create-bitmap-from-a-byte-array-of-pixel-data
        public static Bitmap getBitmap(int blocksPerRow, Color[] palette, byte[][] image1D)
        {
            if (palette == null)
            {
                palette = new Color[] //default palette to prevent crashing 'n stuff
                {
                    Color.FromArgb(0xA5, 0xA5, 0xA5),
                    Color.FromArgb(0x00, 0x42, 0xC6),
                    Color.FromArgb(0x42, 0x29, 0xCE),
                    Color.FromArgb(0x6B, 0x00, 0xBD),

                    Color.FromArgb(0x94, 0x29, 0x94),
                    Color.FromArgb(0x9C, 0x10, 0x42),
                    Color.FromArgb(0x9C, 0x39, 0x00),
                    Color.FromArgb(0x84, 0x5E, 0x21),

                    Color.FromArgb(0x5F, 0x7B, 0x21),
                    Color.FromArgb(0x2D, 0x8C, 0x29),
                    Color.FromArgb(0x18, 0x8E, 0x10),
                    Color.FromArgb(0x2E, 0x86, 0x63),

                    Color.FromArgb(0x29, 0x73, 0x9C),
                    Color.FromArgb(0x00, 0x00, 0x00),
                    Color.FromArgb(0x00, 0x00, 0x00),
                    Color.FromArgb(0x00, 0x00, 0x00),
                };
            }

            BPPRender.palette = palette;
            BPPRender.blocksPerRow = blocksPerRow;
            BPPRender.image1D = image1D;
            if (blocksPerRow < image1D.Length)
                width = (blocksPerRow * Constants.BLOCK_DIM);
            else width = image1D.Length * Constants.BLOCK_DIM;
            height = (image1D.Length / blocksPerRow) * Constants.BLOCK_DIM;

            if (height == 0)
                height = Constants.BLOCK_DIM;

            return byteToBitmap(Conversion.ToByteArray(convert2DListTo1DList2(image1D)), width, height, palette);
        }

        public static Bitmap getBitmap(int blocksPerRow, Color[] palette, byte[] image1D)
        { return getBitmap(blocksPerRow, palette, new byte[][] { image1D }); }


        private static List<byte> convert2DListTo1DList2(byte[][] image)
        {

            List<byte> returnMe = new List<byte>();
            int listIndex = 0;

            //Transfer the bytes into sequential order that a bitmap is stored in
            //instead of storing bytes as individual tiles.

            int zz = Constants.BLOCK_DIM * Constants.BLOCK_DIM * image.Length;

            while ((listIndex < image.Length))
                for (int tileRow = 0; tileRow < Constants.BLOCK_DIM; tileRow++, listIndex += blocksPerRow)
                    for (int y = 0; y < Constants.BLOCK_DIM; y++)
                        for (int tile = 0; tile < blocksPerRow; tile++)
                            for (int x = 0; x < Constants.BLOCK_DIM && (tile + listIndex < image.Length); x++)
                                returnMe.Add(image[tile + listIndex][y * Constants.BLOCK_DIM + x]);
            return returnMe;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pixelData"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="palette"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static Bitmap byteToBitmap(byte[] pixelData, int width, int height, Color[] palette)
        {
            if (height == 0)
                throw new ArgumentOutOfRangeException("Height can't be 0.");
            Bitmap b = new Bitmap(width, height, Constants.stanard_px_format);
            Palette.paletteSet(ref b, palette);
            Rectangle BoundsRect = new Rectangle(0, 0, width, height);
            BitmapData bmpData = b.LockBits(
                BoundsRect,
                ImageLockMode.WriteOnly,
                b.PixelFormat);
            Marshal.Copy(pixelData, 0, bmpData.Scan0, pixelData.Length);
            b.UnlockBits(bmpData);
            return b;
        }
    }
}