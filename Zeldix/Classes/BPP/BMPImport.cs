﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.BPP
{
    public static class BMPImport
    {
        public static void importBitmap(string filePath, int address, int height, int width, int BPP)
        {
            Bitmap bmp = BitmapHandler.LoadBitmap(File.ReadAllBytes(filePath));

            if (bmp.Height == height && bmp.Width == width)
            {
                if (bmp.PixelFormat == Constants.stanard_px_format)
                {
                    LockBitmap b = new LockBitmap(bmp);
                    b.LockBits();
                    byte[] pix = b.Pixels;
                    b.UnlockBits();
                    byte[][] tiles = new byte[(height / Constants.BLOCK_DIM) * (width / Constants.BLOCK_DIM)][];

                    for (int i = 0; i < pix.Length; i++)
                    {
                        int bitmap_coord_x = i % height,
                            bitmap_coord_y = i / height,

                            tileCoord_x = bitmap_coord_x % Constants.BLOCK_DIM,
                            tileCoord_y = bitmap_coord_y % Constants.BLOCK_DIM,

                            tileNo = ((i % height) / Constants.BLOCK_DIM) + ((i / width) / Constants.BLOCK_DIM * (width / Constants.BLOCK_DIM));

                        byte[] temp = new byte[] { pix[i] };

                        tiles[tileNo] = (tiles[tileNo] == null ? temp : tiles[tileNo].Concat(temp).ToArray());

                        if (i % height == 0)
                            continue;
                    }

                    int offset = 0;

                    for (int i = 0; i < tiles.Length; i++)
                    {
                        if (tiles[i] == null)
                            throw new Exception();

                        byte[] v = BPPPack.pack_bpp_tile(tiles[i], BPP);
                        RomIO.writeToArray(address + offset, v.Length, v);
                        offset += v.Length;
                    }
                }
                else new Exception("The pixel format must be 8bpp Indexed");
            }
            else throw new Exception("The canvas must be " + height + " x " + width);
        }
    }
}
