﻿/*
 * Copyright 2016 Sylvain "Skarsnik" Colinet
 * This file is part of the SkarAlttp project.
 * SkarAlttp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * SkarAlttp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with SkarAlttp.  If not, see <http://www.gnu.org/licenses/>
 */
/*
 * Class        :   BMPPack.cs
 * Author       :   Skarsnik, clampsy
 * Description  :  This class does the following:
 *                  a) Converts ROM GFX data to data the can be converted to Bitmap
 *                  b) Packs bitmap data into ROM GFX data
 */

using System;
using System.Drawing;
using Zeldix.Classes.Crap;
namespace Zeldix.Classes.BPP
{
    public static class BPPPack
    {
        public static readonly int[] bytesPerBlock = new int[]
        { -1, 8, 16, 24, 32 };

        public static readonly int[] paletteNumber = new int[]
        { -1, 2, 4, 8, 16 };

        public static int getBytesPerBlock(int bpp)
        {
            if (bpp > 0 && bpp < bytesPerBlock.Length)
                return bytesPerBlock[bpp];
            else throw new ArgumentOutOfRangeException();
        }

        public static int getLargestPaletteEntry()
        { return paletteNumber[paletteNumber.Length - 1]; }

        public static Bitmap[] getGFXAsBMPList(byte[][] b, Color[] c)
        {
            Bitmap[] bitmaps = new Bitmap[b.Length];
            int i = 0;
            foreach (byte[] a in b)
                bitmaps[i++] = BPPRender.getBitmap(1, c, a);
            return bitmaps;
        }


        public static int getNumOfPal(int bpp)
        { return paletteNumber[bpp]; }

        public static byte[][] unpackTiles(int bpp, int pos = 0x0, int tileNumber = 2)
        {
            byte[][] tiles = new byte[tileNumber][];
            if (bpp == 1)
                for (int i = 0; i < tileNumber; i++)
                    tiles[i] = RomIO.readInBinary(pos + i * bytesPerBlock[bpp], bytesPerBlock[bpp]);
            else
                for (int i = 0; i < tileNumber; i++)
                    tiles[i] = unpack_bpp_tile(RomIO.read((pos + i * bytesPerBlock[bpp]), bytesPerBlock[bpp]), 0, bpp);
            return tiles;
        }

        public static byte[][] unpackTiles(int bpp, byte[] data)
        {
            byte[][] tiles = new byte[data.Length / bytesPerBlock[bpp]][];
            if (bpp == 1)
                throw new Exception();
            else
                for (int i = 0; i < tiles.Length; i++)
                    tiles[i] = unpack_bpp_tile(data, i * bytesPerBlock[bpp], bpp);
            return tiles;
        }

        /*
        public static byte[] unpack_bpp2_tile(byte[] data, int offset)
        { return (unpack_bpp_tile(data, offset, 2)); }
        public static byte[] unpack_bpp3_tile(byte[] data, int offset)
        { return (unpack_bpp_tile(data, offset, 3)); }
        public static byte[] unpack_bpp4_tile(byte[] data, int offset)
        { return (unpack_bpp_tile(data, offset, 4)); }*/

        public static byte[] unpack_bpp_tile(byte[] data, int offset, int bppFormat)
        {
            byte[] tile = new byte[Constants.BLOCK_DIM * Constants.BLOCK_DIM];
            if (!(bppFormat >= 2 && bppFormat <= 4))
                throw new Exception("Out of Bitmap range.");

            int mask, temp;
            int[] bpp_pos = new int[bppFormat]; //More for conveniance and readibility
            for (int col = 0; col < Constants.BLOCK_DIM; col++)
                for (int row = 0; row < Constants.BLOCK_DIM; row++)
                {
                    /* SNES bpp format interlace each byte of the first 2 bitplanes.
                     * | byte 1 of first bitplane
                     * | byte 1 of the second bitplane 
                     * | byte 2 of first bitplane 
                     * | byte 2 of second bitplane 
                     * | ..
                     */
                    bpp_pos[0] = offset + col * 2;
                    bpp_pos[1] = bpp_pos[0] + 1;
                    mask = 1 << (7 - row);
                    temp = col * Constants.BLOCK_DIM + row;

                    tile[temp] = bitmasking(data[bpp_pos[0]], mask, 0);
                    tile[temp] |= bitmasking(data[bpp_pos[1]], mask, 1);
                    if (bppFormat == 3)
                    {
                        // When we have 3 bitplanes, the bytes for the third bitplane are after the 16 bytes of the 2 bitplanes.
                        bpp_pos[2] = offset + 16 + col;
                        tile[temp] |= bitmasking(data[bpp_pos[2]], mask, 2);
                    }
                    else if (bppFormat == 4)
                    {
                        // For 4 bitplanes, the 2 added bitplanes are interlaced like the first two.
                        bpp_pos[2] = offset + 16 + col * 2;
                        bpp_pos[3] = bpp_pos[2] + 1;

                        for (int i = 2; i < 4; i++)
                            tile[temp] |= bitmasking(data[bpp_pos[i]], mask, i);
                    }
                }
            return tile;
        }

        private static byte bitmasking(int data, int mask, int shiftNumber)
        { return (byte)(Convert.ToByte(((data & mask) == mask)) << shiftNumber); }
        private static byte bitShift(int mask, int shiftNumber)
        { return (byte)((byte)(mask & 1) << (byte)(shiftNumber)); }
        /*
        public static byte[] pack_bpp1_tile(byte[] tile)
        { return pack_bpp_tile(tile, 1); }
        public static byte[] pack_bpp2_tile(byte[] tile)
        { return pack_bpp_tile(tile, 2); }
        public static byte[] pack_bpp3_tile(byte[] tile)
        { return pack_bpp_tile(tile, 3); }
        public static byte[] pack_bpp4_tile(byte[] tile)
        { return pack_bpp_tile(tile, 4); }*/
        public static byte[] pack_bpp_tile(byte[] tile, int bpp)
        {
            byte[] output = new byte[bpp * Constants.BLOCK_DIM];
            //int maxcolor = 2 << bpp, temp2, temp; //sanity check related code
            int temp2, temp;
            byte color;
            for (int col = 0; col < Constants.BLOCK_DIM; col++)
                for (int row = 0; row < Constants.BLOCK_DIM; row++)
                {
                    color = tile[col * Constants.BLOCK_DIM + row];
                    //if (color > maxcolor)         //sanity check code may slow the code down
                    //throw new Exception("Error");

                    temp2 = (Constants.BLOCK_DIM - 1) - row;
                    if (bpp == 1)
                        output[col] += bitShift(color, temp2);
                    else
                    {
                        if (bpp >= 2)
                        {
                            temp = col * 2;
                            output[temp] += bitShift(color, temp2);
                            output[temp + 1] += bitmasking(color, 2, temp2);
                        }
                        if (bpp == 3)
                            output[16 + col] += bitmasking(color, 4, temp2);
                        else if (bpp == 4)
                            for (int i = 0; i < 2; i++)
                                output[(16 + col * 2) + i] += bitmasking(color, bpp * (i + 1), temp2);
                    }
                }
            return output;
        }
    }
}
