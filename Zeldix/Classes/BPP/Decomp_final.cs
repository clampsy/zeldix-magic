﻿using System;
using System.Collections.Generic;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.BPP
{
    public static class Decomp_final
    {
        private const int maxBuffer = 0x600;
        public static List<byte> Decompress(int pos, bool isLZ2 = false, bool buffer = false)
        {
            List<byte> dataBuffer = new List<byte>();
            bool done = false;

            while (done == false)
            {
                bool expand = false;
                byte b = RomIO.read(pos);

                if (b == 0xFF)
                    done = true;

                if (((b >> 5) & 0x07) == 7) //expanded command
                    expand = true;
                byte cmd = 0;
                short length = 0;
                if (expand)
                {
                    cmd = (byte)((b >> 2) & 0x07);
                    length = BitConverter.ToInt16(new byte[] { (RomIO.read(pos + 1)), (byte)(b & 0x03) }, 0);
                    pos += 2;
                }
                else
                {
                    cmd = (byte)((b >> 5) & 0x07);
                    length = (byte)(b & 0x1F);
                    pos += 1;
                }
                length += 1;
                if (cmd == 0)//000 Direct Copy Followed by (L+1) bytes of data
                    for (int i = 0; i < length; i++)
                        dataBuffer.Add(RomIO.read(pos++));
                if (cmd == 1)//001 "Byte Fill" Followed by one byte to be repeated (L + 1) times
                {
                    byte copiedByte = RomIO.read(pos++);
                    for (int i = 0; i < length; i++)
                        dataBuffer.Add(copiedByte);
                }
                if (cmd == 2)//010    "Word Fill" Followed by two bytes. Output first byte, then second, then first,then second, etc. until (L+1) bytes has been outputted
                {
                    byte copiedByte = RomIO.read(pos);
                    byte copiedByte2 = RomIO.read(pos + 1);
                    pos += 2;
                    int j = 0;
                    for (int i = 0; i < length; i++)
                    {
                        if (j == 0)
                        {
                            dataBuffer.Add(copiedByte);
                            j = 1;
                        }
                        else
                        {
                            dataBuffer.Add(copiedByte2);
                            j = 0;
                        }
                    }
                }

                if (cmd == 3)//"Increasing Fill" Followed by one byte to be repeated (L + 1) times, but the byte is increased by 1 after each write
                {
                    byte copiedByte = RomIO.read(pos);
                    pos += 1;
                    for (int i = 0; i < length; i++)
                        dataBuffer.Add((byte)(copiedByte + i));
                }

                if (cmd == 4)//"Repeat" Followed by two bytes (ABCD byte order) containing address (in the output buffer) to copy (L + 1) bytes from
                {
                    int pos2;
                    if (isLZ2)
                        pos2 = Conversion.toInt(RomIO.read(pos + 1), RomIO.read(pos));
                    else pos2 = Conversion.toInt(RomIO.read(pos), RomIO.read(pos + 1));
                    pos += 2;
                    for (int i = 0; i < length; i++)
                        dataBuffer.Add(dataBuffer[pos2++]);
                }
            }

            if (buffer)
            {
                while (dataBuffer.Count < maxBuffer)
                    dataBuffer.Add(0x0);
                while (dataBuffer.Count > maxBuffer)
                    dataBuffer.RemoveAt(maxBuffer);
            }
            return dataBuffer;
        }
    }
}
