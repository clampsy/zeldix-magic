﻿using System;
using System.Drawing;
using Zeldix.Classes.Crap;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Classes.BPP
{
    delegate int math(int index);
    /// <summary>
    /// Class that does the following:
    /// a) Stores LZ1 pointers
    /// b) Decompresses graphics and converts to Bitmap image
    /// c) Stores Bitmaps in an array to save on retrival time
    /// </summary>
    public sealed class DecompressBuffer
    {
        /* Documentation help:
         * Zarby, MathOnNapkins
         */
         
        private bitmapss[] bitmapBuffer;

        private const int
            pointersStart = 0x4F80,
            numOfPointers = 223; //223 * 3 = 29D
        private int[] arrayOfAddresses;
        private int currentGFXIndex;
        private Get_Pallette getPal;
        /// <summary>
        /// Structure to store the bitmaps at different
        /// indexes for different bit depths.
        /// </summary>
        private struct bitmapss
        {
            public Bitmap
                bmp3;
            public byte[] rawData;
            public byte[][] sortedRawData;
        }

        /// <summary>
        /// Initializes a new instance of the DecompressBuffer class
        /// </summary>
        /// <param name="rom"></param>
        public DecompressBuffer()
        {
            currentGFXIndex = 0;
            bitmapBuffer = new bitmapss[getSize];
            arrayOfAddresses = getLZ1Pointers();
            getPal = new Get_Pallette();
        }

        /// <summary>
        /// Guess what this does
        /// </summary>
        /// <returns>int array of addresses for each graphic entry</returns>
        private int[] getLZ1Pointers()
        {
            int[] addresses = new int[numOfPointers];
            int jj = pointersStart;

            math bob = delegate (int index) { return jj + (numOfPointers * index); };

            for (int i = 0; i < numOfPointers; i++, jj = i + pointersStart)
                addresses[i] = RomIO.snesToPc( bob(2), bob(1), bob(0));
            return addresses;
        }

        /// Returns a Bitmap of the graphic at a certain index.
        /// 
        /// Three scenarios to consider:
        /// 
        /// a) If the array, named bitmapBuffer, is null at the index value "index"
        ///    the graphic is decompressed and stored in the array
        /// 
        /// b) If the array, named bitmapBuffer, is NOT null at the index value "index",
        ///    the bitmap is returned.
        ///    
        /// c) If the flag update is set to true,
        ///    the graphic is decompressed and stored in the array regardless of
        ///    the null value at the index
        ///    
        /// The explained scenario saves of CPU cycles assuming a graphic entry is called
        /// more than once.
        /// </summary>
        /// <param name="index">The index to read the graphic</param>
        /// <param name="isBBP3">Specify the BPP to read the graphic as</param>
        /// <param name="paly">A palre</param>
        /// <param name="update"></param>
        /// <returns></returns>
        public Bitmap getBMP(int index = -1, int BBP = 3, Color[] paly = null, bool update = false)
        {
            indexRepeat(ref index, update);

            Bitmap returnme = null;

            if (bitmapBuffer[index].rawData == null)
                bitmapBuffer[index].rawData = Decomp_final.Decompress(arrayOfAddresses[index], true, true).ToArray();

            if (BBP == 3)
            {
                if (bitmapBuffer[index].bmp3 == null || update)
                    bitmapBuffer[index].bmp3 = BPPRender.getBitmap(16, paly, BPPPack.unpackTiles(BBP, bitmapBuffer[index].rawData));
                returnme = bitmapBuffer[index].bmp3;
            }
            else
                throw new NotImplementedException();

            if (paly != null)
                Palette.paletteSet(ref returnme, paly);

            if (returnme == null)
                throw new NullReferenceException();

            return returnme;
        }

        private void indexRepeat(ref int index, bool update)
        {
            if (index == -1)
            {
                if (currentGFXIndex != -1)  //sanity check
                    index = currentGFXIndex;
                else throw new IndexOutOfRangeException();
            }
            else if (!isWithinLimit(index)) //more sanity checking
                throw new IndexOutOfRangeException();

            if (bitmapBuffer[index].rawData == null || update)
                bitmapBuffer[index].rawData = Decomp_final.Decompress(arrayOfAddresses[index], true, true).ToArray();
        }

        public byte[][] getBytes(int bitsPerPixel, int index = -1, bool update = false)
        {
            indexRepeat(ref index, update);
            byte[] rawData = bitmapBuffer[index].rawData;

            if (rawData.Length % bitsPerPixel != 0)
            {
                if (bitmapBuffer[index].sortedRawData == null || !update)
                {
                    int numOfBlocks = rawData.Length / bitsPerPixel;
                    bitmapBuffer[index].sortedRawData = new byte[numOfBlocks][];

                    for (int i = 0; i < numOfBlocks; i++)
                        bitmapBuffer[index].sortedRawData[i / numOfBlocks][i % numOfBlocks] = rawData[i];
                }
                return bitmapBuffer[index].sortedRawData;
            }
            else throw new Exception("Leftover pixels.");
        }

        /// <summary>
        /// Public getter for the amount of graphic entries the game has
        /// </summary>
        public int getSize
        { get { return numOfPointers; } }


        /************************\
         *                      *
         *  |  For the GUI |    *
         *  V              V    *
        \************************/

        /// <summary>
        /// Increment the index to read by one.
        /// </summary>
        /// <returns></returns>
        public int goDown()
        { return (isWithinLimit(currentGFXIndex + 1) ? currentGFXIndex++ : getCurrentAddress()); }

        /// <summary>
        /// Decrement the index to read by one.
        /// </summary>
        /// <returns></returns>
        public int goUp()
        { return ((currentGFXIndex - 1 >= 0) && isWithinLimit(currentGFXIndex - 1)) ? currentGFXIndex-- : getCurrentAddress(); }

        /// <summary>
        /// Change the current Bitmap to read with a custom input
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int setCurrent(int index)
        {
            if (isWithinLimit(cursor: index))
                currentGFXIndex = index;
            return arrayOfAddresses[currentGFXIndex];
        }

        /// <summary>
        /// Specify what graphic index you want to read at.
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public int gotoEntry(int num)
        {
            if (num == -1)
                throw new ArgumentOutOfRangeException();
            return isWithinLimit(num) ? arrayOfAddresses[currentGFXIndex = num] : -1;
        }

        /// <summary>
        /// Get the address of the current entry
        /// </summary>
        /// <returns></returns>
        public int getCurrentAddress()
        { return arrayOfAddresses[currentGFXIndex]; }

        /// <summary>
        /// Get the current index the class is set to read at
        /// </summary>
        /// <returns></returns>
        public int getCurrentIndex()
        { return currentGFXIndex; }

        /// <summary>
        /// Determine whether or not an GFX index value
        /// is with the acceptable range.
        /// </summary>
        /// <param name="cursor"></param>
        /// <returns></returns>
        public bool isWithinLimit(int cursor = -1)
        {
            if (cursor == -1)
                cursor = currentGFXIndex;
            return (cursor >= 0) && (cursor < getSize);
        }
    }
}