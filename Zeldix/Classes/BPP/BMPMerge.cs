﻿/*
 * Class        :   BMPMerge.cs
 * Author       :   clampsy
 * Description  :   Merges Bitmaps into one Bitmap
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using Zeldix.Classes.PaletteProj;
namespace Zeldix.Classes.BPP
{
    public static class BMPMerge
    {
        //source: https://stackoverflow.com/questions/6383123/merge-two-images-to-create-a-single-image-in-c-net
        public static Bitmap mergeBitmaps(List<Bitmap> images, int blockDim, int myBlocksPerRow)
        {
            Bitmap finalImage = null;

            double thing = images.Count / myBlocksPerRow;
            int width = blockDim * myBlocksPerRow,
                height = (((thing % 2 != 0) ? (int)thing : (int)thing + 1)) * images[0].Height;
            finalImage = new Bitmap(width, height); //create a bitmap to hold the combined image

            using (Graphics g = Graphics.FromImage(finalImage))
            {
                g.Clear(Palette.transparency); //set background color
                int xFinal = 0, yFinal = xFinal;
                foreach (Bitmap image in images) //go through each image and draw it on the final image
                {
                    g.DrawImage(image, new Rectangle(xFinal, yFinal, image.Width, image.Height));
                    if ((xFinal += image.Width) + image.Width > finalImage.Width)
                    {
                        xFinal = 0;
                        yFinal += image.Height;
                    }
                }
            }

            return finalImage;
        }

        public static void mergeBitmaps(ref Bitmap finalImage, Bitmap a, Bitmap b)
        {
            if (!((a == null || b == null) && (!(a == null && b == null))))
            {

                if (finalImage == null)
                    finalImage = new Bitmap(Math.Max(a.Width, b.Width), a.Height + b.Height);

                int height;
                bool temp = false;
                int[] array = new int[2];
                if (a.Width + b.Width > finalImage.Width)
                {
                    height = a.Height + b.Height;
                    temp = true;
                }
                else height = Math.Max(a.Height, b.Height);
                finalImage = merge(finalImage, a, b, temp);
            }
            else
            {
                if (b == null)
                    finalImage = a;
                else finalImage = b;
            }
        }

        public static Bitmap mergeBitmaps(Bitmap a, Bitmap b)
        {
            Bitmap finalImage = null;
            mergeBitmaps(ref finalImage, a, b);
            return finalImage;
        }


        public static Bitmap mergeBitmaps(Bitmap a, Bitmap b, int maxWidth)
        {
            int width, height;
            bool temp = false;
            int[] array = new int[2];
            if (a.Width + b.Width > maxWidth)
            {
                height = a.Height + b.Height;
                width = Math.Max(a.Width, b.Width);
                temp = true;
            }
            else
            {
                height = Math.Max(a.Height, b.Height);
                width = a.Width + b.Width;
            }
            return merge((new Bitmap(maxWidth, height)), a, b, temp);
        }

        private static Bitmap merge(Bitmap canvas, Bitmap a, Bitmap b, bool theFlag)
        {
            using (Graphics g = Graphics.FromImage(canvas))
            {
                g.Clear(Palette.transparency);
                g.DrawImage(a, 0, 0);
                if (theFlag)
                    g.DrawImage(b, 0, a.Height);
                else g.DrawImage(b, a.Width, 0);
            }
            return canvas;
        }
    }
}