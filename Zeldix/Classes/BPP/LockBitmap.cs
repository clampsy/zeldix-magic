﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
//from "Work with Bitmaps Faster in C#"
namespace Zeldix.Classes.BPP
{
    public class LockBitmap
    {
        #region Variables
        Bitmap source = null;
        IntPtr Iptr = IntPtr.Zero;
        BitmapData bitmapData = null;
        #endregion
        #region Attributes
        public byte[] Pixels { get; set; }
        public int Depth { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        #endregion
        #region Constructor
        public LockBitmap(Bitmap source)
        { this.source = source; }
        #endregion
        #region Lock Bitmaps
        /// <summary>
        /// Lock bitmap data
        /// </summary>
        public void LockBits()
        {
            // Get width and height of bitmap
            Width = source.Width;
            Height = source.Height;

            // get total locked pixels count

            // get source bitmap pixel format size
            Depth = System.Drawing.Bitmap.GetPixelFormatSize(source.PixelFormat);

            // Check if bpp (Bits Per Pixel) is 8, 24, or 32
            if (Depth == 8 || Depth == 24 || Depth == 32)
            {
                // Lock bitmap and return bitmap data
                bitmapData = source.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadWrite,
                                             source.PixelFormat);

                // create byte array to copy pixel values
                Pixels = new byte[(Width * Height) * Depth / 8];
                Iptr = bitmapData.Scan0;

                // Copy data from pointer to array
                Marshal.Copy(Iptr, Pixels, 0, Pixels.Length);
            }
            else throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
        }
        #endregion
        #region Unlock Bits
        /// <summary>
        /// Unlock bitmap data
        /// </summary>
        public void UnlockBits()
        {
            // Copy data from byte array to pointer
            Marshal.Copy(Pixels, 0, Iptr, Pixels.Length);

            // Unlock bitmap data
            source.UnlockBits(bitmapData);
        }
        #endregion
        #region Get Pixel
        /// <summary>
        /// Get the color of the specified pixel
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Color GetPixel(int x, int y)
        {
            Color clr = Color.Empty;

            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            if (i > Pixels.Length - cCount)
                throw new IndexOutOfRangeException();

            if (Depth == 32) // For 32 bpp get Red, Green, Blue and Alpha
            {
                byte
                    b = Pixels[i],
                    g = Pixels[i + 1],
                    r = Pixels[i + 2],
                    a = Pixels[i + 3]; // a
                clr = Color.FromArgb(a, r, g, b);
            }
            if (Depth == 24) // For 24 bpp get Red, Green and Blue
            {
                byte
                    b = Pixels[i],
                    g = Pixels[i + 1],
                    r = Pixels[i + 2];
                clr = Color.FromArgb(r, g, b);
            }
            if (Depth == 8)
            // For 8 bpp get color value (Red, Green and Blue values are the same)
            {
                byte c = Pixels[i];
                clr = Color.FromArgb(c, c, c);
            }
            return clr;
        }
#endregion
        #region Set Pixel
        /// <summary>
        /// Set the color of the specified pixel
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="color"></param>
        public void SetPixel(int x, int y, Color color)
        {
            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            if (Depth == 32) // For 32 bpp set Red, Green, Blue and Alpha
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
                Pixels[i + 3] = color.A;
            }
            if (Depth == 24) // For 24 bpp set Red, Green and Blue
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
            }
            // For 8 bpp set color value (Red, Green and Blue values are the same)
            if (Depth == 8)
                Pixels[i] = color.B;
        }

        public void SetPixel(int x, int y, int color)
        {
            if (x >= 0 && y >= 0)
            {
                // Get color components count
                int cCount = Depth / 8;

                // Get start index of the specified pixel
                int i = ((y * Width) + x) * cCount;

                if (i < Pixels.Length)
                {
                    // For 8 bpp set color value (Red, Green and Blue values are the same)
                    if (Depth == 8)
                        Pixels[i] = (byte)color;
                }
            }
            else throw new IndexOutOfRangeException();
        }

        public void QuickSetPixel(int x, int y, int color)
        {
            LockBits();
            // Get color components count
            int cCount = Depth / 8;

            // Get start index of the specified pixel
            int i = ((y * Width) + x) * cCount;

            // For 8 bpp set color value (Red, Green and Blue values are the same)
            if (Depth == 8)
                Pixels[i] = (byte)color;
            UnlockBits();
        }

        #endregion
    }
}