﻿using System;
using System.Drawing;
using Zeldix.Classes.Crap;
using Zeldix.Classes.PaletteProj;

namespace Zeldix.Classes.BPP
{
    public class DecompressBuffer2
    {
        public static int getSize
        { get { return 223; } } //223 * 3 = 29D

        private bitmapss[] bitmapBuffer = new bitmapss[getSize];

        private const int pointersStart = 0x4F80;
        private static int[] arrayOfAddresses;


        private struct bitmapss
        {
            public byte[] rawData;
            public byte[][] sortedRawData;
        }
        
        private int[] getLZ1Pointers()
        {
            int[] addresses = new int[getSize];
            int jj = pointersStart;

            math bob = delegate (int index) { return jj + (getSize * index); };

            for (int i = 0; i < getSize; i++, jj = i + pointersStart)
                addresses[i] = RomIO.snesToPc(bob(2), bob(1), bob(0));
            return addresses;
        }

        public Bitmap getBMP(int index, bool update = false)
        {
            if (arrayOfAddresses == null)
                arrayOfAddresses = getLZ1Pointers();

            Bitmap returnme = null;

            if (bitmapBuffer[index].rawData == null || update)
                bitmapBuffer[index].rawData = Decomp_final.Decompress(arrayOfAddresses[index], true, true).ToArray();

            return returnme;
        }
    }
}
