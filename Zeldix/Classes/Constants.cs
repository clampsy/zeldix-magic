﻿using System;
using System.Drawing.Imaging;


namespace Zeldix.Classes
{
    public static class Constants
    {
        public const int
            GFX_font_address_USA_EU = 0x70000,
            GFX_font_address_CAN = 0x65078,
            GFX_font_address_GER = 0x646E8,
            GFX_font_address_FRA = 0x646E8,

            GFX_misc_address_ALL = 0x87000,

            GFX_link_address_ALL = 0x80000,

            ///Bank 01
            mono_bank01_U_size = 0x8000,
            mono_bank01_U_lo = 0x753F5,
            mono_bank01_U_hi = 0x753F6,
            mono_bank01_U_bank = 0x753EF,

            ///Bank 02
            mono_bank02_U_size = 0x14C0,
            mono_bank02_U_lo = 0x75437,
            mono_bank02_U_hi = 0x75438,
            mono_bank02_U_bank = 0x7543C,

            //both version are ahead of the American version
            mono_european_Offset = 0x23,
            mono_german_Offset = 0x8AB9E;




        public const PixelFormat stanard_px_format = PixelFormat.Format8bppIndexed;

        public const int BLOCK_DIM = 8;


        public const char
            SEP = '%',
            IGNORE1 = '$',
            IGNORE2 = IGNORE1,
            SPECIAL1 = '#',
            SPECIAL2 = SPECIAL1,
            ACTION1 = '[',
            ACTION2 = ']';

        public const bool debugMode = true;

        public const string
            PATH = (debugMode ? "../.." : ".") + "/Resources/",
            TXT = ".txt",
            SEPERATOR = " = ";

        public static readonly string
            INVALID = IGNORE2 + "INVALID CHARACTER" + IGNORE2,
            FREESPACE = IGNORE1 + "Free Space" + IGNORE2,
            NEWLINE = ACTION1 + "\\n" + ACTION2,
            NAME = SPECIAL1 + "NAME" + SPECIAL2,
            TXTFILEOPEN = "TXT|*" + TXT;

        public static readonly string
            newLine = Environment.NewLine,
            seperatorForTextFile = newLine + SEP + newLine;

    }
}
