﻿/*
 * Author:  Some dude online who made Be.Hexeditor
 * Comment: This code sucks
 */

namespace Zeldix.Classes.HexBox
{
    /// <summary>
    /// Specifies the case of hex characters in the HexBox control
    /// </summary>
    public enum HexCasing
    {
        /// <summary>
        /// Converts all characters to uppercase.
        /// </summary>
        Upper = 0,
        /// <summary>
        /// Converts all characters to lowercase.
        /// </summary>
        Lower = 1
    }
}