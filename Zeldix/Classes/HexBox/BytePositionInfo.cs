﻿/*
 * Author:  Some dude online who made Be.Hexeditor
 * Comment: This code sucks
 */

namespace Zeldix.Classes.HexBox
{
    /// <summary>
    /// Represents a position in the HexBox control
    /// </summary>
    struct BytePositionInfo
    {
        long _index;
        int _characterPosition;
        public BytePositionInfo(long index, int characterPosition)
        {
            _index = index;
            _characterPosition = characterPosition;
        }
        public int CharacterPosition
        { get { return _characterPosition; } }
        public long Index
        { get { return _index; } }        
    }
}
