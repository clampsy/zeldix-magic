/*
 * Author:  Some dude online who made Be.Hexeditor
 * Comment: This code sucks
 */

namespace Zeldix.Classes.HexBox
{
    internal abstract class DataBlock
    {
        internal DataMap _map;
        internal DataBlock
            _nextBlock,
            _previousBlock;
        public abstract long Length
        { get; }
        public DataMap Map
        { get { return _map; } }
        public DataBlock NextBlock
        { get { return _nextBlock; } }
        public DataBlock PreviousBlock
        { get { return _previousBlock; } }
        public abstract void RemoveBytes(long position, long count);
    }
}