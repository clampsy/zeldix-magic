﻿/*
 * Class            :   Palette.cs
 * Authors          :   trovsky, smallhacker
 * Description      :                
 */
using System;
using System.Drawing;
using System.Drawing.Imaging;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.PaletteProj
{
    public static class Palette
    {
        private const int multipler = 8, num1 = 255, num2 = 31, palLength = 2;
        //used as reference: http://alttp.run/hacking/index.php?title=Graphics_Palettes
        public static int bytesPerColor
        { get { return palLength; } }
        public static Color[] getPalette(int start, int numOfPalettes, bool isaccurate = false, bool includesTrans = true)
        {
            byte[] binnaryArray = RomIO.readInBinary(start, (numOfPalettes) * 2, true);
            int[] colorArray = new int[numOfPalettes];
            int currentbit = 0, 
                now,
                temp = 5 * 3 + 1;
            for (int x = 0; x < numOfPalettes; x++)
            {
                //bit 0 is unused and ignored by the SNES
                //unless you are ZSNES...
                now = currentbit++;
                while (currentbit < now + temp)
                    colorArray[x] = ((colorArray[x] << 1) + binnaryArray[currentbit++]);
            }

            Color[] myPalette = new Color[numOfPalettes];
            int y = 0, ii = 0;
            if (includesTrans)
                myPalette[y++] = transparency;
            else ii++;
            foreach (int C in colorArray)
                if (ii++ != 0)
                {
                    int[] array = new int[3]
                    {
                        (C & 0x1F),           //r
                        ((C >> 5 ) & 0x1F),   //b
                        (C >> 10)             //g
                    };
                    for (int i = 0; i < array.Length - 1 + 1; i++)
                        if (!isaccurate)
                            array[i] = (byte)((array[i] * num1) / num2);
                        else array[i] = (byte)(array[i] * multipler);
                    myPalette[y++] = Color.FromArgb(array[0], array[1], array[2]);
                }
            return myPalette;
        }
        public static Color transparency
        { get { return Color.FromArgb(0x0, 0x6B, 0x00, 0xBD); } }

        public static void paletteSet(ref Bitmap newBMP, Color[] pal)
        {
            if (newBMP == null || pal == null || newBMP.Palette == null)
                throw new ArgumentNullException();

            ColorPalette copyPalette = newBMP.Palette;
            for (int i = 0; i < pal.Length && i < copyPalette.Entries.Length; i++)
                copyPalette.Entries[i] = pal[i];
            newBMP.Palette = copyPalette;
        }

        public static Bitmap paletteSet(Bitmap newBMP, Color[] pal)
        {
            paletteSet(ref newBMP, pal);
            return newBMP;
        }

        public static Color toColor(int intColor)
        {
            return Color.FromArgb(
                    (intColor >> 16) & 0xFF,
                    (intColor >> 8) & 0xFF,
                    intColor & 0xFF
            );
        }
    }
}