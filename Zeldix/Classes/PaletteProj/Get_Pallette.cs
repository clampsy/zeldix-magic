﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Zeldix.Classes.BPP;

namespace Zeldix.Classes.PaletteProj
{
    public class Get_Pallette
    {
        private List<int>[] myPaletteAddr;

        public Get_Pallette()
        {
            myPaletteAddr = new List<int>[5];

            for (int i = 0; i < myPaletteAddr.Length; i++)
                myPaletteAddr[i] = new List<int>();

            for (int i = 0; i < 8; i++)
                myPaletteAddr[2].Add(0xDD660 + i * 4 * 2);    //font

            for (int i = 0; i < 1; i++)
                myPaletteAddr[1].Add(0xDD660 + i * 4 * 2);    //dummy 1BPP color

            int[] dungeon = new int[]
            {
                0xDD734,
                0xDD7E8,
                0xDD89C,
                0xDD950,
                0xDDA04,
                0xDDAB8,
                0xDDB6C,
                0xDDC20,
                0xDDCD4,
                0xDDD88,
                0xDDE3C,
                0xDDEF0,
                0xDDFA4,
                0xDE058,
                0xDE10C,
                0xDE1C0,
                0xDE274,
                0xDE328,
                0xDE3DC,
                0xDE490
            };

            foreach (int ii in dungeon)
            {
                int temp = ii;
                for (int i = 0; i < 12; i++)                    //dungeon pal. 
                {
                    myPaletteAddr[3].Add(temp);
                    temp += 8 * 2;
                    if (i % 2 != 1) //WTH, Nintendo?
                        temp -= 2;
                }
            }
            for (int i = 0; i < 3; i++)
                myPaletteAddr[4].Add(0xDD306 + i * 15 * 2);     //link
        }

        public List<int>[] list
        { get { return myPaletteAddr; } }

        public List<int> getList(int index)
        { return myPaletteAddr[index]; }

        public Color[] getPalette(int bpp, int index, bool trans = false)
        {
            if (!(bpp > 0 && index > -1))
                throw new ArgumentOutOfRangeException();

            return Palette.getPalette(
                    myPaletteAddr[bpp][index],
                    BPPPack.getNumOfPal(bpp),
                    includesTrans: trans);
        }
    }
}
