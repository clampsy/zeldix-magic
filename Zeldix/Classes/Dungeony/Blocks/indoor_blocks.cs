﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zeldix.Classes.Crap;
using static Zeldix.Classes.Dungeony.Dungeon;

namespace Zeldix.Classes.Dungeony.Blocks
{
    public class indoor_blocks : frameWork
    {
        private const int
            maxBytesPerBlockFor3 = 0x0C,
            bytesPerBlock = 4,
            addressOfTotalMax = 0x8896;
        private readonly int
            maxBlockEntries,
            sizeOfBlockData,
            maxBytesPerBlockFor012;

        private readonly int[] dataSize;

        public indoor_blocks() : base(4)
        {
            switch (RegionId.myRegion)
            {
                case ((int)RegionId.region.USA):
                    primaryPointer_location = new int[] { 0x15AFA, 0x15B01, 0x15B08, 0x15B0F };
                    break;
                case ((int)RegionId.region.Japan):
                    primaryPointer_location = new int[] { 0x1585E, 0x15865, 0x1586C, 0x15873 };
                    break;
                case ((int)RegionId.region.German):
                    primaryPointer_location = new int[] { 0x15B7C, 0x15B83, 0x15B8A, 0x15B91 };
                    break;
                default:
                    throw new NotImplementedException();
            }

            //maxBytesPerBlockFor012 = ReadMixedNumbers.readTwoByte(addressOfStorageMaxFor012);
            maxBytesPerBlockFor012 = 0x80;

            dataSize = new int[]
            {
                   maxBytesPerBlockFor012,
                   maxBytesPerBlockFor012,
                   maxBytesPerBlockFor012,
                   maxBytesPerBlockFor3
            };

            for (int i = 0; i < numberOfPointers; i++)
                sizeOfBlockData += dataSize[i];
            maxBlockEntries = sizeOfBlockData / bytesPerBlock;
            refreshPointer3Bytes();
        }

        private int getNumberOfPointers()
        { return primaryPointer_location.Length; }

        public void movePointers(uint pointerNo, int newAddress)
        {
            if (!(pointerNo <= getNumberOfPointers()))
                throw new ArgumentOutOfRangeException();

            int size = dataSize[pointerNo];

            if (RomIO.isEmpty(newAddress, size))
            {
                PointerRead.checkAddressWithinRangeOf3Byte(newAddress);
                RomIO.swapBytes(primaryPointer_address[pointerNo], newAddress, size);
                byte[] newPointer = PointerRead.generatePointer3(newAddress);
                RomIO.writeToArray(primaryPointer_location[pointerNo], newPointer.Length, newPointer);
                refreshPointer3Bytes();
            }
            else throw new Exception(Settings.getString(moveError));
        }

        /// <summary>
        /// Read the rom data to generate a Linked List of block objects
        /// </summary>
        public SortedList<ushort, List<i_block>> readBlocks()
        {
            byte[] allData = null;

            //Merge all data into one array because I want to keep things simple and readable.
            for (int i = 0; i < numberOfPointers; i++)
            {
                byte[] b = RomIO.read(primaryPointer_address[i], dataSize[i]);
                if (allData == null)
                    allData = b;
                else allData = allData.Concat(b).ToArray();
            }

            SortedList<ushort, List<i_block>> allDungeonBlocks = new SortedList<ushort, List<i_block>>();

            for (ushort i = 0; i <= maxRoomNo; i++)
                allDungeonBlocks.Add(i, new List<i_block>());

            //now read every 4 byte pair and create 
            for (int i = 0; i < allData.Length; i += bytesPerBlock)
            {
                /*
                 * Skip adding the block if the room number is the value of 0xFFFF
                 * The game doesn't have unused/empty data, but for the purposes of this
                 * editor, we'll use 0xFFFF.
                 */
                if (!i_block.isNulledOut(allData[i + 0], allData[i + 1]))
                {
                    i_block blocky = new i_block(allData[i + 0], allData[i + 1], allData[i + 2], allData[i + 3]);

                    if (!allDungeonBlocks.ContainsKey(blocky.room))
                        allDungeonBlocks.Add(blocky.room, new List<i_block>());
                    allDungeonBlocks[blocky.room].Add(blocky);
                }
            }
            return allDungeonBlocks;
        }

        public void writeAllBlocks(SortedList<ushort, List<i_block>> allDungeonBlocks)
        {
            List<i_block> allBlocks = new List<i_block>();

            foreach (ushort i in allDungeonBlocks.Keys)
                foreach (i_block blocky in allDungeonBlocks[i])
                    allBlocks.Add(blocky);

            const string error = "dungeon_error_block_overload";

            if (allBlocks.Count > maxBlockEntries)
                throw new Exception(Settings.getString(error));

            byte[][] blockData_set = new byte[numberOfPointers][];

            int currentEntry = 0;

            /* Convert block data to byte arrays and attempt to insert into array blockData_set.
             * 
             * blockData_set is of length pointerNumber (the number of pointers for the blocks.
             * Each index represents the locations to insert the location. Therefore, all data
             * at index i is stored at the address primaryPointer_address[i].
             */

            foreach (i_block blocky in allBlocks)
            {
                /* You get this error if you run out of space, or in other words,
                 * if you are inserting too many blocks.
                 */
                if (currentEntry > numberOfPointers)
                    throw new Exception(Settings.getString(error));

                byte[] blockData;
                blocky.getBytes(out blockData);

                if (blockData_set[currentEntry] == null)
                {
                    /* Store the block entry. No need to check storage limits here
                     * because it's very unlikely that an entire space dedicated
                     * to block storage is less than a single block.
                     */
                    blockData_set[currentEntry] = blockData;
                }
                else if (blockData_set[currentEntry].Length < dataSize[currentEntry] + blockData.Length)
                {
                    //Store the block entry if space is available.
                    blockData_set[currentEntry] = blockData_set[currentEntry].Concat(blockData).ToArray();
                }
                else
                {
                    //otherwise move to the next location, if it exists
                    currentEntry++;

                    if (currentEntry > numberOfPointers)
                        throw new Exception(Settings.getString(error));
                    else blockData_set[currentEntry] = blockData;
                }
            }

            for (int i = 0; i < numberOfPointers; i++)
            {
                if (blockData_set[i] != null)
                    RomIO.writeToArray(primaryPointer_address[i], blockData_set[i].Length, blockData_set[i]);

                //clear up unused space

                int size = blockData_set[i] == null ? 0 : blockData_set[i].Length;
                for (int j = size; j < dataSize[i]; j++)
                    RomIO.writeToArray(primaryPointer_address[i] + j, nullValue);
            }
        }
    }
}
