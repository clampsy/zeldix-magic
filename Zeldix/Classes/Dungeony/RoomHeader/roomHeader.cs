﻿using System;
using Zeldix.Classes.Crap;
namespace Zeldix.Classes.Dungeony.RoomHeader
{
    public class roomHeader
    {
        private const int
            numberOfHeaders = 0x140,
            bankOffset1 = 2,
            bankOffset2 = 10,
            sizeOfEachHeader = 14;

        public const int
            Max_BG2 = 8,
            Max_Collision = 4,
            Max_Tag = i_roomHeader.maxAndMask2,
            Max_Effect = i_roomHeader.maxAndMask1,
            Max_Planes = i_roomHeader.maxAndMask3,
            NumberOfExits = i_roomHeader.numberOfPlanesAndExits;

        private int secondaryPointer;
        private readonly int primaryPointer;

        private int[] roomHeaderPointers;

        private i_roomHeader[] roomHeaders;

        public roomHeader()
        {
            switch (RegionId.myRegion)
            {
                case (int)RegionId.region.Japan:
                case (int)RegionId.region.USA:
                    primaryPointer = 0xB5DD;
                    break;
                case (int)RegionId.region.German:
                    primaryPointer = 0xB5BD;
                    break;
                default:
                    throw new NotImplementedException();
            }

            roomHeaders = new i_roomHeader[numberOfHeaders];
            readPointer();
        }

        private void readPointer()
        {
            if (RomIO.read(primaryPointer + bankOffset1) != RomIO.read(primaryPointer + bankOffset2))
                throw new Exception();

            secondaryPointer = PointerRead.LongReadLoHiBank(primaryPointer);

            roomHeaderPointers = new int[numberOfHeaders];
            for (int i = 0; i < numberOfHeaders; i++)
            {
                int temp = secondaryPointer + i * 2;
                roomHeaderPointers[i] = AddressLoROM.snesToPc(
                        RomIO.read(temp),
                        RomIO.read(temp + 1),
                        AddressLoROM.pcToSnes_Bank(secondaryPointer));

                roomHeaders[i] = new i_roomHeader(RomIO.read(roomHeaderPointers[i], sizeOfEachHeader));
            }
        }

        public i_roomHeader getHeader(ushort room)
        {
            if (room <= Dungeon.maxRoomNo)
                return roomHeaders[room];
            else throw new ArgumentOutOfRangeException();
        }

        public void setHeader(ushort room, i_roomHeader rm)
        {
            if (room <= Dungeon.maxRoomNo)
                roomHeaders[room] = rm;
            else throw new ArgumentOutOfRangeException();
        }
    }
}
