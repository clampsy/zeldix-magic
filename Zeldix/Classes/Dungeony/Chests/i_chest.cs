﻿using System;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.Dungeony.Chests
{
    public class i_chest
    {
        /*
         * B??? ???r    rrrr rrrr    iiii iiii
         * 
         * B = is big key
         * i = item
         * r = room
         */
        private ushort MYroom;
        private ushort room
        {
            get
            { return MYroom; }
            set
            {
                if (value <= Dungeon.maxRoomNo)
                    MYroom = value;
                else throw new ArgumentOutOfRangeException();
            }
        }

        public byte item { get; set; }

        public i_chest(byte byte1, byte byte2, byte byte3)
        {
            room = getRoom(byte1, byte2);
            item = byte3;
        }

        internal static ushort getRoom(byte byte1, byte byte2)
        {
            ushort mask = 0x1FF;
            ushort number = Conversion.toUShort(byte2, byte1);
            return (ushort)(number & mask);
        }

        public byte[] getBytes()
        {
            byte[] data = BitConverter.GetBytes(room);
            return new byte[] { data[1], data[0], item };
        }
    }
}
