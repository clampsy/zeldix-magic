﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.Dungeony.DamagePits
{
    public class indoor_damagepits
    {
        private const int
            embedded2 = 2,
            sizeOfEachEntry = 2;

        private readonly int
            dataAddress,
            POINTER_LOCATION,
            DATALENGTH_VALUE_LOCATION,
            maxLength;

        private SortedDictionary<ushort, ushort> entries;

        internal indoor_damagepits()
        {
            switch (RegionId.myRegion)
            {
                case (int)RegionId.region.German:
                case (int)RegionId.region.USA:
                    POINTER_LOCATION = 0x394AB;
                    DATALENGTH_VALUE_LOCATION = 0x394A6;
                    break;
                case (int)RegionId.region.Japan:
                    POINTER_LOCATION = 0x394A2;
                    DATALENGTH_VALUE_LOCATION = 0x3949D;
                    break;
                default:
                    throw new NotImplementedException();
            }


            dataAddress = PointerRead.LongReadLoHiBank(POINTER_LOCATION);
            maxLength = ReadMixedNumbers.readTwoByte(DATALENGTH_VALUE_LOCATION) + embedded2;
            readData();
        }

        private void readData()
        {
            entries = new SortedDictionary<ushort, ushort>();
            byte[] b = RomIO.read(dataAddress, maxLength);
            for (int i = 0; i < maxLength; i += sizeOfEachEntry)
            {
                ushort room = Conversion.toUShort(b[i + 1], b[i]);
                if (room <= Dungeon.maxRoomNo)
                    entries.Add(room, room);
            }
        }

        public bool doesRoomHaveDamagePits(ushort roomNo)
        { return entries.ContainsKey(roomNo); }

        public void makeRoomHaveDamagePits(ushort roomNo, bool yesMakeHavePit)
        {

            if (yesMakeHavePit)
            {
                if (roomNo <= Dungeon.maxRoomNo)
                    entries.Add(roomNo, roomNo);
            }
            else
            {
                if (entries.ContainsKey(roomNo))
                    entries.Remove(roomNo);
            }
        }

        public void writeAllData()
        {
            if (entries.Count > maxLength / sizeOfEachEntry)
                Settings.getString(String.Format("dungeon_error_pit_size", entries.Count - (maxLength / sizeOfEachEntry)));

            byte[] data = new byte[0];

            foreach (ushort i in entries.Keys)
            {
                byte[] b = BitConverter.GetBytes(i);
                data = data.Concat(new byte[] { b[1], b[0] }).ToArray();
            }

            while (entries.Count < maxLength / sizeOfEachEntry)
                data = data.Concat(new byte[] { Dungeon.nullValue, Dungeon.nullValue }).ToArray();

            RomIO.writeToArray(dataAddress, maxLength, data);
        }
    }
}
