﻿using System.Drawing;
using Zeldix.Properties;

namespace Zeldix.Classes.Dungeony
{
    public static class Dungeon_constants
    {
        public const int
            SPRITEBASE = 16,
            BLOCKBASE = 8;

        public static Image image_torch { get { return (Image)getsReource("t_torch"); } }
        public static Image image_block { get { return (Image)getsReource("t_block"); } }
        public static Image image_sprite { get { return (Image)getsReource("t_sprite"); } }
        public static Image image_item { get { return (Image)getsReource("t_item"); } }
        public static Image image_delete { get { return (Image)getsReource("delete"); } }
        public static Image image_layer { get { return (Image)getsReource("layer"); } }
        public static Image image_bg { get { return (Image)getsReource("t_layout"); } }
        private static object getsReource(string fileName) { return Resources.ResourceManager.GetObject(fileName); }

        public static string[] pot_item_names { get; private set; }
        public static string[] pot_item_special_names { get; private set; }
        public static string[] sprite_names { get; private set; }

        public static void generateStrings()
        {
            pot_item_names = new string[22];
            sprite_names = new string[byte.MaxValue];
            pot_item_special_names = new string[5];

            for (int i = 0; i < pot_item_names.Length; i++)
                pot_item_names[i] = Crap.Settings.getString("pot_item_op" + i.ToString("000"));

            for (int i = 0; i < pot_item_special_names.Length; i++)
                pot_item_special_names[i] = Crap.Settings.getString("pot_item_special_op" + i);

            for (int i = 0; i <= 242; i++)
                sprite_names[i] = Crap.Settings.getString("sprite_names_op" + i.ToString("000"));
        }
    }
}
