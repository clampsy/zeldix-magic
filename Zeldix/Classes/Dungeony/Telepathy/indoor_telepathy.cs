﻿using System;
using Zeldix.Classes.Crap;

namespace Zeldix.Classes.Dungeony.Telepathy
{
    public class indoor_telepathy
    {
        private const int
            POINTER_LOCATION = 0x3B500,
            size = 0x280,
            sizeOfEachEntry = 2;
        private readonly int
            dataAddress;

        private ushort[] allData;

        internal indoor_telepathy()
        {
            dataAddress = AddressLoROM.snesToPc(
                RomIO.read(POINTER_LOCATION),
                RomIO.read(POINTER_LOCATION + 1),
                AddressLoROM.pcToSnes_Bank(POINTER_LOCATION));
        }

        private void readAllData()
        {
            allData = new ushort[Dungeon.maxRoomNo];
            byte[] rawData = RomIO.read(dataAddress, size);
            for (ushort room = 0; room < size; room += sizeOfEachEntry)
                allData[room] = Conversion.toUShort(rawData[room + 1], rawData[room + 0]);
        }

        public ushort getMonologueEntryForRoom(ushort room)
        { return allData[room]; }

        public void setMonologueEntryForRoom(ushort room, ushort entry)
        { allData[room] = entry; }

        internal void writeAllData()
        {
            byte[] rawData = new byte[size];

            int i = 0;
            for (ushort room = 0; room < size; room += sizeOfEachEntry)
            {
                byte[] b = BitConverter.GetBytes(room);
                rawData[i + 0] = b[1];
                rawData[i + 1] = b[0];
            }

            RomIO.writeToArray(dataAddress, size, rawData);
        }
    }
}