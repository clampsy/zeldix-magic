﻿/*
 * Class            :   PixelBox.cs
 * Author           :   Yvan Rodrigues
 * Description      :   source codeproject.com            
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
namespace Zeldix.Classes.GraphicBoxSelect
{
    public partial class PixelBox : PictureBox
    {
        private const int PIXEL = 8;
        private Bitmap preserveFormat;


        #region Initialization
        /// <summary>
        /// Initializes new instance of the <see cref="PixelBox">class.</see>
        /// </summary>


        public PixelBox()
        { constructor(); }

        public void constructor()
        {
            InterpolationMode = InterpolationMode.NearestNeighbor;
            InitializeComponent();
        }

        public PixelBox(int Width, int Height)
        {
            constructor();
            base.Width = Width;
            base.Height = Height;
        }

        /// <summary>
        /// Have to store the indexed image in an object
        /// because Microsoft is full of crackpots who can't
        /// code and convert to 32BPPRGB given any chance.
        /// </summary>

        public new Bitmap Image
        {
            set
            {
                preserveFormat = value;
                base.Image = value;
            }

            get
            { return preserveFormat; }
        }

        #endregion
        #region Properties
        /// <value>The interpolation mode.</value>
        [Category("Behavior")]
        [DefaultValue(InterpolationMode.Default)]
        private InterpolationMode InterpolationMode { get; set; }
        #endregion
        #region Overrides of PictureBox
        protected override void OnPaint(PaintEventArgs pe)
        {
            /*
            if (!temp && BorderStyle == BorderStyle.FixedSingle)
            {
                temp = true;
                Width += scale;
                Height += scale;
            }*/
            pe.Graphics.InterpolationMode = InterpolationMode;
            pe.Graphics.PixelOffsetMode = PixelOffsetMode.Half; //to stop cutoff
            base.OnPaint(pe);
        }
        #endregion
        public bool picImage_MouseDown(MouseEventArgs e, ref PixelBox selecting, int pixel = PIXEL)
        {
            bool pass = false;
            if (withInBorder(e))
            {
                double scale = scaleFactor(pixel, selecting);
                Rectangle rectangle = new Rectangle((borderize(e.X, this) * pixel), (borderize(e.Y, this) * pixel), pixel, pixel);
                selecting.Image = Image.Clone(rectangle, Image.PixelFormat);
                pass = true;
            }
            return pass;
        }

        public bool picImage_MouseDown(MouseEventArgs e, out int x, out int y)
        {
            x = y = -1;
            bool pass = false;
            if (withInBorder(e))
            {
                x = borderize(e.X, pixel: 4);
                y = borderize(e.Y, pixel: 4);
                pass = true;
            }
            return pass;
        }

        private double scaleFactor(int pixel, PixelBox selecting = null)
        {
            if (Image != null)
            {
                if (selecting != null)
                    return Math.Ceiling(format(Width, pixel) / format(selecting.Width, pixel));
                else
                    return Math.Ceiling(format(Width, pixel) / format(Image.Width, pixel));
            }
            else throw new NullReferenceException();
        }

        private double format(int a, int pixel)
        { return (Width / pixel * pixel); }

        public int getTileNumber(MouseEventArgs e, int pixel = PIXEL)
        { return (int)(borderize(e.Y) * 0x10 + borderize(e.X) * scaleFactor(pixel)); }

        private int borderize(int i, PixelBox pxl = null, int pixel = PIXEL)
        {
            if (pxl != null)
                return (int)(((i * scaleFactor(pixel, pxl)) / (pixel * 2)));
            else return (int)(((i * scaleFactor(pixel)) / (pixel * 2)));
        }

        public bool withInBorder(MouseEventArgs e)
        { return ((e.X >= 0) && (e.Y >= 0) && (e.X < Width) && (e.Y < Height)); }
    }
}